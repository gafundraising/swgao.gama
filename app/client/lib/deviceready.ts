/**
 * Created by mjwheatley on 3/15/16.
 */
import {Constants} from "../../both/Constants";
import {Session} from "meteor/session";

declare var navigator;
declare var device;
declare var window;

export class DeviceReady {
    public static onDeviceReady(): void {
        Session.set("deviceReady", true);
        // Listen for application resumed event
        document.addEventListener('resume', this.onResume, false);

        Session.set(Constants.SESSION.DEVICE_READY, true);
        navigator.globalization.getPreferredLanguage((language) => {
            console.log('navigator.globalization.getPreferredLanguage(): ' + language.value);
            if (language.value === 'es-ES' || language.value === 'es-US') {
                console.log("Set default language to spanish");
            }
        }, function () {
            console.log('Error getting language');
        });

        // Change color of recent apps title bar color (Android)
        if (device.platform === Constants.DEVICE.ANDROID) {
            window.plugins.headerColor.tint("#0a5978");
        }
    }

    public static onResume() {
        console.log("Application resumed, checking for timeout...");
    }
}