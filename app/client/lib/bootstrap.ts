/**
 * Created by mjwheatley on 7/01/16.
 */
import {Constants} from "../../both/Constants";
import {Meteor} from "meteor/meteor";
import {Session} from "meteor/session";
import {Tracker} from "meteor/tracker";

if (Meteor.settings.public.environment === "PRODUCTION") {
    console.log = function () {
    };
}

Session.setDefault(Constants.SESSION.DEVICE_READY, false);
Session.setDefault(Constants.SESSION.THREE_DEE_TOUCH_PAYLOAD, Constants.EMPTY_STRING);
Session.setDefault(Constants.SESSION.REMEMBER_ME, true);

Tracker.autorun(function () {
    console.log("Meteor server connection status: " + Meteor.status().status);
    //console.log("Accounts.loginServicesConfigured(): " + Accounts.loginServicesConfigured());
});