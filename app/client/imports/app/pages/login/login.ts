import {Alert, AlertController, MenuController, NavController, Platform} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Meteor} from "meteor/meteor";
import {TranslateService} from "@ngx-translate/core";
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Component, NgZone} from '@angular/core';
import {Constants} from "../../../../../both/Constants";
import {ToastMessenger} from "../../utils/ToastMessenger";

declare var Session; // persistent-session

@Component({
    selector: "page-login",
    templateUrl: "login.html"
})
export class LoginPage extends MeteorComponent {
    public loginForm: FormGroup;
    public loginControl: {
        username: AbstractControl,
        password: AbstractControl
    };
    public user: { username?: string, password?: string, rememberMe?: boolean } = {};
    public useFingerprint: boolean = false;

    constructor(public nav: NavController,
                public platform: Platform,
                public menu: MenuController,
                public alertCtrl: AlertController,
                public zone: NgZone,
                public translate: TranslateService,
                public fb: FormBuilder) {
        super();
        this.menu.swipeEnable(false);
    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            'username': ['', Validators.compose([
                Validators.required
            ])],
            'password': ['', Validators.compose([
                Validators.required
            ])]
        });

        this.loginControl = {
            username: this.loginForm.controls['username'],
            password: this.loginForm.controls['password']
        };

        if (Session.get(Constants.SESSION.REMEMBER_ME)) {
            this.user.rememberMe = Session.get(Constants.SESSION.REMEMBER_ME);
            this.user.username = Session.get(Constants.SESSION.USER_NAME);
        }

        this.autorun(() => this.zone.run(() => {
            this.useFingerprint = Session.get(Constants.SESSION.USE_FINGERPRINT);
        }));
    }

    public rememberMe(): void {
        Session.setPersistent(Constants.SESSION.REMEMBER_ME, this.user.rememberMe);
        if (!Session.get(Constants.SESSION.REMEMBER_ME)) {
            Session.setPersistent(Constants.SESSION.USER_NAME, null);
        } else {
            Session.setPersistent(Constants.SESSION.USER_NAME, this.user.username);
        }
    }

    public onSubmit(): void {
        var self = this;
        if (this.loginForm.valid) {
            if (this.user.rememberMe) {
                Session.setPersistent(Constants.SESSION.USER_NAME, this.user.username);
            }
            if (!Meteor.status().connected) {
                new ToastMessenger().toast({
                    type: "error",
                    message: self.translate.instant("general.noServerConnection"),
                    title: self.translate.instant("login.errors.signIn")
                });
                return;
            }
            Session.set(Constants.SESSION.LOADING, true);
            let credentials: string = btoa(this.user.username.toLocaleLowerCase() + ":" + this.user.password);
            Meteor.call(Constants.GAO_ENDPOINTS.AUTHENTICATE, credentials, (error, result) => {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    self.handleError(error);
                } else {
                    Meteor.loginWithToken(result.loginToken);
                }
            });
        }
    }

    private handleError(error: any): void {
        let self = this;
        let errorTitle: string = self.translate.instant("login.errors.signIn");
        if (error.reason === "Incorrect password") {
            errorTitle = self.translate.instant("login.errors.unauthorized");
            error.reason = self.translate.instant("login.errors.invalidCredentials");
        }
        let alert: Alert = self.alertCtrl.create({
            title: errorTitle,
            message: error.reason || error.message || error,
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    ionViewWillLeave() {
        this.menu.swipeEnable(true);
    }
}
