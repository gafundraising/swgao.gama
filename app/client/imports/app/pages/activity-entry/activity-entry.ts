import {Component} from '@angular/core'
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: "page-activity-entry",
    templateUrl: "activity-entry.html"
})
export class ActivityEntryPage extends MeteorComponent {
    public user:Meteor.User;
    public selectedWeek:string;
    public weeks:any;
    public activity:any = {};

    constructor(public nav:NavController,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.selectedWeek = "0";
        this.weeks = [
            {value: "0", text:"07/25 - 07/29"},
            {value: "1", text:"08/01 - 08/05"}
        ];
    }

    public onSubmit():void {

    }

    public clearAll():void {
        this.activity = {};
    }
}