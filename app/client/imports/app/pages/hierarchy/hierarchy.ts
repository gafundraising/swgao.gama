import {Component, NgZone, OnInit} from '@angular/core';
import {Alert, AlertController, MenuController, NavController, Platform} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Meteor} from "meteor/meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {LandingPage} from "../landing/landing";
import {ToastMessenger} from "../../utils/ToastMessenger";

declare var device;

@Component({
    selector: "page-hierarchy",
    templateUrl: "hierarchy.html"
})
export class HierarchyPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public items: Array<IHierarchy> = [];
    public groups: Array<IItemGroup> = [];
    public searchQuery: string = Constants.EMPTY_STRING;

    constructor(public nav: NavController,
                public alertCtrl: AlertController,
                public platform: Platform,
                public menuCtrl: MenuController,
                public zone: NgZone,
                public translate: TranslateService) {
        super();
        this.menuCtrl.swipeEnable(false);
    }

    ngOnInit() {
        let self = this;
        this.autorun(() => {
            this.user = Meteor.user();
        });
        if (!Meteor.status().connected) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("general.noServerConnection")
            });
            return;
        }
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call(Constants.GAO_ENDPOINTS.HIERARCHY, (error, response) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.error("Error retrieving hierarchy: ", error);
                let alert: Alert = self.alertCtrl.create({
                    title: self.translate.instant("general.apiError"),
                    message: self.translate.instant("page-hierarchy.errors.apiErrorMessage"),
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            } else {
                if (response && response.salesOrg && response.salesOrg.memberReps) {
                    self.items = response.salesOrg.memberReps;
                    self.groupItems();
                }
            }
        });
    }

    private groupItems(): void {
        let sortedItems = this.items.sort(function (a, b) {
            if (a.displayName < b.displayName) return -1;
            if (a.displayName > b.displayName) return 1;
            return 0;
        });

        if (this.searchQuery && this.searchQuery.trim() != Constants.EMPTY_STRING) {
            let searchQuery: string = this.searchQuery.toLocaleLowerCase().trim();
            sortedItems = sortedItems.filter((item) => {
                let normalizedName: string = item.displayName.toLocaleLowerCase().trim();
                let searchableItemString: string = item.accountNumber.toString() + normalizedName;
                return searchableItemString.indexOf(searchQuery) > -1;
            });
        }

        let currentLetter: string = Constants.EMPTY_STRING;
        let currentItems = [];
        let groups: Array<IItemGroup> = [];

        sortedItems.forEach((item, index) => {
            if (item.displayName.charAt(0) != currentLetter) {

                currentLetter = item.displayName.charAt(0);

                let newGroup = {
                    letter: currentLetter,
                    items: []
                };

                currentItems = newGroup.items;

                groups.push(newGroup);
            }

            currentItems.push(item);
        });
        this.zone.run(() => {
            this.groups = groups;
        });
    }

    public getItems(event: any): void {
        this.searchQuery = event.target.value;
        this.groupItems();
    }

    public onClearSearch(event: any): void {
        this.searchQuery = Constants.EMPTY_STRING;
        this.groupItems();
    }

    public onSelectedItem(item: IHierarchy): void {
        let self = this;
        let alert: Alert = this.alertCtrl.create({
            title: this.translate.instant("page-hierarchy.alerts.switchTo.title"),
            message: item.accountNumber + " - " + item.displayName,
            buttons: [{
                role: "cancel",
                text: this.translate.instant("general.cancel")
            }, {
                text: this.translate.instant("general.ok"),
                handler: () => {
                    self.doImpersonation(item);
                }
            }]
        });
        alert.present();
    }

    private doImpersonation(item: IHierarchy): void {
        let self = this;
        if (!Meteor.status().connected) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("general.noServerConnection")
            });
            return;
        }
        let info: any = {
            platform: this.platform.platforms()[0],
            version: this.platform.userAgent()
        };
        if (Meteor.isCordova) {
            info.platform = device.platform;
            info.version = device.version;
        }
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.call(Constants.GAO_ENDPOINTS.IMPERSONATE, {
            accountNumber: item.accountNumber,
            agentInfo: info
        }, (error, result) => {
            if (error) {
                Session.set(Constants.SESSION.LOADING, false);
                self.handleError(error);
            } else {
                Meteor.loginWithToken(result.loginToken, () => {
                    Session.set(Constants.SESSION.LOADING, false);
                    self.zone.run(() => {
                        self.nav.setRoot(LandingPage);
                    });
                });
            }
        });
    }

    private handleError(error: any): void {
        let self = this;
        let alert: Alert = self.alertCtrl.create({
            title: self.translate.instant("general.apiError"),
            message: error.reason || error.message || error,
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    ionViewWillLeave() {
        this.menuCtrl.swipeEnable(true);
    }
}

interface IItemGroup {
    letter?: string,
    items?: Array<IHierarchy>
}

interface IHierarchy {
    accountNumber?: number,
    displayName?: string,
    isActive?: boolean
}