import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, MenuController, App} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {Meteor} from "meteor/meteor";
import {Session} from "meteor/session";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {LoginPage} from "../login/login";
import {DashboardPage} from "../dashboard/dashboard";
import {HierarchyPage} from "../hierarchy/hierarchy";


@Component({
    selector: "page-landing",
    templateUrl: "landing.html"
})
export class LandingPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public isCordova:boolean = false;
    public customUrl:string;

    constructor(public nav:NavController,
                public menu:MenuController,
                public app:App,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.isCordova = Meteor.isCordova;

        this.app.setTitle(Meteor.settings.public["appName"]);

        let path:string = Session.get(Constants.SESSION.PATH);
        let urlParams:any = Session.get(Constants.SESSION.URL_PARAMS);

        // if (this.isCordova) {
            Meteor.defer(() => {
                this.autorun(() => {
                    this.user = Meteor.user() || Session.get(Constants.SESSION.OFFLINE_USER);
                    let passwordResetTokenExists:boolean = Session.get(Constants.SESSION.RESET_PASSWORD_TOKEN);
                    let viewCtrl = this.nav.getActive();
                    let activeComponent = viewCtrl.component;
                    if (!passwordResetTokenExists || (passwordResetTokenExists && activeComponent !== LoginPage)) {
                        this.delayForUser();
                    }
                });
            });
        // }
    }

    public delayForUser():void {
        Session.set(Constants.SESSION.LOADING, true);
        setTimeout(() => {
            Session.set(Constants.SESSION.LOADING, false);
            this.setRootPage();
        }, 2500);
    }

    public setRootPage():void {
        let page:any =  DashboardPage;
        if (Session.get(Constants.SESSION.RESET_PASSWORD_TOKEN) || !this.user) {
            page = LoginPage;
        } else {
            if (this.user && !this.user.profile.accountNumber) {
                page = HierarchyPage;
            }
        }
        this.zone.run(() => {
            this.nav.setRoot(page);
        });
    }
}