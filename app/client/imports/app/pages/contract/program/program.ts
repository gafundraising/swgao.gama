import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {ProgramModel} from "../../../../../../both/models/ProgramModel";
import {ProgramAgreementService} from "../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../both/models/ProgramAgreementReviewModel";
import {GroundConfigurationCollection} from "../../../../../../both/collections/GroundConfigurationCollection";
import {IConfigurationModel, IKeyText} from "../../../../../../both/models/ConfigurationModel";
import {IContractNavigationToolbarComponent} from "../components/contract-navigation-toolbar/contract-navigation-toolbar";

@Component({
    selector: "page-program",
    templateUrl: "program.html"
})
export class ProgramPage extends MeteorComponent implements IContractNavigationToolbarComponent {
    public user: Meteor.User;
    public nextStepEnabled: boolean = true;
    public previousStepEnabled: boolean = true;
    public title: string = "contract.newContract";
    public contract: ProgramAgreementReviewModel;
    public program: ProgramModel;
    public numberOfStudentsMin: number = 1;
    public numberOfStudentsMax: number = 5000;
    public numberOfStudentsStepValue: number = 1;
    public numberOfClassroomsMin: number = 1;
    public numberOfClassroomsMax: number = 50;
    public numberOfClassroomsStepValue: number = 1;
    public estWholesaleMin: number = 1;
    public estWholesaleMax: number = 50000;
    public estWholesaleStepValue: number = 1;
    public poTypes: Array<IKeyText> = [];
    public configuration: IConfigurationModel;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        if (this.contract.key) {
            this.title = "contract.editContract";
        }
        this.program = new ProgramModel(this.contract.program);
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });
                this.poTypes = this.configuration.purchaseOrderRequirements;
            }
        });
    }

    public startDateChanged(): void {
    }

    public signDateChanged(): void {
    }

    nextStepButton_onClicked(): void {
        // let programAgreement: ProgramAgreementReviewModel = this.programAgreementService.getProgramAgreement();
        // programAgreement.program = this.program;
        // this.programAgreementService.upsert(programAgreement);
    }

    previousStepButton_onClicked(): void {

    }
}