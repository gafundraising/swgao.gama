import {Component} from '@angular/core'
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {GroupInfoPage} from "../group-info/group-info";
import {GroupModel} from "../../../../../../../both/models/GroupModel";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {
    IPartner,
    IPartnerContact,
    IPartnerRelationsAsParent
} from "../../../../../../../both/models/ProgramAgreementDownloadResponseModel";
import {Enums} from "../../../../../../../both/Enums";
import {GroundPartnerContactCollection} from "../../../../../../../both/collections/GroundPartnerContactCollection";
import {OrganizationModel} from "../../../../../../../both/models/OrganizationModel";

@Component({
    selector: "page-group-list",
    templateUrl: "group-list.html"
})
export class GroupListPage extends MeteorComponent {
    public user: Meteor.User;
    public showSearchCancel: boolean = false;
    public searchQuery: string;
    public items: Array<GroupModel>;
    public groups: Array<GroupModel>;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
        super();
    }

    ngOnInit(): void {
        this.setItems();
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user && this.user.profile.accountNumber) {
                this.autorun(() => {
                    let organization: OrganizationModel = this.programAgreementService.getProgramAgreement().organization;
                    let orgPartner: IPartner = organization.partnerContact.partners.find((partner: IPartner) => {
                        return partner.partnerTypeCode == Enums.PARTNER_TYPE.ZSCH;
                    });
                    let childPartnerIds: Array<string> = orgPartner.partnerRelationsAsParent.map((item: IPartnerRelationsAsParent) => {
                        return item.childPartnerID;
                    });
                    console.log("childPartnerIds: ", childPartnerIds);
                    let groupPartnerContacts: Array<IPartnerContact> = GroundPartnerContactCollection.find({
                        userId: this.user._id,
                        "partners.partnerTypeCode": Enums.PARTNER_TYPE.ZGRP,
                        "partners.guid": {
                            $in: childPartnerIds
                        }
                    }, {
                        sort: {
                            name: 1
                        }
                    }).fetch();
                    console.log("groupPartnerContacts: ", groupPartnerContacts.length);
                    if (groupPartnerContacts) {
                        let groups: Array<GroupModel> = [];
                        groupPartnerContacts.forEach((groupPartnerContact: IPartnerContact) => {
                            let groupPartner: IPartner = groupPartnerContact.partners.find((partner: IPartner) => {
                                return partner.partnerTypeCode == Enums.PARTNER_TYPE.ZGRP;
                            });
                            let childPartnerIds: Array<string> = groupPartner.partnerRelationsAsParent.map((item: IPartnerRelationsAsParent) => {
                                return item.childPartnerID;
                            });
                            let sponsorPartnerContacts: Array<IPartner> = GroundPartnerContactCollection.find({
                                userId: this.user._id,
                                "partners.partnerTypeCode": Enums.PARTNER_TYPE.ZSPR,
                                "partners.guid": {
                                    $in: childPartnerIds
                                }
                            }).fetch();
                            console.log("sponsorPartnerContacts: ", sponsorPartnerContacts.length);
                            let sponsorContactPartner: IPartnerContact;
                            if (sponsorPartnerContacts && sponsorPartnerContacts.length > 0) {
                                sponsorContactPartner = sponsorPartnerContacts[0];
                            }
                            groups.push(this.programAgreementService.createGroupModelFromPartnerContact({
                                groupPartnerContact: groupPartnerContact,
                                sponsorPartnerContact: sponsorContactPartner
                            }));
                        });
                        this.groups = groups;
                        this.setItems();
                    }
                });
            }
        });
    }

    public setItems(): void {
        this.items = this.groups || [];
    }

    public filterItems(event): void {
        // Reset items back to all of the items
        this.setItems();

        // get the input value
        let value = event.target.value;

        // if the value is an empty string don't filter the items
        if (value && value.trim() != '') {
            this.items = this.items.filter((item) => {
                return (item.name.toLowerCase().indexOf(value.toLowerCase()) > -1);
            });
        }
    }

    public navPush(group): void {
        if (group) {
            this.programAgreementService.getProgramAgreement().group = group;
        }
        this.nav.push(GroupInfoPage);
    }
}