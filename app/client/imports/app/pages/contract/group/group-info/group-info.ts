import {AlertController, NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder} from '@angular/forms';
import {Component} from '@angular/core';
import {PartnerContactAddress} from "../../../../../../../both/models/Address";
import {GroupModel} from "../../../../../../../both/models/GroupModel";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";
import {GroundGroupCollection} from "../../../../../../../both/collections/GroupCollection";
import {IContractNavigationToolbarComponent} from "../../components/contract-navigation-toolbar/contract-navigation-toolbar";

@Component({
    selector: "page-group-info",
    templateUrl: "group-info.html"
})
export class GroupInfoPage extends MeteorComponent implements IContractNavigationToolbarComponent {
    public phones: Array<IPhone> = [];
    public canAddPhone: boolean = true;
    public groupInfo: GroupModel;
    public nextStepEnabled: boolean = true;
    public previousStepEnabled: boolean = true;
    public title: string = "contract.newContract";
    public contract: ProgramAgreementReviewModel;

    constructor(public nav: NavController,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public fb: FormBuilder,
                public programAgreementService: ProgramAgreementService
    ) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        if (this.contract.key) {
            this.title = "contract.editContract";
        }
        this.groupInfo = new GroupModel(this.contract.group);
        if (!this.groupInfo.name) {
            this.groupInfo.address = new PartnerContactAddress(
                this.programAgreementService.getProgramAgreement().organization.addresses.physical);
        }
        let mobilePhone = this.groupInfo.sponsor.phoneNumbers.mobile;
        let workPhone = this.groupInfo.sponsor.phoneNumbers.work;
        let homePhone = this.groupInfo.sponsor.phoneNumbers.home;
        if (mobilePhone) {
            this.phones.push(this.createPhoneInput({type: "mobile", number: mobilePhone.number}));
        }
        if (workPhone) {
            this.phones.push(this.createPhoneInput({type: "work", number: workPhone.number}));
        }
        if (homePhone) {
            this.phones.push(this.createPhoneInput({type: "home", number: homePhone.number}));
        }
    }

    public createPhoneInput(data: { type: string, number: string }): IPhone {
        let phone: IPhone = {
            number: data.number,
            type: data.type,
            icon: ""
        };
        switch (data.type) {
            case 'mobile':
                phone.icon = "phone-portrait";
                break;
            case 'home':
                phone.icon = "home";
                break;
            case 'work':
                phone.icon = "call";
        }
        return phone;
    }

    public addPhone(): void {
        var self = this;
        let phoneTypes: Array<IPhoneType> = self.getAvailablePhoneTypes();
        if (phoneTypes.length > 0) {
            phoneTypes[0].checked = true;
            let alert = this.alertCtrl.create({
                title: this.translate.instant("group-info.labels.phoneType.title"),
                inputs: phoneTypes,
                buttons: [
                    {
                        text: this.translate.instant("general.cancel")
                    },
                    {
                        text: this.translate.instant("general.ok"),
                        handler: (data) => {
                            self.phones.push(this.createPhoneInput({type: data, number: ""}));
                            //  if last phone type was added, hide the add phone button
                            if (phoneTypes.length === 1) {
                                self.canAddPhone = false;
                            }
                        }
                    }
                ]
            });
            alert.present();
        }

    }

    public getAvailablePhoneTypes(): Array<IPhoneType> {
        let phoneTypes: Array<IPhoneType> = [{
            type: 'radio',
            label: this.translate.instant("group-info.labels.phoneType.mobile"),
            value: 'mobile'
        }, {
            type: 'radio',
            label: this.translate.instant("group-info.labels.phoneType.home"),
            value: 'home'
        }, {
            type: 'radio',
            label: this.translate.instant("group-info.labels.phoneType.work"),
            value: 'work'
        }];
        this.phones.forEach((phone, index) => {
            for (var i = 0; i < phoneTypes.length; i++) {
                if (phone.type === phoneTypes[i].label.toLowerCase()) {
                    phoneTypes.splice(i, 1);
                }
            }
        });
        return phoneTypes;
    }

    public removePhone(phone): void {
        var index = this.phones.indexOf(phone);
        if (index > -1) {
            this.phones.splice(index, 1);
        }
        // if all phone types were used, now one is available, show the add phone button again
        if (!this.canAddPhone) {
            this.canAddPhone = true;
        }
    }

    nextStepButton_onClicked(): void {
        // var programAgreement: ProgramAgreementReviewModel = this.programAgreementService.getProgramAgreement();
        // this.groupInfo.orgId = programAgreement.organization._id;
        // if (this.groupInfo._id) {
        //     var modifiedGroup: GroupModel = new GroupModel(this.groupInfo);
        //     delete modifiedGroup._id;
        //     GroundGroupCollection.update(this.groupInfo._id, {$set: modifiedGroup}, {
        //         multi: false,
        //         upsert: false
        //     }, (error, result) => {
        //         if (error) {
        //             console.log("error: ", error);
        //         } else {
        //             console.log("result: ", result);
        //         }
        //     });
        // } else {
        //     this.groupInfo._id = GroundGroupCollection.insert(this.groupInfo);
        // }
        //
        // programAgreement.group = this.groupInfo;
        // this.programAgreementService.upsert(programAgreement);
    }

    previousStepButton_onClicked(): void {

    }
}

interface IPhone {
    number: string,
    type: string,
    icon: string
}

interface IPhoneType {
    type: string,
    label: string,
    value: string,
    checked?: boolean
}
