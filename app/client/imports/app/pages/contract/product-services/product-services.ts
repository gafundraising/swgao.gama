import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {ProductServicesModel} from "../../../../../../both/models/ProductServicesModel";
import {ProgramAgreementService} from "../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../both/models/ProgramAgreementReviewModel";
import {GroundConfigurationCollection} from "../../../../../../both/collections/GroundConfigurationCollection";
import {IConfigurationModel, IKeyText} from "../../../../../../both/models/ConfigurationModel";
import {IContractNavigationToolbarComponent} from "../components/contract-navigation-toolbar/contract-navigation-toolbar";

@Component({
    selector: "page-product-services",
    templateUrl: "product-services.html"
})
export class ProductServicesPage extends MeteorComponent implements IContractNavigationToolbarComponent {
    public user: Meteor.User;
    public nextStepEnabled: boolean = true;
    public previousStepEnabled: boolean = true;
    public productServices: ProductServicesModel;
    public deliveryTypes: Array<IKeyText> = [];
    public serviceLevels: Array<IKeyText> = [];
    public aPlusServices: Array<IKeyText> = [];
    public prizePrograms: Array<IKeyText> = [];
    public prePackPrizes: Array<IKeyText> = [];
    public goalPrizeCharges: Array<IKeyText> = [];
    public goalPrizeLevelMin: number = 0;
    public goalPrizeLevelMax: number = 30;
    public goalPrizeLevelStepValue: number = 1;
    public configuration: IConfigurationModel;
    public title: string = "contract.newContract";
    public contract: ProgramAgreementReviewModel;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        if (this.contract.key) {
            this.title = "contract.editContract";
        }
        this.productServices = new ProductServicesModel(this.contract.productServices);
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });
                this.deliveryTypes = this.configuration.requestedDeliveryTypes;
                this.serviceLevels = this.configuration.collectionMethods;
                this.aPlusServices = this.configuration.serviceLevels;
                this.prizePrograms = this.configuration.prizePrograms;
                this.prePackPrizes = this.configuration.prizePrePackIndicators;
                this.goalPrizeCharges = this.configuration.goalPrizeChargeIndicators;
            }
        });
    }

    nextStepButton_onClicked(): void {
        // let programAgreement: ProgramAgreementReviewModel = this.programAgreementService.getProgramAgreement();
        // programAgreement.productServices = this.productServices;
        // this.programAgreementService.upsert(programAgreement);
    }

    previousStepButton_onClicked(): void {

    }
}