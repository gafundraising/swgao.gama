import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {NavController, NavParams} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Meteor} from "meteor/meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {IProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";
import {GroundConfigurationCollection} from "../../../../../../../both/collections/GroundConfigurationCollection";
import {IConfigurationModel} from "../../../../../../../both/models/ConfigurationModel";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {Constants} from "../../../../../../../both/Constants";
import * as moment from "moment";

declare var SignaturePad;

@Component({
    selector: "page-capture-signature",
    templateUrl: "capture-signature.html"
})
export class CaptureSignaturePage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public contract: IProgramAgreementReviewModel;
    public configuration: IConfigurationModel;
    public signatureType: string;
    public signaturePad: any;
    public canvas: any;
    @ViewChild('signaturePadElement') signaturePadElement: ElementRef;


    constructor(public nav: NavController,
                public params: NavParams,
                public zone: NgZone,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {
        super();
    }

    ngOnInit() {
        this.contract = this.programAgreementService.getProgramAgreement();
        this.signatureType = this.params.data.signatureType;
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user && this.user.profile.accountNumber) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });
            }
        });
    }

    ngAfterViewInit() {
        this.canvas = this.signaturePadElement.nativeElement;
        this.signaturePad = new SignaturePad(this.canvas);
        this.resizeCanvas();
    }

    // Adjust canvas coordinate space taking into account pixel ratio,
    // to make it look crisp on mobile devices.
    // This also causes canvas to be cleared.
    private resizeCanvas() {
        // When zoomed out to less than 100%, for some very strange reason,
        // some browsers report devicePixelRatio as less than 1
        // and only part of the canvas is cleared then.
        if (this.canvas) {
            let ratio = Math.max(window.devicePixelRatio || 1, 1);
            this.canvas.width = this.canvas.offsetWidth * ratio;
            this.canvas.height = this.canvas.offsetHeight * ratio;
            this.canvas.getContext("2d").scale(ratio, ratio);
        }
    }

    public clearSignature(): void {
        this.signaturePad.clear();
    }

    public acceptSignature(): void {
        if (this.signaturePad.isEmpty()) {
            new ToastMessenger().toast({
                type: "error",
                message: this.translate.instant("page-capture-signature.errors.noSignature")
            });
        } else {
            switch(this.signatureType) {
                case Constants.SIGNATURE_TYPES.SPONSOR:
                    this.contract.sponsorApproved = moment().toISOString();
                    this.contract.sponsorSignature = this.signaturePad.toDataURL();
                    break;
                case Constants.SIGNATURE_TYPES.SALES_REP:
                    this.contract.salesRepApproved = moment().toISOString();
                    this.contract.salesRepSignature = this.signaturePad.toDataURL();
                    break;
            }
            //todo: save program agreement
            this.nav.pop();
        }
    }
}