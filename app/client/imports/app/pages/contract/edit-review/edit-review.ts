import {Component, NgZone} from '@angular/core';
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {OrganizationInfoPage} from "../organization/org-info/org-info";
import {GroupInfoPage} from "../group/group-info/group-info";
import {ProgramPage} from "../program/program";
import {BrochurePage} from "../brochure/brochure";
import {ProductServicesPage} from "../product-services/product-services";
import {InvoiceOtherPage} from "../invoice-other/invoice-other";
import {ProgramAgreementReviewModel} from "../../../../../../both/models/ProgramAgreementReviewModel";
import {ProgramAgreementService} from "../../../services/ProgramAgreement.service";
import {GroundConfigurationCollection} from "../../../../../../both/collections/GroundConfigurationCollection";
import {IConfigurationModel, IKeyText} from "../../../../../../both/models/ConfigurationModel";
import {Constants} from "../../../../../../both/Constants";
import {CaptureSignaturePage} from "./capture-signature/capture-signature";

@Component({
    selector: "page-edit-review",
    templateUrl: "edit-review.html"
})
export class EditReviewPage extends MeteorComponent {
    public user: Meteor.User;
    public SECTION_HEADERS: any = {
        ORGANIZATION: "organization",
        GROUP: "group",
        PROGRAM: "program",
        BROCHURES: "brochures",
        PRODUCT_SERVICES: "productServices",
        INVOICE_OTHERS: "invoiceOthers"
    };
    public contract: ProgramAgreementReviewModel;
    public configuration: IConfigurationModel;
    public hideImportMessage: boolean = Session.get(Constants.SESSION.HIDE_IMPORTANT_MESSAGE);
    public SIGNATURE_TYPES: any = Constants.SIGNATURE_TYPES;

    constructor(public nav: NavController,
                public zone: NgZone,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user && this.user.profile.accountNumber) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });
                this.translateKeys();
            }
        });
    }

    public editSection(section: string): void {
        let pages: any = {
            organization: OrganizationInfoPage,
            group: GroupInfoPage,
            program: ProgramPage,
            brochures: BrochurePage,
            productServices: ProductServicesPage,
            invoiceOthers: InvoiceOtherPage
        };
        this.nav.push(pages[section]);
    }

    public captureSignature(signatureType: string): void {
        this.nav.push(CaptureSignaturePage, {signatureType: signatureType});
    }

    public clearSignature(signatureType: string): void {
        switch (signatureType) {
            case Constants.SIGNATURE_TYPES.SPONSOR:
                this.contract.sponsorSignature = Constants.EMPTY_STRING;
                this.contract.sponsorApproved = Constants.EMPTY_STRING;
                break;
            case Constants.SIGNATURE_TYPES.SALES_REP:
                this.contract.salesRepSignature = Constants.EMPTY_STRING;
                this.contract.salesRepApproved = Constants.EMPTY_STRING;
                break;
        }
        //todo: save program agreement
    }

    public toggleImportantMessageVisibility(): void {
        this.hideImportMessage = !this.hideImportMessage;
        Session.set(Constants.SESSION.HIDE_IMPORTANT_MESSAGE, this.hideImportMessage);
    }

    public translateKeys(): void {
        if (this.contract && this.contract.productServices) {
            if (this.contract.productServices) {
                let prizeProgramKey: IKeyText = this.configuration.prizePrograms.find((item: IKeyText) => {
                    return item.key === this.contract.productServices.prizeProgramKey;
                });
                let goalPrizeChargeType: IKeyText = this.configuration.goalPrizeChargeIndicators.find((item: IKeyText) => {
                    return item.key === this.contract.productServices.goalPrizeChargeTypeKey;
                });
                let prizePrePackKey: IKeyText = this.configuration.prizePrePackIndicators.find((item: IKeyText) => {
                    return item.key === this.contract.productServices.prizePrePackKey;
                });
                let collectionMethodKey: IKeyText = this.configuration.collectionMethods.find((item: IKeyText) => {
                    return item.key === this.contract.productServices.collectionMethodKey;
                });
                let aplusServiceTypeKey: IKeyText = this.configuration.serviceLevels.find((item: IKeyText) => {
                    return item.key === this.contract.productServices.aplusServiceTypeKey;
                });
                let requestedDeliveryTypeKey: IKeyText = this.configuration.requestedDeliveryTypes.find((item: IKeyText) => {
                    return item.key === this.contract.productServices.requestedDeliveryTypeKey;
                });

                this.contract.productServices["translations"] = {
                    prizeProgramKey: prizeProgramKey ? prizeProgramKey.text : Constants.EMPTY_STRING,
                    goalPrizeLevel: this.contract.productServices.goalPrizeLevel,
                    goalPrizeChargeTypeKey: goalPrizeChargeType ? goalPrizeChargeType.text : Constants.EMPTY_STRING,
                    prizePrePackKey: prizePrePackKey ? prizePrePackKey.text : Constants.EMPTY_STRING,
                    collectionMethodKey: collectionMethodKey ? collectionMethodKey.text : Constants.EMPTY_STRING,
                    aplusServiceTypeKey: aplusServiceTypeKey ? aplusServiceTypeKey.text : Constants.EMPTY_STRING,
                    requestedDeliveryTypeKey: requestedDeliveryTypeKey ? requestedDeliveryTypeKey.text : Constants.EMPTY_STRING,
                };
            }

            let purchaseOrderTypeKey: IKeyText = this.configuration.purchaseOrderRequirements.find((item: IKeyText) => {
                return item.key === this.contract.program.purchaseOrderTypeKey;
            });
            this.contract.program["translations"] = {
                purchaseOrderTypeKey: purchaseOrderTypeKey ? purchaseOrderTypeKey.text : Constants.EMPTY_STRING
            };

            let accountTaxClassificationKey: IKeyText = this.configuration.taxClassifications.find((item: IKeyText) => {
                return item.key === this.contract.invoiceOther.accountTaxClassificationKey;
            });
            this.contract.invoiceOther["translations"] = {
                accountTaxClassificationKey: accountTaxClassificationKey ? accountTaxClassificationKey.text : Constants.EMPTY_STRING
            };
        }
    }
}