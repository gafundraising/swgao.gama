import {Modal, ModalController, NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {ProgramAgreementReviewModel} from "../../../../../../both/models/ProgramAgreementReviewModel";
import {ProgramAgreementService} from "../../../services/ProgramAgreement.service";
import {BrochureModel} from "../../../../../../both/models/BrochureModel";
import {IContractNavigationToolbarComponent} from "../components/contract-navigation-toolbar/contract-navigation-toolbar";
import {BrochureEntryPage} from "./brochure-entry/brochure-entry";

@Component({
    selector: "page-brochure",
    templateUrl: "brochure.html"
})
export class BrochurePage extends MeteorComponent implements IContractNavigationToolbarComponent {
    public user: Meteor.User;
    public nextStepEnabled: boolean = true;
    public previousStepEnabled: boolean = true;
    public title: string = "contract.newContract";
    public contract: ProgramAgreementReviewModel;
    public brochures: Array<BrochureModel>;

    constructor(public nav: NavController,
                public modalCtrl: ModalController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        this.brochures = this.contract.brochures;
        if (this.contract.key) {
            this.title = "contract.editContract";
        }
        this.autorun(() => {
            this.user = Meteor.user();
        })
    }

    public editBrochure(brochureGuid?: string): void {
        let modal: Modal = this.modalCtrl.create(BrochureEntryPage, {guid: brochureGuid});
        // modal.onDidDismiss((brochure: BrochureModel) => {
        //     if (brochure) {
        //         console.log("brochure: ", brochure);
        //     } else {
        //         console.log("Dismissed with no data");
        //     }
        // });
        modal.present()
    }

    nextStepButton_onClicked(): void {
    }

    previousStepButton_onClicked(): void {

    }
}