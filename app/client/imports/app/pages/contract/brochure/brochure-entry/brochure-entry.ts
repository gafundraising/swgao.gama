import {Component, NgZone, OnInit} from '@angular/core';
import {NavController, NavParams, ViewController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Meteor} from "meteor/meteor";
import {TranslateService} from "@ngx-translate/core";
import {
    BrochureModel,
    IProductLineForProfitPercentageModel,
    ProductLineForProfitPercentageModel
} from "../../../../../../../both/models/BrochureModel";
import {
    IConfigurationModel,
    IKeyText,
    IProductLineEntry,
    IProductLineShippingLevelProfitLevelMapping
} from "../../../../../../../both/models/ConfigurationModel";
import {GroundConfigurationCollection} from "../../../../../../../both/collections/GroundConfigurationCollection";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import * as moment from "moment";
import {IProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";
import {Constants} from "../../../../../../../both/Constants"
import {IProductServicesSelection} from "../../../../../../../both/models/ProgramAgreementDownloadResponseModel";

@Component({
    selector: "page-brochure-entry",
    templateUrl: "brochure-entry.html"
})
export class BrochureEntryPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public contract: IProgramAgreementReviewModel;
    public brochure: BrochureModel;
    public brochures: Array<BrochureModel> = [];
    public configuration: IConfigurationModel;
    public profitPercentData: Array<IProductLineForProfitPercentageModel>;
    public brochureGroups: Array<IKeyText> = [];
    public brochureCodes: Array<IKeyText> = [];
    public brochureMaterials: Array<IKeyText> = [];
    public shippingLevels: Array<IKeyText> = [];

    constructor(public nav: NavController,
                public params: NavParams,
                public viewCtrl: ViewController,
                public zone: NgZone,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {
        super();
    }

    ngOnInit() {
        let brochureGuid: string = this.params.data.guid;
        this.contract = this.programAgreementService.getProgramAgreement();
        let brochures: Array<BrochureModel> = [];
        if (this.contract.brochures) {
            this.contract.brochures.forEach((brochure: BrochureModel) => {
                brochures.push(new BrochureModel(brochure));
            });
        }
        let brochure: IProductServicesSelection = brochures.find((brochure: BrochureModel) => {
            return brochure.guid == brochureGuid;
        });
        if (!brochure) {
            brochure = {
                programAgreementID: this.contract.guid,
                programAgreement: {
                    ref: this.contract.id
                }
            };
        }
        this.brochure = new BrochureModel(brochure);
        console.log("brochure: ", this.brochure);
        if (brochures.length === 0) {
            brochures.push(this.brochure);
        }
        this.brochures = brochures;

        this.setProfitPercentData();

        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user && this.user.profile.accountNumber) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });

                this.setSelectorOptions();
            }
        });
    }

    private setProfitPercentData(): void {
        let profitPercentData: Array<IProductLineForProfitPercentageModel> = [];

        // Check if hybrid brochure and add related brochures
        if (this.brochure.productLineMasterMaterialKey) {
            let brochures: Array<BrochureModel> = this.brochures;
            if (brochures.length === 1) {
                // Only this current brochure is listed
                // Create related brochures
                let productLines: Array<IProductLineEntry> = this.configuration
                    .productLineEntries.filter((pl: IProductLineEntry) => {
                    return pl.masterProductLineMaterialNumber === this.brochure.productLineMasterMaterialKey &&
                        pl.productLineMaterialNumber !== this.brochure.productLineMaterialKey;
                });
                productLines.forEach((pl: IProductLineEntry) => {
                    let productLineMaterial: IKeyText = this.configuration.productLineMaterials.find((material: IKeyText) => {
                        return material.key === pl.productLineMaterialNumber;
                    });
                    // copy current brochure and edit to get new guid and new product line data
                    let currentBrochure: BrochureModel = new BrochureModel(this.brochure);
                    delete currentBrochure.guid;
                    let newBrochure: BrochureModel = new BrochureModel(currentBrochure);
                    newBrochure.productLineMaterialKey = pl.productLineMaterialNumber;
                    newBrochure.productLineMaterialDescription = productLineMaterial ? productLineMaterial.text : Constants.EMPTY_STRING;
                    brochures.push(new BrochureModel(newBrochure));
                });
            }
            let relatedBrochures: Array<BrochureModel> = brochures.filter((brochure: BrochureModel) => {
                return brochure.productLineMasterMaterialKey == this.brochure.productLineMasterMaterialKey;
            });
            relatedBrochures.forEach((brochure: BrochureModel) => {
                profitPercentData.push(new ProductLineForProfitPercentageModel(brochure));
            });
        } else {
            profitPercentData.push(new ProductLineForProfitPercentageModel(this.brochure));
        }
        this.profitPercentData = profitPercentData;

        console.log("brochures: ", this.brochures);
    }

    private setSelectorOptions(): void {
        let modelChanged: boolean = false;
        let productLinesByBrochureType: Array<IProductLineEntry>;
        let productLinesByBrochureGroup: Array<IProductLineEntry>;
        let productLinesByBrochureCode: Array<IProductLineEntry>;

        // Get product lines by brochure type
        if (this.brochure.brochureTypeKey) {
            productLinesByBrochureType = this.configuration.productLineEntries.filter((pl: IProductLineEntry) => {
                let now = moment();
                let validFrom = moment(pl.validFrom);
                let validTo = moment(pl.validTo);
                let isValid = (now >= validFrom && now <= validTo);
                return pl.brochureType == this.brochure.brochureTypeKey && isValid
            });
        }

        // Get product lines by type and group
        if (productLinesByBrochureType && this.brochure.brochureGroupKey) {
            productLinesByBrochureGroup = productLinesByBrochureType.filter((pl: IProductLineEntry) => {
                return pl.brochureGroup == this.brochure.brochureGroupKey;
            });
        }

        // Get product lines by type, group, and code
        if (productLinesByBrochureGroup && this.brochure.brochureCodeKey) {
            productLinesByBrochureCode = productLinesByBrochureGroup.filter((pl: IProductLineEntry) => {
                return pl.brochureCode == this.brochure.brochureCodeKey;
            });
        }

        // Only show brochure groups for product lines that have the selected type
        if (productLinesByBrochureType) {
            let brochureGroupKeys: any = productLinesByBrochureType.map((pl: IProductLineEntry) => {
                return pl.brochureGroup;
            });
            this.brochureGroups = this.configuration.brochureGroups.filter((brochureGroup: IKeyText) => {
                return brochureGroupKeys.includes(brochureGroup.key);
            });

            if (this.brochureGroups.length === 1 && !this.brochure.brochureGroupKey) {
                this.brochure.brochureGroupKey = this.brochureGroups[0].key;
                modelChanged = true;
            }
        }

        // Only show brochure codes for product lines that have the selected type and group
        if (productLinesByBrochureGroup) {
            let brochureCodeKeys: any = productLinesByBrochureGroup.map((pl: IProductLineEntry) => {
                return pl.brochureCode;
            });
            this.brochureCodes = this.configuration.brochureCodes.filter((brochureCode: IKeyText) => {
                return brochureCodeKeys.includes(brochureCode.key);
            });
            if (this.brochureCodes.length === 0 && this.brochure.brochureCodeKey !== null) {
                this.brochure.brochureCodeKey = null;
                modelChanged = true;
            }
            if (this.brochureCodes.length === 1 && !this.brochure.brochureCodeKey) {
                this.brochure.brochureCodeKey = this.brochureCodes[0].key;
                modelChanged = true;
            }
            if (this.brochure.brochureCodeKey) {
                let pl: IProductLineEntry = productLinesByBrochureGroup.find((pl: IProductLineEntry) => {
                    return pl.brochureType == this.brochure.brochureTypeKey &&
                        pl.brochureGroup == this.brochure.brochureGroupKey &&
                        pl.brochureCode == this.brochure.brochureCodeKey;
                });
                if (pl) {
                    if (pl.masterProductLineMaterialNumber && !this.brochure.productLineMasterMaterialKey) {
                        this.brochure.productLineMasterMaterialKey = pl.masterProductLineMaterialNumber;
                        let plMaster: IKeyText = this.configuration.productLineMasters.find((master: IKeyText) => {
                            return master.key === this.brochure.productLineMasterMaterialKey;
                        });
                        this.brochure.productLineMasterMaterialDescription = plMaster ? plMaster.text : Constants.EMPTY_STRING;
                    }
                    if (pl.productLineMaterialNumber && !this.brochure.productLineMaterialKey) {
                        this.brochure.productLineMaterialKey = pl.productLineMaterialNumber;
                        let material: IKeyText = this.configuration.productLineMaterials.find((material: IKeyText) => {
                            return material.key == this.brochure.productLineMaterialKey;
                        });
                        this.brochure.productLineMaterialDescription = material ? material.text : Constants.EMPTY_STRING;
                    }
                }
            }
        }

        // Only show brochure materials for product lines that have the selected type, group, and code
        if (productLinesByBrochureCode) {
            let brochureMaterialKeys: any = productLinesByBrochureGroup.map((pl: IProductLineEntry) => {
                return pl.brochureMaterialNumber;
            });
            this.brochureMaterials = this.configuration.brochureMaterials
                .filter((brochureMaterial: IKeyText) => {
                    return brochureMaterialKeys.includes(brochureMaterial.key);
                });
            if (this.brochureMaterials.length === 0 && this.brochure.brochureMaterialKey !== null) {
                this.brochure.brochureMaterialKey = null;
                modelChanged = true;
            }
            if (this.brochureMaterials.length === 1 && !this.brochure.brochureMaterialKey) {
                this.brochure.brochureMaterialKey = this.brochureMaterials[0].key;
                modelChanged = true;
            }
        }

        // Get shipping levels
        if (this.brochure.productLineMaterialKey) {
            let mappings: Array<IProductLineShippingLevelProfitLevelMapping> = this.configuration.productLineShippingLevelProfitLevelMappings
                .filter((mapping: IProductLineShippingLevelProfitLevelMapping) => {
                    return mapping.productLineMaterialNumber == this.brochure.productLineMaterialKey;
                });
            let shippingLevelKeys: any = mappings.map((map: IProductLineShippingLevelProfitLevelMapping) => {
                return map.shippingLevel;
            });
            this.shippingLevels = this.configuration.shippingLevels.filter((shippingLevel: IKeyText) => {
                return shippingLevelKeys.includes(shippingLevel.key);
            });
            if (this.shippingLevels.length === 0 && this.brochure.shippingLevelKey !== null) {
                this.brochure.shippingLevelKey = null;
                modelChanged = true;
            }
            if (this.shippingLevels.length === 1 && !this.brochure.shippingLevelKey) {
                this.brochure.shippingLevelKey = this.shippingLevels[0].key;
                modelChanged = true;
            }
        }

        this.brochureGroups.sort(this.sortKeyTextByText);
        this.brochureCodes.sort(this.sortKeyTextByText);
        this.brochureMaterials.sort(this.sortKeyTextByText);
        this.shippingLevels.sort(this.sortKeyTextByText);

        this.setProfitPercentData();

        if (modelChanged) {
            this.setSelectorOptions();
        }
    }

    private sortKeyTextByText(a: IKeyText, b: IKeyText): number {
        return (a.text > b.text) ? 1 : ((b.text > a.text) ? -1 : 0);
    }

    public getProfitLevels(profitData: ProductLineForProfitPercentageModel): Array<IKeyText> {
        if (this.brochure.productLineMaterialKey) {
            let mappings: Array<IProductLineShippingLevelProfitLevelMapping> = this.configuration.productLineShippingLevelProfitLevelMappings
                .filter((mapping: IProductLineShippingLevelProfitLevelMapping) => {
                    return mapping.productLineMaterialNumber == profitData.plMaterialNumber &&
                        mapping.shippingLevel == this.brochure.shippingLevelKey;
                });
            let profitLevelKeys: any = mappings.map((map: IProductLineShippingLevelProfitLevelMapping) => {
                return map.profitLevel;
            });
            let profitLevels: Array<IKeyText> = this.configuration.profitLevels.filter((profitLevel: IKeyText) => {
                return profitLevelKeys.includes(profitLevel.key);
            });
            if (profitLevels.length === 0 && this.brochure.profitLevelKey !== null) {
                profitData.profitLevelKey = null;
            }
            if (profitLevels.length === 1 && !this.brochure.profitLevelKey) {
                profitData.profitLevelKey = profitLevels[0].key;
            }
            return profitLevels.sort(this.sortKeyTextByText);
        }
    }

    public dismiss(save?: boolean): void {
        let returnData: BrochureModel;
        if (save) {
            returnData = this.brochure;
        }
        this.viewCtrl.dismiss(returnData);
    }

    public onSelectorChanged(event): void {
        console.log("selectorChanged event: ", event);
        this.setSelectorOptions();
    }
}