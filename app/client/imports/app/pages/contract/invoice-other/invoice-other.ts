import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {InvoiceOtherModel} from "../../../../../../both/models/InvoiceOtherModel";
import {ProgramAgreementService} from "../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../both/models/ProgramAgreementReviewModel";
import {GroundConfigurationCollection} from "../../../../../../both/collections/GroundConfigurationCollection";
import {IConfigurationModel, IKeyText} from "../../../../../../both/models/ConfigurationModel";
import {IContractNavigationToolbarComponent} from "../components/contract-navigation-toolbar/contract-navigation-toolbar";

@Component({
    selector: "page-invoice-other",
    templateUrl: "invoice-other.html"
})
export class InvoiceOtherPage extends MeteorComponent implements IContractNavigationToolbarComponent {
    public user: Meteor.User;
    public nextStepEnabled: boolean = true;
    public previousStepEnabled: boolean = true;
    public invoiceOther: InvoiceOtherModel;
    public taxStatusOptions: Array<IKeyText> = [];
    public configuration: IConfigurationModel;
    public title: string = "contract.newContract";
    public contract: ProgramAgreementReviewModel;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        if (this.contract.key) {
            this.title = "contract.editContract";
        }
        this.invoiceOther = new InvoiceOtherModel(this.contract.invoiceOther);
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });
                this.taxStatusOptions = this.configuration.taxClassifications;
            }
        });
    }

    nextStepButton_onClicked(): void {
        // let programAgreement: ProgramAgreementReviewModel = this.programAgreementService.getProgramAgreement();
        // programAgreement.invoiceOther = this.invoiceOther;
        // this.programAgreementService.upsert(programAgreement);
    }

    previousStepButton_onClicked(): void {

    }
}