import {NavController} from "ionic-angular/es2015";
import {Component} from '@angular/core';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {OrganizationModel} from "../../../../../../../both/models/OrganizationModel";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";
import {IContractNavigationToolbarComponent} from "../../components/contract-navigation-toolbar/contract-navigation-toolbar";
import {
    IPartnerContactAddress, IPartnerContactPhone,
    Partner,
    PartnerContact,
    PartnerContactPhone,
    ProgramAgreementPartner
} from "../../../../../../../both/models/ProgramAgreementDownloadResponseModel";
import {Enums} from "../../../../../../../both/Enums";
import * as moment from "moment";
import {PartnerContactAddress} from "../../../../../../../both/models/Address";
import {Constants} from "../../../../../../../both/Constants";

@Component({
    selector: "page-org-info",
    templateUrl: "org-info.html"
})
export class OrganizationInfoPage extends MeteorComponent implements IContractNavigationToolbarComponent {
    public user: Meteor.User;
    public nextStepEnabled: boolean = true;
    public previousStepEnabled: boolean = true;
    public readOnly: boolean = false;
    public organization: OrganizationModel;
    public title: string = "contract.newContract";
    public contract: ProgramAgreementReviewModel;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {
        super();
    }

    ngOnInit(): void {
        this.contract = this.programAgreementService.getProgramAgreement();
        if (this.contract.key) {
            this.title = "contract.editContract";
        }
        this.organization = new OrganizationModel(this.contract.organization);
        if (this.organization.partnerContact && this.organization.partnerContact.isMasterData) {
            this.readOnly = true;
        }
        this.autorun(() => {
            this.user = Meteor.user();
        });
    }

    nextStepButton_onClicked(): void {
        if (!this.organization.partnerContact) {
            this.contract.programAgreementPartners.push(this.createNewProgramAgreementPartner());
        } else {
            if (!this.organization.partnerContact.isMasterData) {
                this.updatePartnerContact();
            }
        }
        let programAgreement: ProgramAgreementReviewModel = this.programAgreementService.getProgramAgreement();
        programAgreement.organization = this.organization;
        this.programAgreementService.upsert();
    }

    private createNewProgramAgreementPartner(): ProgramAgreementPartner {
        // create new partner and partner contact
        let partnerContact: PartnerContact = new PartnerContact({
            userId: this.user._id,
            name: this.organization.name
        });
        this.organization.addresses.physical.partnerContactID = partnerContact.guid;
        this.organization.addresses.physical.partnerContact = {ref: partnerContact.id};
        this.organization.addresses.mailing.partnerContactID = partnerContact.guid;
        this.organization.addresses.mailing.partnerContact = {ref: partnerContact.id};
        partnerContact.partnerContactAddresses.push(this.organization.addresses.physical);

        if (this.organization.addresses.differentMailingAddress) {
            partnerContact.partnerContactAddresses.push(this.organization.addresses.mailing);
        }

        let partnerContactPhone: PartnerContactPhone;
        if (this.organization.phone) {
            partnerContactPhone = new PartnerContactPhone({
                phoneTypeKey: Enums.PHONE_TYPE.W,
                number: this.organization.phone,
                partnerContactID: partnerContact.guid,
                partnerContact: {
                    ref: partnerContact.id
                }
            });
            partnerContact.partnerContactPhones.push(partnerContactPhone)
        }
        if (this.organization.fax) {
            partnerContactPhone = new PartnerContactPhone({
                phoneTypeKey: Enums.PHONE_TYPE.F,
                number: this.organization.fax,
                partnerContactID: partnerContact.guid,
                partnerContact: {
                    ref: partnerContact.id
                }
            });
            partnerContact.partnerContactPhones.push(partnerContactPhone)
        }

        let partner: Partner = new Partner({
            partnerTypeCode: Enums.PARTNER_TYPE.ZSCH,
            partnerContactID: partnerContact.guid,
            partnerContact: {
                ref: partnerContact.id
            }
            //partner attributes will be added after submission and review
        });
        partnerContact.partners.push(partner);

        this.organization.partnerContact = partnerContact;

        return new ProgramAgreementPartner({
            programAgreementID: this.contract.guid,
            partnerContactID: partnerContact.guid,
            partnerFunctionKey: Enums.PARTNER_FUNCTION.ORGANIZATION,
            programAgreement: {
                ref: this.contract.id
            }
        });
    }

    private updatePartnerContact() {
        let partnerContact: PartnerContact = new PartnerContact(this.organization.partnerContact);
        if (partnerContact.name !== this.organization.name) {
            partnerContact.name = this.organization.name;
        }

        partnerContact.partnerContactAddresses.forEach((address:IPartnerContactAddress) => {
            switch (address.addressTypeKey) {
                case Enums.ADDRESS_TYPE.P:
                    address = this.organization.addresses.physical;
                    break;
                case Enums.ADDRESS_TYPE.M:
                    if (this.organization.addresses.differentMailingAddress) {
                        address = this.organization.addresses.mailing;
                    }
                    break;
            }
        });

        partnerContact.partnerContactPhones.forEach((phone: IPartnerContactPhone) => {
            switch (phone.phoneTypeKey) {
                case Enums.PHONE_TYPE.W:
                    if (this.organization.phone) {
                        phone.number = this.organization.phone;
                    }
                    break;
                case Enums.PHONE_TYPE.F:
                    if (this.organization.fax) {
                        phone.number = this.organization.fax;
                    }
                    break;
            }
        });
        this.organization.partnerContact = partnerContact;
    }

    previousStepButton_onClicked(): void {

    }
}