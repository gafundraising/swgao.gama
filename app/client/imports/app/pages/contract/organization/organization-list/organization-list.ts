import {Component} from '@angular/core';
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../../both/Constants";
import {OrganizationInfoPage} from "../org-info/org-info";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";
import {OrganizationModel} from "../../../../../../../both/models/OrganizationModel";
import {GroundPartnerContactCollection} from "../../../../../../../both/collections/GroundPartnerContactCollection";
import {IPartnerContact} from "../../../../../../../both/models/ProgramAgreementDownloadResponseModel";
import {Enums} from "../../../../../../../both/Enums";

@Component({
    selector: "page-org-list",
    templateUrl: "organization-list.html"
})
export class OrganizationListPage extends MeteorComponent {
    public user: Meteor.User;
    public showSearchCancel: boolean = false;
    public searchQuery: string = Constants.EMPTY_STRING;
    public items: Array<OrganizationModel>;
    public organizations: Array<OrganizationModel>;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
        super();
    }

    ngOnInit(): void {
        this.setItems();
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user && this.user.profile.accountNumber) {
                this.autorun(() => {
                    let partnerContacts: Array<IPartnerContact> = GroundPartnerContactCollection.find({
                        userId: this.user._id,
                        "partners.partnerTypeCode": Enums.PARTNER_TYPE.ZSCH
                    }, {
                        sort: {
                            name: 1
                        }
                    }).fetch();
                    console.log("partnerContacts: ", partnerContacts.length);
                    if (partnerContacts) {
                        let organizations: Array<OrganizationModel> = [];
                        partnerContacts.forEach((partnerContact: IPartnerContact) => {
                            organizations.push(this.programAgreementService.createOrganizationModelFromPartnerContact(partnerContact));
                        });
                        this.organizations = organizations;
                        this.setItems();
                    }
                });
            }
        });
    }

    public setItems(): void {
        this.items = this.organizations || [];
    }

    public filterItems(event): void {
        // Reset items back to all of the items
        this.setItems();

        // get the input value
        let value = event.target.value;

        // if the value is an empty string don't filter the items
        if (value && value.trim() != '') {
            this.items = this.items.filter((item) => {
                return (item.name.toLowerCase().indexOf(value.toLowerCase()) > -1);
            });
        }
    }

    public navPush(organization: OrganizationModel): void {
        this.programAgreementService.setProgramAgreement(new ProgramAgreementReviewModel());
        if (organization) {
            this.programAgreementService.getProgramAgreement().organization = organization;
        }
        this.nav.push(OrganizationInfoPage);
    }
}