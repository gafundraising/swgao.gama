/**
 * Created by mjwheatley on 7/20/16.
 */
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NavController, ViewController} from "ionic-angular/es2015";
import {TranslateService} from "@ngx-translate/core";
import {GroupListPage} from "../../group/group-list/group-list";
import {GroupInfoPage} from "../../group/group-info/group-info";
import {OrganizationInfoPage} from "../../organization/org-info/org-info";
import {ProgramPage} from "../../program/program";
import {BrochurePage} from "../../brochure/brochure";
import {ProductServicesPage} from "../../product-services/product-services";
import {InvoiceOtherPage} from "../../invoice-other/invoice-other";
import {EditReviewPage} from "../../edit-review/edit-review";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {IProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";

@Component({
    selector: "contract-navigation-toolbar",
    templateUrl: "contract-navigation-toolbar.html"
})
export class ContractNavigationToolbarComponent {
    @Input() nextStepEnabled: boolean = true;
    @Output() nextStepButton_onClicked: EventEmitter<boolean> = new EventEmitter();
    @Input() previousStepEnabled: boolean = true;
    @Output() previousStepButton_onClicked: EventEmitter<boolean> = new EventEmitter();
    public showPrevious: boolean = false;
    public currentPage: any;

    public previousPage: any = {
        "GroupInfoPage": OrganizationInfoPage,
        "ProgramPage": GroupInfoPage,
        "BrochurePage": ProgramPage,
        "ProductServicesPage": BrochurePage,
        "InventoryOtherPage": ProductServicesPage
    };

    public nextPage: any = {
        "OrganizationInfoPage": GroupListPage,
        "GroupInfoPage": ProgramPage,
        "ProgramPage": BrochurePage,
        "BrochurePage": ProductServicesPage,
        "ProductServicesPage": InvoiceOtherPage,
        "InvoiceOtherPage": EditReviewPage
    };

    constructor(public nav: NavController,
                public view: ViewController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {

    }

    ngOnInit(): void {
        this.currentPage = this.view.component;
        if (this.currentPage == OrganizationInfoPage) {
            this.showPrevious = false;
        }

        let programAgreement: IProgramAgreementReviewModel = this.programAgreementService.getProgramAgreement();
        if (programAgreement.group.partnerContact && programAgreement.group.partnerContact.guid) {
            this.nextPage.OrganizationInfoPage = GroupInfoPage;
        }
    }

    public previousStep(): void {
        this.nav.push(this.previousPage[this.currentPage.name]);
        this.previousStepButton_onClicked.emit(true);
    }

    public nextStep(): void {
        let setRoot: boolean = false;
        if (this.currentPage === InvoiceOtherPage) {
            setRoot = true;
        }
        if (setRoot) {
            this.nav.setRoot(this.nextPage[this.currentPage.name]);
        } else {
            this.nav.push(this.nextPage[this.currentPage.name]);
        }
        this.nextStepButton_onClicked.emit(true);
    }
}

export interface IContractNavigationToolbarComponent {
    nextStepButton_onClicked($event: boolean): void;
    previousStepButton_onClicked($event: boolean): void;
}