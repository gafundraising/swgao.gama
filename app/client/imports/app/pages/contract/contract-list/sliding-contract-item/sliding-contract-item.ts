/**
 * Created by mjwheatley on 4/16/16.
 */
import {Component, Input} from '@angular/core';
import {NavController} from "ionic-angular/es2015";
import {TranslateService} from "@ngx-translate/core";
import {ProgramAgreementService} from "../../../../services/ProgramAgreement.service";
import {ProgramAgreementReviewModel} from "../../../../../../../both/models/ProgramAgreementReviewModel";
import {EditReviewPage} from "../../edit-review/edit-review";
import {Enums} from "../../../../../../../both/Enums";

@Component({
    selector: 'sliding-contract-item',
    templateUrl: "sliding-contract-item.html"
})
export class SlidingContractItemComponent {
    @Input() contract: ProgramAgreementReviewModel;
    public PROGRAM_AGREEMENT_STATE_DESCRIPTION: any = Enums.PROGRAM_AGREEMENT_STATE_DESCRIPTION;
    public pushPage: any = EditReviewPage;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
    }

    ngOnInit() {
    }

    public navPush(contract: ProgramAgreementReviewModel): void {
        this.programAgreementService.setProgramAgreement(contract);
        this.nav.push(this.pushPage);
    }

    public remove(contractId: string): void {
        this.programAgreementService.remove(contractId);
    }
}