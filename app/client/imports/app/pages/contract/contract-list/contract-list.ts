import {Component, NgZone} from '@angular/core';
import {AlertController, NavController, Platform} from "ionic-angular/es2015";
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../both/Constants";
import {OrganizationListPage} from "../organization/organization-list/organization-list";
import {
    IProgramAgreementReviewModel,
    ProgramAgreementReviewModel
} from "../../../../../../both/models/ProgramAgreementReviewModel";
import {
    GroundProgramAgreementCollection,
    GroundTempProgramAgreementCollection
} from "../../../../../../both/collections/ProgramAgreementCollection";
import {GroundConfigurationCollection} from "../../../../../../both/collections/GroundConfigurationCollection";
import {IConfigurationModel, ISeason} from "../../../../../../both/models/ConfigurationModel";
import * as moment from "moment";
import {IPartnerContact, IProgramAgreement} from "../../../../../../both/models/ProgramAgreementDownloadResponseModel";
import {
    GroundPartnerContactCollection,
    GroundTempPartnerContactCollection
} from "../../../../../../both/collections/GroundPartnerContactCollection";
import {ProgramAgreementService} from "../../../services/ProgramAgreement.service";

declare var Session;
declare var device;

@Component({
    selector: "page-contract-list",
    templateUrl: "contract-list.html"
})
export class ContractListPage extends MeteorComponent {
    public user: Meteor.User;
    public contractType: any = Constants.CONTRACT_STATUS;
    public seasonList: Array<ISeason> = [];
    public contractFilter = Constants.CONTRACT_STATUS.ALL;
    public items: Array<ProgramAgreementReviewModel>;
    public showSearchCancel: boolean = false;
    public searchQuery: string = Constants.EMPTY_STRING;
    public selectedSeason: string;
    public programAgreements: Array<IProgramAgreement>;
    public partnerContacts: Array<IPartnerContact>;
    public tempProgramAgreements: Array<IProgramAgreement>;
    public tempPartnerContacts: Array<IPartnerContact>;
    public toolbarColor: string;
    public segmentColor: string;
    public configuration: IConfigurationModel;
    public contractCount: number;
    public initSelectedSeason: boolean = true;
    public sortDescending: boolean = Session.get(Constants.SESSION.SORT_CONTRACTS_DESCENDING);
    public CATEGORIES: any = {
        GROUP_NAME: "groupName",
        START_DATE: "startDate",
        WHOLESALE: "wholesale"
    };
    public selectedCategory: string = this.CATEGORIES.GROUP_NAME;

    constructor(public nav: NavController,
                public platform: Platform,
                public alertCtrl: AlertController,
                public zone: NgZone,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService) {
        super();
    }

    ngOnInit(): void {
        this.toolbarColor = this.platform.is('android') ? Constants.EMPTY_STRING : null;
        this.segmentColor = this.platform.is('android') ? Constants.EMPTY_STRING : null;
        this.autorun(() => {
            this.user = Meteor.user();
            if (this.user && this.user.profile.accountNumber) {
                this.autorun(() => {
                    let programAgreements: Array<IProgramAgreement> = GroundProgramAgreementCollection.find({
                        userId: this.user._id
                    }).fetch();
                    console.log("programAgreements: ", programAgreements.length);
                    this.programAgreements = programAgreements;
                    this.filterItems();
                });
                this.autorun(() => {
                    let partnerContacts: Array<IPartnerContact> = GroundPartnerContactCollection.find({
                        userId: this.user._id
                    }).fetch();
                    console.log("partnerContacts: ", partnerContacts.length);
                    this.partnerContacts = partnerContacts;
                    this.filterItems();
                });

                this.autorun(() => {
                    let programAgreements: Array<IProgramAgreement> = GroundTempProgramAgreementCollection.find({
                        userId: this.user._id
                    }).fetch();
                    console.log("tempProgramAgreements: ", programAgreements.length);
                    this.tempProgramAgreements = programAgreements;
                    this.filterItems();
                });
                this.autorun(() => {
                    let partnerContacts: Array<IPartnerContact> = GroundTempPartnerContactCollection.find({
                        userId: this.user._id
                    }).fetch();
                    console.log("tempPartnerContacts: ", partnerContacts.length);
                    this.tempPartnerContacts = partnerContacts;
                    this.filterItems();
                });
            }
        });

        this.autorun(() => {
            if (this.user) {
                this.configuration = GroundConfigurationCollection.findOne({
                    salesRepAccountNumber: this.user.profile.accountNumber
                });
                if (this.configuration) {
                    this.seasonList = this.configuration.seasons;
                    if (this.initSelectedSeason) {
                        this.initSelectedSeason = false;
                        console.log("init selected season...");
                        if (Session.get(Constants.SESSION.SELECTED_SEASON)) {
                            this.selectedSeason = Session.get(Constants.SESSION.SELECTED_SEASON);
                        } else {
                            this.seasonList.forEach((season: ISeason) => {
                                if (Session.get(Constants.SESSION.SELECTED_SEASON)) {
                                    this.selectedSeason = season.descriptor;
                                } else {
                                    let now = moment();
                                    if (now > moment(season.start) && now < moment(season.end)) {
                                        this.selectedSeason = season.descriptor;
                                    }
                                }
                            });
                        }
                        this.filterItems();
                    }
                }
            }
        });

        if (Meteor.status().connected) {
            let info: any = {
                platform: this.platform.platforms()[0],
                version: this.platform.userAgent()
            };
            if (Meteor.isCordova) {
                info.platform = device.platform;
                info.version = device.version;
            }
            // Meteor.call("/api/programagreement/download", {
            //     accountNumber: this.user.profile.accountNumber,
            //     agentInfo: info
            // }, (error, result) => {
            //     if (error) {
            //         console.error("Error downloading contracts: " + JSON.stringify(error));
            //     } else {
            //         console.log("Download contracts result: " + JSON.stringify(result));
            //         this.contractCount = result.programAgreements;
            //     }
            // });
        }
    }

    public setItems(): void {
        Session.setPersistent(Constants.SESSION.SELECTED_SEASON, this.selectedSeason);

        // Merge temp data
        if (this.tempProgramAgreements) {
            this.tempProgramAgreements.forEach((tempContract: IProgramAgreement) => {
                let reference: number;
                this.programAgreements.find((contract: IProgramAgreement, index: number) => {
                    let isMatch: boolean = false;
                    if (contract.guid === tempContract.guid) {
                        reference = index;
                        isMatch = true;
                    }
                    return isMatch;
                });
                if (reference) {
                    this.programAgreements[reference] = tempContract;
                } else {
                    this.programAgreements.push(tempContract);
                }
            });
        }

        if (this.tempPartnerContacts) {
            this.tempPartnerContacts.forEach((tempContact: IPartnerContact) => {
                let reference: number;
                this.partnerContacts.find((contact: IPartnerContact, index: number) => {
                    let isMatch: boolean = false;
                    if (contact.guid === tempContact.guid) {
                        reference = index;
                        isMatch = true;
                    }
                    return isMatch;
                });
                if (reference) {
                    this.partnerContacts[reference] = tempContact;
                } else {
                    this.partnerContacts.push(tempContact);
                }
            });
        }

        this.items = this.programAgreementService.constructProgramAgreementReviewModels({
            programAgreements: this.programAgreements,
            partnerContacts: this.partnerContacts,
            configuration: this.configuration
        });
    }

    public getItems(event: any): void {
        this.searchQuery = event.target.value;
        this.filterItems();
    }

    public filterItems(): void {
        // Reset items back to all of the items
        this.setItems();

        this.items = this.items.filter((item) => {
            return (item.season === this.selectedSeason);
        });

        if (this.contractFilter !== Constants.CONTRACT_STATUS.ALL) {
            this.items = this.items.filter((item) => {
                return (item.status === this.contractFilter);
            });
        }

        // get the input value
        let value = this.searchQuery;

        // if the value is an empty string don't filter the items
        if (value && value.trim() != Constants.EMPTY_STRING) {
            this.items = this.items.filter((item) => {
                return (item.searchable.toLowerCase().indexOf(value.toLowerCase()) > -1);
            });
        }
        this.sortContracts();
    }

    public selectSeason(): void {
        let self = this;
        let inputs: Array<any> = [];
        this.seasonList.forEach((season: ISeason) => {
            let input: any = {
                type: 'radio',
                label: season.descriptor,
                value: season.descriptor,
                checked: (self.selectedSeason === season.descriptor)
            };
            inputs.push(input);
        });

        let alert = self.alertCtrl.create({
            title: self.translate.instant("contract-list.season"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.ok"),
                handler: (selectedValue: string) => {
                    self.selectedSeason = selectedValue;
                    Session.setPersistent(Constants.SESSION.SELECTED_SEASON, self.selectedSeason);
                    self.filterItems();
                }
            }]
        });
        alert.present();
    }

    public selectSortCategory(): void {
        let self = this;
        let categories: Array<string> = Object.keys(self.CATEGORIES);
        let inputs: Array<any> = [];
        categories.forEach((category: string) => {
            let input: any = {
                type: 'radio',
                label: self.translate.instant("contract-list.sortCategories." + self.CATEGORIES[category]),
                value: self.CATEGORIES[category],
                checked: (self.selectedCategory === self.CATEGORIES[category])
            };
            inputs.push(input);
        });

        let alert = self.alertCtrl.create({
            title: self.translate.instant("contract-list.alerts.selectCategory.title"),
            inputs: inputs,
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.ok"),
                handler: (selectedValue: string) => {
                    self.selectedCategory = selectedValue;
                    Session.setPersistent(Constants.SESSION.SORT_CATEGORY, self.selectedCategory);
                    self.sortContracts();
                }
            }]
        });
        alert.present();
    }

    public toggleSortDirection(): void {
        this.zone.run(() => {
            this.sortDescending = !this.sortDescending;
            Session.setPersistent(Constants.SESSION.SORT_PROGRAMS_DESCENDING, this.sortDescending);
            this.sortContracts();
        });
    }

    private sortContracts(): void {
        this.zone.run(() => {
            switch (this.selectedCategory) {
                case this.CATEGORIES.START_DATE:
                    this.items.sort(this.sortByDate);
                    break;
                case this.CATEGORIES.GROUP_NAME:
                    this.items.sort(this.sortByName);
                    break;
                case this.CATEGORIES.WHOLESALE:
                    this.items.sort(this.sortByWholesale);
                    break;
            }

            if (this.sortDescending) {
                this.items.reverse();
            }
        });
    }

    private sortByName(a: IProgramAgreementReviewModel, b: IProgramAgreementReviewModel): number {
        if (!a.group.name && !b.group.name) {
            return 0;
        } else if (!a.group.name && b.group.name) {
            return -1;
        } else if (a.group.name && !b.group.name) {
            return 1;
        } else if (a.group.name.toLowerCase().trim() > b.group.name.toLowerCase().trim()) {
            return 1;
        } else if (a.group.name.toLowerCase() < b.group.name.toLowerCase()) {
            return -1;
        } else {
            return 0;
        }
    }

    private sortByDate(a: IProgramAgreementReviewModel, b: IProgramAgreementReviewModel): number {
        if (!a.program.startDate && !b.program.startDate) {
            return 0;
        } else if (!a.program.startDate && b.program.startDate) {
            return -1;
        } else if (a.program.startDate && !b.program.startDate) {
            return 1;
        } else {
            let aMoment = moment(a.program.startDate);
            let bMoment = moment(b.program.startDate);
            let diff = aMoment.diff(bMoment);
            if (diff > 0) {
                return 1;
            } else if (diff < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    private sortByWholesale(a: IProgramAgreementReviewModel, b: IProgramAgreementReviewModel): number {
        if (!a.program.estimatedWholesale && !b.program.estimatedWholesale) {
            return 0;
        } else if (!a.program.estimatedWholesale && b.program.estimatedWholesale) {
            return -1;
        } else if (a.program.estimatedWholesale && !b.program.estimatedWholesale) {
            return 1;
        } else if (a.program.estimatedWholesale > b.program.estimatedWholesale) {
            return 1;
        } else if (a.program.estimatedWholesale < b.program.estimatedWholesale) {
            return -1;
        } else {
            return 0;
        }
    }

    public startCreateContract(): void {
        this.nav.push(OrganizationListPage);
    }
}