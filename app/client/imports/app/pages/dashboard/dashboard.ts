import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {MeteorComponent} from 'angular2-meteor';
import {Meteor} from "meteor/meteor";
import {Alert, AlertController, NavController, Platform} from "ionic-angular/es2015";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {ToastMessenger} from "../../utils/ToastMessenger";
import {IConfigurationModel, IProfitSalesLevel} from "../../../../../both/models/ConfigurationModel";
import {GroundConfigurationCollection} from "../../../../../both/collections/GroundConfigurationCollection";

declare var device;
declare var ProgressBar;
declare var Chart;

@Component({
    selector: "page-dashboard",
    templateUrl: "dashboard.html"
})
export class DashboardPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public configuration: IConfigurationModel;

    @ViewChild('contractsChartElement') set setContractsChartElement(content: ElementRef) {
        this.contractsChartElement = content;
        this.configureContractsChart();
    };

    @ViewChild('bonusBaseProgressElement') set setBonusBaseProgressElement(content: ElementRef) {
        this.bonusBaseProgressElement = content;
        this.configureBonusBaseProgressBar();
    };

    @ViewChild('incentiveTripProgressElement') set setIncentiveTripProgressElement(content: ElementRef) {
        this.incentiveTripProgressElement = content;
        this.configureIncentiveTripProgressBar();
    };

    @ViewChild('salesClubProgressElement') set setSalesClubProgressElement(content: ElementRef) {
        this.salesClubProgressElement = content;
        this.configureSalesClubProgressBar();
    };

    @ViewChild('profitabilitySalaryProgressElement') set setProfitabilitySalaryElement(content: ElementRef) {
        this.profitabilitySalaryProgressElement = content;
        this.configureProfitabilitySalaryProgressBar();
    };

    public contractsChartElement: ElementRef;
    public bonusBaseProgressElement: ElementRef;
    public incentiveTripProgressElement: ElementRef;
    public salesClubProgressElement: ElementRef;
    public profitabilitySalaryProgressElement: ElementRef;

    public contractsChart: any;
    public bonusBaseProgressBar: any;
    public incentiveTripProgressBar: any;
    public salesClubProgressBar: any;
    public profitabilitySalaryProgressBar: any;


    public bonusBaseProgressInfo: IProgressInfo = {
        color: "#00e500"
    };
    public incentiveTripProgressInfo: IProgressInfo = {
        color: "#00e500"
    };
    public salesClubProgressInfo: IProgressInfo = {
        color: "#00e500"
    };
    public profitabilitySalaryProgressInfo: IProgressInfo = {
        color: "#00e500"
    };

    public selectedProfitSegmentControl: string = "0";
    public profitSegmentButtons: any = {
        current: true,
        next: true,
        plusTwo: true
    };

    constructor(public nav: NavController,
                public alertCtrl: AlertController,
                public platform: Platform,
                public zone: NgZone,
                public translate: TranslateService) {
        super();
    }

    ngOnInit(): void {
        this.autorun(() => {
            this.user = Session.get(Constants.SESSION.OFFLINE_USER);
            if (this.user && this.user.profile.accountNumber) {
                this.autorun(() => {
                    this.configuration = GroundConfigurationCollection.findOne({
                        salesRepAccountNumber: this.user.profile.accountNumber
                    });
                    if (!this.configuration) {
                        this.refreshConfiguration();
                    }
                    this.configureProgressBars();
                });
            }
        });
    }

    public doRefresh(refresher) {
        this.refreshConfiguration(refresher);
    }

    private refreshConfiguration(refresher?): void {
        let self = this;
        if (!Meteor.status().connected) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("general.noServerConnection")
            });
            return;
        }
        let info: any = {
            platform: this.platform.platforms()[0],
            version: this.platform.userAgent()
        };
        if (Meteor.isCordova) {
            info.platform = device.platform;
            info.version = device.version;
        }
        if (!refresher) {
            Session.set(Constants.SESSION.LOADING, true);
        }
        Meteor.call(Constants.GAO_ENDPOINTS.CONFIGURE, {
            accountNumber: this.user.profile.accountNumber,
            agentInfo: info
        }, (error, result) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (refresher) {
                refresher.complete();
            }
            if (error) {
                self.handleError(error);
            } else {
                self.zone.run(() => {
                    self.configuration = result.configuration;
                    self.configureProgressBars();
                });
            }
        });
    }

    private handleError(error: any): void {
        let self = this;
        let alert: Alert = self.alertCtrl.create({
            title: self.translate.instant("general.apiError"),
            message: error.reason || error.message || error,
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    private configureProgressBars(): void {
        if (this.configuration) {
            this.configureContractsChart();
            this.configureBonusBaseProgressBar();
            this.configureIncentiveTripProgressBar();
            this.configureSalesClubProgressBar();
            this.configureProfitabilitySalaryProgressBar();
        }
    }

    private configureContractsChart(): void {
        if (this.configuration && this.contractsChartElement) {
            let datasets: Array<any> = [{
                label: this.translate.instant("dashboard.current"),
                data: [
                    this.configuration.salesRepActivity.accountsSignedNowCurrentWeek,
                    this.configuration.salesRepActivity.accountsSignedTotalCurrentSeasonToDate,
                    this.configuration.salesRepActivity.accountsSignedTotalCurrentYearToDate
                ],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)', // green
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(75, 192, 192, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)', // green
                    'rgba(75, 192, 192, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1
            }, {
                label: this.translate.instant("dashboard.prior"),
                data: [
                    this.configuration.salesRepActivity.accountsSignedNowPriorWeek,
                    this.configuration.salesRepActivity.accountsSignedTotalPriorSeasonToDate,
                    this.configuration.salesRepActivity.accountsSignedTotalPriorYearToDate
                ],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)', // blue
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)', // blue
                    'rgba(54, 162, 235, 1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }];

            if (!this.contractsChart) {

                this.contractsChart = new Chart(this.contractsChartElement.nativeElement.getContext('2d'), {
                    type: 'bar',
                    data: {
                        labels: [
                            this.translate.instant("dashboard.week"),
                            this.translate.instant("dashboard.season"),
                            this.translate.instant("dashboard.year")
                        ],
                        datasets: datasets
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            } else {
                this.contractsChart.data.datasets.forEach((dataset, index) => {
                    dataset.data = datasets[index].data;
                });

                this.contractsChart.update();
            }
        }
    }

    private configureBonusBaseProgressBar(): void {
        if (this.configuration && this.bonusBaseProgressElement) {
            this.bonusBaseProgressInfo.current = this.configuration.salesRepActivity.growthBonusActual || 0;
            this.bonusBaseProgressInfo.goal = this.configuration.salesRepActivity.growthBonusGoal || 0;
            this.configureProgressBar({
                progressInfo: this.bonusBaseProgressInfo,
                progressElement: this.bonusBaseProgressElement
            });
        }
    }

    private configureIncentiveTripProgressBar(): void {
        if (this.configuration && this.incentiveTripProgressElement) {
            this.incentiveTripProgressInfo.current = this.configuration.salesRepActivity.incentiveTripActual || 0;
            this.incentiveTripProgressInfo.goal = this.configuration.salesRepActivity.incentiveTripGoal || 0;
            this.configureProgressBar({
                progressInfo: this.incentiveTripProgressInfo,
                progressElement: this.incentiveTripProgressElement
            });
        }
    }

    private configureProgressBar(data: { progressInfo: IProgressInfo, progressElement: ElementRef }): void {
        let progressPercent: number = (data.progressInfo.current / data.progressInfo.goal) || 0;
        data.progressInfo.percent = Math.floor(progressPercent * 100);
        data.progressInfo.progress = progressPercent;
        if (data.progressInfo.progress >= 1) {
            data.progressInfo.progress = 1;
        }

        if (!data.progressInfo.text) {
            data.progressInfo.text = "<div style='text-align:center; font-size:36px;'>" + data.progressInfo.percent + "%</div>";
        }

        let progressBar: any;
        switch (data.progressElement) {
            case this.bonusBaseProgressElement:
                if (!this.bonusBaseProgressBar) {
                    this.bonusBaseProgressBar = this.createProgressBar({
                        nativeElement: data.progressElement.nativeElement,
                        color: data.progressInfo.color,
                        text: data.progressInfo.text
                    })
                }
                progressBar = this.bonusBaseProgressBar;
                break;
            case this.incentiveTripProgressElement:
                if (!this.incentiveTripProgressBar) {
                    this.incentiveTripProgressBar = this.createProgressBar({
                        nativeElement: data.progressElement.nativeElement,
                        color: data.progressInfo.color,
                        text: data.progressInfo.text
                    })
                }
                progressBar = this.incentiveTripProgressBar;
                break;
            case this.salesClubProgressElement:
                if (!this.salesClubProgressBar) {
                    this.salesClubProgressBar = this.createProgressBar({
                        nativeElement: data.progressElement.nativeElement,
                        color: data.progressInfo.color,
                        text: data.progressInfo.text
                    })
                }
                progressBar = this.salesClubProgressBar;
                break;
            case this.profitabilitySalaryProgressElement:
                if (!this.profitabilitySalaryProgressBar) {
                    this.profitabilitySalaryProgressBar = this.createProgressBar({
                        nativeElement: data.progressElement.nativeElement,
                        color: data.progressInfo.color,
                        text: data.progressInfo.text
                    })
                }
                progressBar = this.profitabilitySalaryProgressBar;
                break;
        }

        this.zone.run(() => {
            if (progressBar) {
                progressBar.setText(data.progressInfo.text);
                progressBar.animate(data.progressInfo.progress);
            }
        });
    }

    private createProgressBar(data: { nativeElement: any, color: string, text: string }): any {
        return new ProgressBar.Circle(data.nativeElement, {
                color: data.color,
                trailColor: '#FFEA82',
                trailWidth: 4,
                duration: 1400,
                easing: 'bounce',
                strokeWidth: 12,
                text: {
                    value: data.text,
                    style: {
                        color: data.color,
                        position: 'absolute',
                        left: '50%',
                        top: '50%',
                        padding: 0,
                        margin: 0,
                        // You can specify styles which will be browser prefixed
                        transform: {
                            prefix: true,
                            value: 'translate(-50%, -50%)'
                        }
                    }
                }
            }
        );
    }

    private configureSalesClubProgressBar(): void {
        if (this.configuration && this.salesClubProgressElement) {
            let progressInfo: any = this.salesClubProgressInfo;
            let salesClubGoals: any = {
                milestone: this.configuration.salesRepActivity.milestoneClubGoal,
                presidents: this.configuration.salesRepActivity.presidentsClubGoal,
                chairmans: this.configuration.salesRepActivity.chairmansClubGoal,
                chairmansGold: this.configuration.salesRepActivity.chairmansGoldClubGoal,
                chairmansPlatinum: this.configuration.salesRepActivity.chairmansPlatinumClubGoal,
                chairmansElite: this.configuration.salesRepActivity.chairmansEliteClubGoal
            };

            let currentSalesValue: number = Number(this.configuration.salesRepActivity.clubSalesActual || 0);

            progressInfo.salesClubs = [];
            let salesClubNames: any = this.translate.instant("dashboard.salesClubNames");
            let salesClubKeys: Array<string> = Object.keys(salesClubNames);
            let currentSalesClubName: string = Constants.EMPTY_STRING;
            let currentSalesClubRange: number = 0;

            salesClubKeys.forEach((club: string) => {
                let salesClub: any = {
                    name: salesClubNames[club],
                    isComplete: (currentSalesValue >= salesClubGoals[club])
                };
                progressInfo.salesClubs.push(salesClub);
            });

            if (currentSalesValue <= salesClubGoals.milestone) {
                currentSalesClubName = salesClubNames.milestone;
                currentSalesClubRange = salesClubGoals.milestone;
            } else if (currentSalesValue > salesClubGoals.milestone && currentSalesValue <= salesClubGoals.presidents) {
                currentSalesClubName = salesClubNames.presidents;
                currentSalesClubRange = salesClubGoals.presidents;
            } else if (currentSalesValue > salesClubGoals.presidents && currentSalesValue <= salesClubGoals.chairmans) {
                currentSalesClubName = salesClubNames.chairmans;
                currentSalesClubRange = salesClubGoals.chairmans;
            } else if (currentSalesValue > salesClubGoals.chairmans && currentSalesValue <= salesClubGoals.chairmansGold) {
                currentSalesClubName = salesClubNames.chairmansGold;
                currentSalesClubRange = salesClubGoals.chairmansGold;
            } else if (currentSalesValue > salesClubGoals.chairmansGold && currentSalesValue <= salesClubGoals.chairmansPlatinum) {
                currentSalesClubName = salesClubNames.chairmansPlatinum;
                currentSalesClubRange = salesClubGoals.chairmansPlatinum;
            } else if (currentSalesValue > salesClubGoals.chairmansPlatinum) {
                currentSalesClubName = salesClubNames.chairmansElite;
                currentSalesClubRange = salesClubGoals.chairmansElite;
            }

            progressInfo.current = currentSalesValue;
            progressInfo.goal = Number(currentSalesClubRange || 0);
            progressInfo.title = currentSalesClubName;
            this.salesClubProgressInfo = progressInfo;

            this.configureProgressBar({
                progressInfo: this.salesClubProgressInfo,
                progressElement: this.salesClubProgressElement
            });
        }
    }

    private configureProfitabilitySalaryProgressBar(): void {
        if (this.configuration && this.profitabilitySalaryProgressElement) {
            let profitabilityProgressInfo: any = this.profitabilitySalaryProgressInfo;
            let profitSales: number = this.configuration.salesRepActivity.profitabilitySalesActual;
            let profitSalesLevels: Array<IProfitSalesLevel> = this.configuration.profitSalesLevels;

            let profitSalaryEarned: number = 0;
            let currentProfitSalesLevelIndex: number = 0;

            for (let i = 0; i < profitSalesLevels.length; i++) {
                if (i + 1 < profitSalesLevels.length) {
                    if (profitSales >= profitSalesLevels[i].salesLevel && profitSales < profitSalesLevels[i + 1].salesLevel) {
                        currentProfitSalesLevelIndex = i + 1;
                    }
                } else {
                    if (profitSales >= profitSalesLevels[i - 1].salesLevel) {
                        currentProfitSalesLevelIndex = i;
                    }
                }
            }

            if (currentProfitSalesLevelIndex > 0) {
                profitSalaryEarned = profitSalesLevels[currentProfitSalesLevelIndex - 1].salaryEarned;
            }

            let selectedProfitSalesLevelIndex = currentProfitSalesLevelIndex + Number(this.selectedProfitSegmentControl);
            let selectedProfitSalesLevel = profitSalesLevels[selectedProfitSalesLevelIndex];

            let progressPercent: number = (profitSales / selectedProfitSalesLevel.salesLevel) || 0;
            if (progressPercent >= 0.995) {
                progressPercent = 1;
            }
            if (progressPercent == 1) {
                profitSalaryEarned = selectedProfitSalesLevel.salaryEarned;
            }

            profitabilityProgressInfo.current = profitSales;
            profitabilityProgressInfo.goal = selectedProfitSalesLevel.salesLevel;
            profitabilityProgressInfo.earned = profitSalaryEarned;
            profitabilityProgressInfo.next = selectedProfitSalesLevel.salaryEarned;
            profitabilityProgressInfo.percent = Math.floor(progressPercent * 100);
            profitabilityProgressInfo.text = "<div style='text-align:center; font-size:36px;'>"
                + profitabilityProgressInfo.percent + "%</div>"
                + "<div style='text-align:center; font-size:22px;'>$"
                + profitabilityProgressInfo.next.toLocaleString()
                + "</div>";

            this.profitabilitySalaryProgressInfo = profitabilityProgressInfo;

            this.configureProgressBar({
                progressInfo: this.profitabilitySalaryProgressInfo,
                progressElement: this.profitabilitySalaryProgressElement
            });

            // segment buttons
            if (currentProfitSalesLevelIndex < profitSalesLevels.length - 1) {
                this.profitSegmentButtons.current = true;
            }
            if (currentProfitSalesLevelIndex <= profitSalesLevels.length - 2) {
                this.profitSegmentButtons.next = true;
            }
            if (currentProfitSalesLevelIndex <= profitSalesLevels.length - 3) {
                this.profitSegmentButtons.plusTwo = true;
            }
        }
    }

    public onProfitSegmentSelected(): void {
        this.configureProfitabilitySalaryProgressBar();
    }
}

interface IProgressInfo {
    imageUri?: string,
    color?: string,
    current?: number,
    goal?: number,
    percent?: number,
    progress?: number,
    text?: string
}
