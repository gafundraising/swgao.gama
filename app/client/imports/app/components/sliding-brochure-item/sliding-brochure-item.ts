/**
 * Created by mjwheatley on 8/13/18.
 */
import {Component, Input} from '@angular/core';
import {NavController} from "ionic-angular/es2015";
import {TranslateService} from "@ngx-translate/core";
import {ProgramAgreementService} from "../../services/ProgramAgreement.service";
import {BrochureModel} from "../../../../../both/models/BrochureModel";

@Component({
    selector: 'sliding-brochure-item',
    templateUrl: "sliding-brochure-item.html"
})
export class SlidingBrochureItemComponent {
    @Input() brochure: BrochureModel;

    constructor(public nav: NavController,
                public translate: TranslateService,
                public programAgreementService: ProgramAgreementService
    ) {
    }

    ngOnInit() {
    }
}