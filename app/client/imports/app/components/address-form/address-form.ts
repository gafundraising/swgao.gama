/**
 * Created by mjwheatley on 3/22/16.
 */
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {FormValidator} from "../../utils/FormValidator";
import {Address, PartnerContactAddress} from "../../../../../both/models/Address";
import {Constants} from "../../../../../both/Constants";

@Component({
    selector: "address-form",
    templateUrl: "address-form.html"
})
export class AddressFormComponent {
    @Input() address:PartnerContactAddress;
    @Input() formName:string;
    @Input() readOnly:boolean;
    @Output() onAddressFormValid:EventEmitter<{formName:string, isValid:boolean}> = new EventEmitter();
    public addressForm:FormGroup;
    public addressControl:{
        streetAddress:AbstractControl,
        locality:AbstractControl,
        region:AbstractControl,
        postalCode:AbstractControl
    };

    constructor(public fb:FormBuilder) {
    }

    ngOnInit() {
        this.addressForm = this.fb.group({
            'streetAddress': [{value: Constants.EMPTY_STRING, disabled: this.readOnly}, Validators.compose([Validators.required])],
            'locality': [{value: Constants.EMPTY_STRING, disabled: this.readOnly}, Validators.compose([Validators.required])],
            'region': [{value: Constants.EMPTY_STRING, disabled: this.readOnly}, Validators.compose([Validators.required, FormValidator.validAddressRegion])],
            'postalCode': [{value: Constants.EMPTY_STRING, disabled: this.readOnly}, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5)])]
        });
        this.addressControl = {
            streetAddress: this.addressForm.controls['streetAddress'],
            locality: this.addressForm.controls['locality'],
            region: this.addressForm.controls['region'],
            postalCode: this.addressForm.controls['postalCode']
        };

        // Report to parent if this form is valid
        this.addressForm.valueChanges.subscribe(data => {
            Session.set(this.formName, this.addressForm.valid);
        });
    }
}

export interface IAddressFormComponent {
    onAddressFormComponentStateChanged($event:{formName:string, isValid:boolean}):void
}