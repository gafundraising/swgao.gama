import { NgModule, ErrorHandler } from "@angular/core";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular/es2015";
import { Storage } from '@ionic/storage';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {TranslateModule, TranslateLoader} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {HttpClientModule, HttpClient} from "@angular/common/http";
import { Constants } from "../../../both/Constants";
import { AppComponent } from "./app.component";
import { LoginPage } from "./pages/login/login";
import {LanguageSelectComponent} from "./components/language-select/language-select";
import {DashboardPage} from "./pages/dashboard/dashboard";
import {ContractNextStepButtonComponent} from "./pages/contract/components/contract-next-step-button/contract-next-step-button";
import {AddressFormComponent} from "./components/address-form/address-form";
import {ActivityEntryPage} from "./pages/activity-entry/activity-entry";
import {BrochurePage} from "./pages/contract/brochure/brochure";
import {SlidingContractItemComponent} from "./pages/contract/contract-list/sliding-contract-item/sliding-contract-item";
import {ContractListPage} from "./pages/contract/contract-list/contract-list";
import {EditReviewPage} from "./pages/contract/edit-review/edit-review";
import {GroupInfoPage} from "./pages/contract/group/group-info/group-info";
import {GroupListPage} from "./pages/contract/group/group-list/group-list";
import {InvoiceOtherPage} from "./pages/contract/invoice-other/invoice-other";
import {OrganizationInfoPage} from "./pages/contract/organization/org-info/org-info";
import {OrganizationListPage} from "./pages/contract/organization/organization-list/organization-list";
import {ProductServicesPage} from "./pages/contract/product-services/product-services";
import {ProgramPage} from "./pages/contract/program/program";
import {ProgramAgreementService} from "./services/ProgramAgreement.service";
import {NewPagePage} from "./pages/newpage/newpage";
import {LandingPage} from "./pages/landing/landing";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {Device} from "@ionic-native/device";
import {Camera} from "@ionic-native/camera";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {HierarchyPage} from "./pages/hierarchy/hierarchy";
import {MomentPipe} from "./pipes/moment.pipe";
import {CaptureSignaturePage} from "./pages/contract/edit-review/capture-signature/capture-signature";
import {SlidingBrochureItemComponent} from "./components/sliding-brochure-item/sliding-brochure-item";
import {ContractNavigationToolbarComponent} from "./pages/contract/components/contract-navigation-toolbar/contract-navigation-toolbar";
import {BrochureEntryPage} from './pages/contract/brochure/brochure-entry/brochure-entry';

@NgModule({
    // Components, Pipes, Directive
    declarations: [
        AppComponent,
        LandingPage,
        NewPagePage,
        LoginPage,
        LanguageSelectComponent,
        DashboardPage,
        ContractNextStepButtonComponent,
        ContractNavigationToolbarComponent,
        AddressFormComponent,
        ActivityEntryPage,
        BrochurePage,
        SlidingContractItemComponent,
        ContractListPage,
        EditReviewPage,
        GroupInfoPage,
        GroupListPage,
        InvoiceOtherPage,
        OrganizationInfoPage,
        OrganizationListPage,
        ProductServicesPage,
        ProgramPage,
        HierarchyPage,
        MomentPipe,
        CaptureSignaturePage,
        SlidingBrochureItemComponent,
        BrochureEntryPage
    ],
    // Entry Components
    entryComponents: [
        AppComponent,
        LandingPage,
        NewPagePage,
        LoginPage,
        DashboardPage,
        AppComponent,
        LoginPage,
        LanguageSelectComponent,
        DashboardPage,
        ContractNextStepButtonComponent,
        AddressFormComponent,
        ActivityEntryPage,
        BrochurePage,
        SlidingContractItemComponent,
        ContractListPage,
        EditReviewPage,
        GroupInfoPage,
        GroupListPage,
        InvoiceOtherPage,
        OrganizationInfoPage,
        OrganizationListPage,
        ProductServicesPage,
        ProgramPage,
        HierarchyPage,
        CaptureSignaturePage,
        BrochureEntryPage
    ],
    // Providers
    providers: [
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        },
        ProgramAgreementService,
        SplashScreen,
        StatusBar,
        Device,
        Camera,
        AndroidPermissions
    ],
    // Modules
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        IonicModule.forRoot(AppComponent, {
            //// http://ionicframework.com/docs/v2/api/config/Config/
            //mode: Constants.STYLE.MD,
            //pageTransition: Constants.STYLE.IOS,
            //tabbarPlacement: 'top',
            swipeBackEnabled: false
        })
    ],
    // Main Component
    bootstrap: [IonicApp]
})

export class AppModule {
    constructor() {

    }
}

export function createTranslateLoader(http:HttpClient) {
    return new TranslateHttpLoader(http, '/i18n/', '.json');
}
