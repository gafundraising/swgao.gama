/**
 * Created by mjwheatley on 4/14/16.
 */
import {Injectable} from '@angular/core';
import {ProgramAgreement, ProgramAgreementReviewModel} from "../../../../both/models/ProgramAgreementReviewModel";
import {
    GroundProgramAgreementCollection,
    GroundTempProgramAgreementCollection
} from "../../../../both/collections/ProgramAgreementCollection";
import {GroupModel} from "../../../../both/models/GroupModel";
import {
    IPartner,
    IPartnerAttribute,
    IPartnerContact,
    IPartnerContactAddress,
    IPartnerContactPhone,
    IProgramAgreement,
    IProgramAgreementNote,
    IProgramAgreementPartner,
    IProgramAgreementProductLine,
    PartnerContact,
    ProgramAgreementNote
} from "../../../../both/models/ProgramAgreementDownloadResponseModel";
import {SponsorModel} from "../../../../both/models/SponsorModel";
import {PartnerContactAddress} from "../../../../both/models/Address";
import {Enums} from "../../../../both/Enums";
import {Constants} from "../../../../both/Constants";
import {OrganizationModel} from "../../../../both/models/OrganizationModel";
import {InvoiceOtherModel} from "../../../../both/models/InvoiceOtherModel";
import {IConfigurationModel, IKeyText, ISeason, ITaxOnPaperworkRule} from "../../../../both/models/ConfigurationModel";
import {ProgramModel} from "../../../../both/models/ProgramModel";
import * as moment from "moment";
import {BrochureModel} from "../../../../both/models/BrochureModel";
import {ProductServicesModel} from "../../../../both/models/ProductServicesModel";
import {GroundTempPartnerContactCollection} from "../../../../both/collections/GroundPartnerContactCollection";

@Injectable()
export class ProgramAgreementService {
    private _programAgreement: ProgramAgreementReviewModel = new ProgramAgreementReviewModel();

    public getProgramAgreement(): ProgramAgreementReviewModel {
        return this._programAgreement;
    }

    public setProgramAgreement(programAgreement: ProgramAgreementReviewModel): void {
        this._programAgreement = programAgreement;
    }

    public upsert(): void {
        this._programAgreement.modified = moment().toISOString();
        let programAgreement: ProgramAgreement = new ProgramAgreement(this._programAgreement);
        delete programAgreement._id;
        let tempExists: boolean = GroundTempProgramAgreementCollection.find({guid: programAgreement.guid}).count() > 0;
        if (this._programAgreement._id && tempExists) {
            GroundTempProgramAgreementCollection.update(this._programAgreement._id, {
                $set: programAgreement
            }, {
                multi: false,
                upsert: false
            }, (error, result) => {
                if (error) {
                    console.log("error: ", error);
                } else {
                    console.log("Successfully updated " + result + " ProgramAgreement");
                }
            });
        } else {
            GroundTempProgramAgreementCollection.insert(programAgreement, (error, result) => {
                if (error) {
                    console.error("Error inserting program agreement: ", error);
                } else {
                    console.log("Successfully inserted new ProgramAgreement: ", result);
                    this._programAgreement._id = result;
                }
            });
        }

        // Save partner contacts
        let partnerContact: PartnerContact = new PartnerContact(this._programAgreement.organization.partnerContact);
        console.log("partnerContact: ", partnerContact);
        delete partnerContact._id;
        tempExists = GroundTempPartnerContactCollection.find({guid: partnerContact.guid}).count() > 0;
        if (this._programAgreement.organization.partnerContact._id && tempExists) {
            if (!partnerContact.isMasterData) {
                delete partnerContact._id;
                GroundTempPartnerContactCollection.update(this._programAgreement.organization.partnerContact._id, {
                    $set: partnerContact
                }, {
                    multi: false,
                    upsert: false
                }, (error, result) => {
                    if (error) {
                        console.log("error: ", error);
                    } else {
                        console.log("Successfully updated " + result + " PartnerContact");
                    }
                });
            }
        } else {
            GroundTempPartnerContactCollection.insert(partnerContact, (error, result) => {
                if (error) {
                    console.error("Error inserting partner contact: ", error);
                } else {
                    console.log("Successfully inserted new PartnerContact: ", result);
                    this._programAgreement.organization.partnerContact._id = result;
                }
            });
        }
    }

    public remove(programAgreementId: string): void {
        GroundTempProgramAgreementCollection.remove(programAgreementId, (error, result) => {
            if (error) {
                console.error("error: ", error);
            } else {
                console.log("result: ", result);
            }
        });
    }

    public constructProgramAgreementReviewModels(data: {
        programAgreements: Array<IProgramAgreement>,
        partnerContacts: Array<IPartnerContact>,
        configuration: IConfigurationModel
    }): Array<ProgramAgreementReviewModel> {
        let contracts: Array<ProgramAgreementReviewModel> = [];
        if (data.programAgreements && data.partnerContacts && data.configuration) {
            data.programAgreements.forEach((programAgreement: IProgramAgreement) => {
                if (data.configuration.seasons) {
                    let programStart = moment(programAgreement.startDate);
                    data.configuration.seasons.forEach((season: ISeason) => {
                        if (programStart >= moment(season.start) && programStart <= moment(season.end)) {
                            programAgreement["season"] = season.descriptor;
                        }
                    });
                }

                switch (programAgreement.programAgreementStateKey) {
                    case Enums.PROGRAM_AGREEMENT_STATE.D0:
                    case Enums.PROGRAM_AGREEMENT_STATE.D1:
                    case Enums.PROGRAM_AGREEMENT_STATE.D2:
                    case Enums.PROGRAM_AGREEMENT_STATE.D3:
                    case Enums.PROGRAM_AGREEMENT_STATE.D4:
                    case Enums.PROGRAM_AGREEMENT_STATE.D5:
                    case Enums.PROGRAM_AGREEMENT_STATE.D6:
                        programAgreement["status"] = Constants.CONTRACT_STATUS.DRAFT;
                        break;
                    case Enums.PROGRAM_AGREEMENT_STATE.A1:
                    case Enums.PROGRAM_AGREEMENT_STATE.A2:
                    case Enums.PROGRAM_AGREEMENT_STATE.A3:
                    case Enums.PROGRAM_AGREEMENT_STATE.A4:
                    case Enums.PROGRAM_AGREEMENT_STATE.A5:
                    case Enums.PROGRAM_AGREEMENT_STATE.A6:
                    case Enums.PROGRAM_AGREEMENT_STATE.S1:
                    case Enums.PROGRAM_AGREEMENT_STATE.S2:
                    case Enums.PROGRAM_AGREEMENT_STATE.S3:
                    case Enums.PROGRAM_AGREEMENT_STATE.S4:
                    case Enums.PROGRAM_AGREEMENT_STATE.S5:
                    case Enums.PROGRAM_AGREEMENT_STATE.S6:
                        programAgreement["status"] = Constants.CONTRACT_STATUS.SUBMITTED;
                        break;
                }

                let orgPartner: IProgramAgreementPartner;
                let orgPartnerContact: IPartnerContact;

                if (programAgreement.programAgreementPartners) {
                    orgPartner = programAgreement.programAgreementPartners.find(
                        (partner: IProgramAgreementPartner) => {
                            return partner.partnerFunctionKey === Enums.PARTNER_FUNCTION.ORGANIZATION;
                        }
                    );
                }

                if (orgPartner) {
                    orgPartnerContact = data.partnerContacts.find((contact: IPartnerContact) => {
                        return contact.guid === orgPartner.partnerContactID;
                    });
                }

                let organization: OrganizationModel = this.createOrganizationModelFromPartnerContact(orgPartnerContact);

                let groupPartner: IProgramAgreementPartner;
                if (programAgreement.programAgreementPartners) {
                    groupPartner = programAgreement.programAgreementPartners.find(
                        (partner: IProgramAgreementPartner) => {
                            return partner.partnerFunctionKey === Enums.PARTNER_FUNCTION.SOLD_TO;
                        }
                    );
                }

                let sponsorPartner: IProgramAgreementPartner;
                if (programAgreement.programAgreementPartners) {
                    sponsorPartner = programAgreement.programAgreementPartners.find(
                        (partner: IProgramAgreementPartner) => {
                            return partner.partnerFunctionKey === Enums.PARTNER_FUNCTION.PAYER;
                        }
                    );
                }

                let groupPartnerContact: IPartnerContact;
                let sponsorPartnerContact: IPartnerContact;

                if (groupPartner) {
                    groupPartnerContact = data.partnerContacts.find((contact: IPartnerContact) => {
                        return contact.guid === groupPartner.partnerContactID;
                    });
                }

                if (sponsorPartner) {
                    sponsorPartnerContact = data.partnerContacts.find((contact: IPartnerContact) => {
                        return contact.guid === sponsorPartner.partnerContactID;
                    });
                }

                let group: GroupModel = this.createGroupModelFromPartnerContact({
                    groupPartnerContact: groupPartnerContact,
                    sponsorPartnerContact: sponsorPartnerContact
                });

                let program: ProgramModel = new ProgramModel(programAgreement);

                let brochures: Array<BrochureModel> = [];
                if (programAgreement.programAgreementProductLines) {
                    programAgreement.programAgreementProductLines.forEach((productLine: IProgramAgreementProductLine) => {
                        let brochureType: IKeyText = data.configuration.brochureTypes.find((type: IKeyText) => {
                            return type.key == productLine.brochureTypeKey;
                        });
                        let taxOnPaperworkRule: ITaxOnPaperworkRule = data.configuration.taxOnPaperworkRules.find((rule: ITaxOnPaperworkRule) => {
                            return rule.indicator == productLine.taxOnPaperworkIndicator;
                        });
                        productLine["brochureTypeDescription"] = brochureType ? brochureType.text : Constants.EMPTY_STRING;
                        productLine["taxOnPaperworkDescription"] = taxOnPaperworkRule ? taxOnPaperworkRule.description : Constants.EMPTY_STRING;
                        brochures.push(new BrochureModel(productLine));
                    });
                }

                let warehouseInstructions: IProgramAgreementNote;
                let carrierInstructions: IProgramAgreementNote;
                let noteForOrderProcessing: IProgramAgreementNote;
                if (programAgreement.programAgreementNotes) {
                    warehouseInstructions = programAgreement.programAgreementNotes.find((note: IProgramAgreementNote) => {
                        return note.programAgreementNoteTypeKey === Enums.PROGRAM_AGREEMENT_NOTE_TYPE.W;
                    });
                    carrierInstructions = programAgreement.programAgreementNotes.find((note: IProgramAgreementNote) => {
                        return note.programAgreementNoteTypeKey === Enums.PROGRAM_AGREEMENT_NOTE_TYPE.C;
                    });
                    noteForOrderProcessing = programAgreement.programAgreementNotes.find((note: IProgramAgreementNote) => {
                        return note.programAgreementNoteTypeKey === Enums.PROGRAM_AGREEMENT_NOTE_TYPE.O;
                    });
                }

                let productServices: ProductServicesModel = new ProductServicesModel({
                    programAgreementID: programAgreement.guid,
                    programAgreement: {
                        ref: programAgreement.id
                    }
                });
                if (programAgreement.productServicesSelections) {
                    productServices = new ProductServicesModel(programAgreement.productServicesSelections[0]);
                }

                if (warehouseInstructions) {
                    productServices.warehouseInstructions = new ProgramAgreementNote(warehouseInstructions);
                }
                if (carrierInstructions) {
                    productServices.carrierInstructions = new ProgramAgreementNote(carrierInstructions);
                }

                programAgreement["noteForOrderProcessing"] = noteForOrderProcessing;

                programAgreement["programAgreementID"] = programAgreement.guid;
                programAgreement["programAgreementRef"] = programAgreement.id;

                programAgreement["organization"] = organization;
                programAgreement["group"] = group;
                programAgreement["program"] = program;
                programAgreement["brochures"] = brochures;
                programAgreement["productServices"] = productServices;
                programAgreement["invoiceOther"] = new InvoiceOtherModel(programAgreement);
                let reviewModel: ProgramAgreementReviewModel = new ProgramAgreementReviewModel(programAgreement);
                contracts.push(reviewModel);
            });
        }
        return contracts;
    }

    public createOrganizationModelFromPartnerContact(orgPartnerContact: IPartnerContact): OrganizationModel {
        let orgPartnerAccountKey: string;
        let orgPhyscialAddress: IPartnerContactAddress;
        let orgMailingAddress: IPartnerContactAddress;
        let differentMailingAddress: boolean = false;
        let orgOfficePhone: IPartnerContactPhone;
        let orgFax: IPartnerContactPhone;

        if (orgPartnerContact) {
            let partner: IPartner = orgPartnerContact.partners.find((partner: IPartner) => {
                return partner.partnerContactID === orgPartnerContact.guid;
            });
            if (partner) {
                orgPartnerAccountKey = partner.accountKey;
            }

            orgPhyscialAddress = orgPartnerContact.partnerContactAddresses.find((address: IPartnerContactAddress) => {
                return address.addressTypeKey === Enums.ADDRESS_TYPE.P;
            });

            orgMailingAddress = orgPartnerContact.partnerContactAddresses.find((address: IPartnerContactAddress) => {
                return address.addressTypeKey === Enums.ADDRESS_TYPE.M;
            });

            if (orgPhyscialAddress && orgMailingAddress) {
                differentMailingAddress = (
                    orgPhyscialAddress.street.toLocaleLowerCase() != orgMailingAddress.street.toLocaleLowerCase());
                // || orgPhyscialAddress.city.toLocaleLowerCase() != orgMailingAddress.city.toLocaleLowerCase()
                // || orgPhyscialAddress.stateProvinceKey.toLocaleLowerCase() != orgMailingAddress.stateProvinceKey.toLocaleLowerCase()
                // || orgPhyscialAddress.postalCode.toLocaleLowerCase() != orgMailingAddress.postalCode.toLocaleLowerCase());
            }

            orgOfficePhone = orgPartnerContact.partnerContactPhones.find((phone: IPartnerContactPhone) => {
                return phone.phoneTypeKey === Enums.PHONE_TYPE.W;
            });
            orgFax = orgPartnerContact.partnerContactPhones.find((phone: IPartnerContactPhone) => {
                return phone.phoneTypeKey === Enums.PHONE_TYPE.F;
            });
        }

        return new OrganizationModel({
            name: orgPartnerContact ? orgPartnerContact.name : Constants.EMPTY_STRING,
            phone: orgOfficePhone ? orgOfficePhone.number : Constants.EMPTY_STRING,
            fax: orgFax ? orgFax.number : Constants.EMPTY_STRING,
            addresses: {
                physical: new PartnerContactAddress(orgPhyscialAddress),
                mailing: new PartnerContactAddress(orgMailingAddress),
                differentMailingAddress: differentMailingAddress
            },
            partnerAccountKey: orgPartnerAccountKey,
            partnerContact: orgPartnerContact
        });
    }

    public createGroupModelFromPartnerContact(data: {
        groupPartnerContact: IPartnerContact,
        sponsorPartnerContact: IPartnerContact
    }): GroupModel {
        let groupPartnerAccountKey: string;
        let onlineStoreId: string;
        let groupPhyscialAddress: IPartnerContactAddress;

        if (data.groupPartnerContact) {
            let partner: IPartner = data.groupPartnerContact.partners.find((partner: IPartner) => {
                return partner.partnerContactID === data.groupPartnerContact.guid;
            });
            if (partner) {
                groupPartnerAccountKey = partner.accountKey;
                let partnerAttribute: IPartnerAttribute = partner.partnerAttributes.find((attr: IPartnerAttribute) => {
                    return attr.partnerAttributeTypeCode == 5;
                });
                if (partnerAttribute) {
                    onlineStoreId = partnerAttribute.value;
                }
            }

            groupPhyscialAddress = data.groupPartnerContact.partnerContactAddresses.find((address: IPartnerContactAddress) => {
                return address.addressTypeKey === Enums.ADDRESS_TYPE.P;
            });
        }

        return new GroupModel({
            name: data.groupPartnerContact ? data.groupPartnerContact.name : Constants.EMPTY_STRING,
            address: new PartnerContactAddress(groupPhyscialAddress),
            sponsor: this.createSponsorModelFromPartnerContact(data.sponsorPartnerContact),
            onlineStoreId: onlineStoreId,
            partnerAccountKey: groupPartnerAccountKey,
            partnerContact: data.groupPartnerContact
        });
    }

    public createSponsorModelFromPartnerContact(sponsorPartnerContact: IPartnerContact): SponsorModel {
        let sponsorPartnerAccountKey: string;
        let sponsorMobilePhoneNumber: IPartnerContactPhone;
        let sponsorHomePhoneNumber: IPartnerContactPhone;
        let sponsorWorkPhoneNumber: IPartnerContactPhone;

        if (sponsorPartnerContact) {
            let partner: IPartner = sponsorPartnerContact.partners.find((partner: IPartner) => {
                return partner.partnerContactID === sponsorPartnerContact.guid;
            });
            if (partner) {
                sponsorPartnerAccountKey = partner.accountKey;
            }

            sponsorMobilePhoneNumber = sponsorPartnerContact.partnerContactPhones.find((phone: IPartnerContactPhone) => {
                return phone.phoneTypeKey === Enums.PHONE_TYPE.M;
            });

            sponsorHomePhoneNumber = sponsorPartnerContact.partnerContactPhones.find((phone: IPartnerContactPhone) => {
                return phone.phoneTypeKey === Enums.PHONE_TYPE.H;
            });

            sponsorWorkPhoneNumber = sponsorPartnerContact.partnerContactPhones.find((phone: IPartnerContactPhone) => {
                return phone.phoneTypeKey === Enums.PHONE_TYPE.W;
            });
        }

        return new SponsorModel({
            name: sponsorPartnerContact ? sponsorPartnerContact.name : Constants.EMPTY_STRING,
            emailAddress: sponsorPartnerContact ? sponsorPartnerContact.emailAddress : Constants.EMPTY_STRING,
            phoneNumbers: {
                mobile: sponsorMobilePhoneNumber,
                home: sponsorHomePhoneNumber,
                work: sponsorWorkPhoneNumber
            },
            partnerAccountKey: sponsorPartnerAccountKey,
            partnerContact: sponsorPartnerContact
        });
    }
}