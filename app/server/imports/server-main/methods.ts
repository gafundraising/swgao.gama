import {Meteor} from "meteor/meteor";
import {Constants} from "../../../both/Constants";
import {IConfigurationModel} from "../../../both/models/ConfigurationModel";
import {ConfigurationCollection} from "../../../both/collections/GroundConfigurationCollection";
import {
    IPartner,
    IPartnerAttribute,
    IPartnerContact,
    IPartnerContactAddress,
    IPartnerContactPhone,
    IPartnerRelationsAsParent,
    IProductServicesSelection,
    IProgramAgreement,
    IProgramAgreementDownloadResponse,
    IProgramAgreementNote,
    IProgramAgreementPartner,
    IProgramAgreementProductLine
} from "../../../both/models/ProgramAgreementDownloadResponseModel";
import {ProgramAgreementDownloadsCollection} from "../../../both/collections/ProgramAgreementDownloadResponseCollection";
import * as moment from "moment";
import {ProgramAgreementCollection} from "../../../both/collections/ProgramAgreementCollection";
import {PartnerContactCollection} from "../../../both/collections/GroundPartnerContactCollection";
declare var Accounts;
declare var HTTP;
declare var Npm;

const Future = Npm.require("fibers/future");

export class MeteorMethods {

    public init(): void {
        Meteor.methods({
            "/api/user/authenticate": (credentials) => {
                let self = this;
                let endpoint: string = Meteor.settings.private.gao.baseUrl + Constants.GAO_ENDPOINTS.AUTHENTICATE;
                let future = new Future();
                let headers: any = {
                    "Content-Type": "application/json; charset=UTF-8",
                    "Authorization": "Basic " + credentials
                };
                let postData: any = {};
                HTTP.call("POST", endpoint, {
                    headers: headers,
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response: any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        if (response.statusCode != 200) {
                            if (response.statusCode == 302) {
                                error = new Meteor.Error(Constants.METEOR_ERRORS.INCORRECT_PASSWORD,
                                    Constants.METEOR_ERRORS.INCORRECT_PASSWORD);
                            } else {
                                error = new Meteor.Error("api-error", "API Error");
                            }
                            future.throw(error);
                        } else {
                            let userProfile: IGAOUserProfile = response.data.userProfile;

                            // Get Cookie
                            const RESPONSE_COOKIE_HEADER_NAME: string = "set-cookie";
                            let authTicketName: string = response.data.authTicketName;
                            let authTicketNameEncoded: string = authTicketName.replace(/=/g, "%3d")
                                .replace(/,/g, "%2c");
                            let cookies: Array<string> = response.headers[RESPONSE_COOKIE_HEADER_NAME];
                            let cookie: string = cookies.find((value) => {
                                return value.startsWith(authTicketNameEncoded);
                            });
                            let cookieCrumbles: Array<string> = cookie.split(";");
                            let authTicketValue: string = cookieCrumbles[0].slice(authTicketNameEncoded.length + 1);

                            let user: Meteor.User = Accounts.findUserByUsername(userProfile.logonName);
                            let userId: string;
                            if (user) {
                                userId = user._id;
                                Meteor.users.update({_id: user._id}, {
                                    $set: {
                                        "profile.authTicket.value": authTicketValue
                                    }
                                });
                            } else {
                                userId = self.createMetorUserFromGAOUserProfile(userProfile);
                                Meteor.users.update({_id: userId}, {
                                    $set: {
                                        authTicket: {
                                            name: authTicketNameEncoded,
                                            value: authTicketValue
                                        }
                                    }
                                });
                            }

                            future.return({loginToken: self.getLoginTokenForUser(userId)});
                        }
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    console.error("GAO API Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            "/api/hierarchy": () => {
                let user: any = this.checkForUser();
                let endpoint: string = Meteor.settings.private.gao.baseUrl + Constants.GAO_ENDPOINTS.HIERARCHY;
                let future = new Future();
                let headers: any = {
                    "Content-Type": "application/json; charset=UTF-8",
                    "Cookie": user.authTicket.name + "=" + user.authTicket.value
                };
                let postData: any = {};
                HTTP.call("GET", endpoint, {
                    headers: headers,
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response: any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        future.return(response.data)
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    console.error("GAO API Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            "/api/user/impersonate": (data: IGaoApiData) => {
                let self = this;
                let user: any = this.checkForUser();
                data.authTicket = user.authTicket;

                let headers: any = this.prepareHeaders(data);
                let postData: any = {};
                let baseUrl: string = Meteor.settings.private.gao.baseUrl;
                let endpoint: string = baseUrl + Constants.GAO_ENDPOINTS.IMPERSONATE;

                let future = new Future();

                HTTP.call("POST", endpoint, {
                    headers: headers,
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response: any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        if (response.statusCode != 200) {
                            error = new Meteor.Error("api-error", "API Error");
                            future.throw(error);
                        } else {
                            let userProfile: IGAOUserProfile = response.data.userProfile;
                            let impersonatedUser: Meteor.User = Accounts.findUserByUsername(userProfile.logonName);
                            let userId: string;
                            if (impersonatedUser) {
                                userId = impersonatedUser._id;
                            } else {
                                userId = self.createMetorUserFromGAOUserProfile(userProfile);
                            }

                            Meteor.users.update({_id: userId}, {
                                $set: {
                                    authTicket: user.authTicket
                                }
                            });

                            future.return({loginToken: self.getLoginTokenForUser(userId)});
                        }
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    console.error("GAO API Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            "/api/configuration/configure": (data: IGaoApiData) => {
                let self = this;
                let user: any = this.checkForUser();
                data.authTicket = user.authTicket;

                let headers: any = this.prepareHeaders(data);
                let postData: any = {};
                let baseUrl: string = Meteor.settings.private.gao.baseUrl;
                let endpoint: string = baseUrl + Constants.GAO_ENDPOINTS.CONFIGURE;

                let future = new Future();

                HTTP.call("GET", endpoint, {
                    headers: headers,
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response: any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        if (response.statusCode != 200) {
                            error = new Meteor.Error("api-error", "API Error");
                            future.throw(error);
                        } else {
                            let configuration: IConfigurationModel = ConfigurationCollection.findOne({
                                salesRepAccountNumber: user.profile.accountNumber
                            });
                            let isNewConfig: boolean = (!configuration);
                            if (isNewConfig) {
                                ConfigurationCollection.insert(response.data.configuration);
                            } else {
                                ConfigurationCollection.update({
                                    _id: configuration._id
                                }, {
                                    $set: response.data.configuration
                                });
                            }
                            future.return(response.data);
                        }
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    console.error("GAO API Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            "/api/programagreement/download": (data: any) => {
                let self = this;
                let user: any = this.checkForUser();
                data.authTicket = user.authTicket;

                let headers: any = this.prepareHeaders(data);
                let postData: any = {};
                let baseUrl: string = Meteor.settings.private.gao.baseUrl;
                let endpoint: string = baseUrl + Constants.GAO_ENDPOINTS.PROGRAM_AGREEMENTS;

                let future = new Future();
                console.log("Downloading contracts....");
                HTTP.call("GET", endpoint, {
                    headers: headers,
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response: any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        if (response.statusCode != 200) {
                            error = new Meteor.Error("api-error", "API Error");
                            future.throw(error);
                        } else {
                            console.log("Downloaded contracts!!!");
                            let programAgreementDownloadResponse: IProgramAgreementDownloadResponse = response.data;
                            let paDownload: IProgramAgreementDownloadResponse = ProgramAgreementDownloadsCollection.findOne({
                                userId: user._id
                            });

                            if (!paDownload) {
                                paDownload = {
                                    userId: user._id,
                                    timestamp: moment().toISOString()
                                };
                                ProgramAgreementDownloadsCollection.insert(paDownload);
                            } else {
                                paDownload.timestamp = moment().toISOString();
                                ProgramAgreementDownloadsCollection.update({
                                    _id: paDownload._id
                                }, {
                                    $set: paDownload
                                });
                            }

                            programAgreementDownloadResponse.partnerContacts.forEach((partnerContact:IPartnerContact) => {
                                partnerContact.userId = user._id;
                                partnerContact.guid = partnerContact.id;
                                partnerContact.id = partnerContact["$id"];
                                delete partnerContact["$id"];

                                partnerContact.partnerContactAddresses.forEach((address:IPartnerContactAddress) => {
                                    address.guid = address.id;
                                    address.id = address["$id"];
                                    address.partnerContact.ref = address.partnerContact["$ref"];
                                    delete address.partnerContact["$ref"];
                                    delete address["$id"];
                                });

                                partnerContact.partnerContactPhones.forEach((phone:IPartnerContactPhone) => {
                                    phone.guid = phone.id;
                                    phone.id = phone["$id"];
                                    phone.partnerContact.ref = phone.partnerContact["$ref"];
                                    delete phone.partnerContact["$ref"];
                                    delete phone["$id"];
                                });

                                partnerContact.partners.forEach((partner:IPartner) => {
                                    partner.guid = partner.id;
                                    partner.id = partner["$id"];
                                    partner.partnerContact.ref = partner.partnerContact["$ref"];
                                    delete partner.partnerContact["$ref"];
                                    delete partner["$id"];

                                    partner.partnerAttributes.forEach((attr:IPartnerAttribute) => {
                                        attr.guid = attr.id;
                                        attr.id = attr["$id"];
                                        attr.partner.ref = attr.partner["$ref"];
                                        delete attr.partner["$ref"];
                                        delete attr["$id"];
                                    });

                                    partner.partnerRelationsAsParent.forEach((item:IPartnerRelationsAsParent) => {
                                        item.guid = item.id;
                                        item.id = item["$id"];
                                        item.parentPartner.ref = item.parentPartner["$ref"];
                                        delete item.parentPartner["$ref"];
                                        delete item["$id"];
                                    });
                                });

                                PartnerContactCollection.upsert({
                                    guid: partnerContact.guid
                                }, partnerContact);
                            });

                            programAgreementDownloadResponse.programAgreements.forEach((programAgreement:IProgramAgreement) => {
                                programAgreement.userId = user._id;
                                programAgreement.guid = programAgreement.id;
                                programAgreement.id = programAgreement["$id"];
                                delete programAgreement["$id"];

                                programAgreement.programAgreementNotes.forEach((note:IProgramAgreementNote) => {
                                    note.guid = note.id;
                                    note.id = note["$id"];
                                    note.programAgreement.ref = note.programAgreement["$ref"];
                                    delete note.programAgreement["$ref"];
                                    delete note["$id"];
                                });

                                programAgreement.programAgreementPartners.forEach((partner:IProgramAgreementPartner) => {
                                    partner.guid = partner.id;
                                    partner.id = partner["$id"];
                                    partner.programAgreement.ref = partner.programAgreement["$ref"];
                                    delete partner.programAgreement["$ref"];
                                    delete partner["$id"];
                                });

                                programAgreement.programAgreementProductLines.forEach((pl:IProgramAgreementProductLine) => {
                                    pl.guid = pl.id;
                                    pl.id = pl["$id"];
                                    pl.programAgreement.ref = pl.programAgreement["$ref"];
                                    delete pl.programAgreement["$ref"];
                                    delete pl["$id"];
                                });

                                programAgreement.productServicesSelections.forEach((services:IProductServicesSelection) => {
                                    services.guid = services.id;
                                    services.id = services["$id"];
                                    services.programAgreement.ref = services.programAgreement["$ref"];
                                    delete services.programAgreement["$ref"];
                                    delete services["$id"];
                                });

                                ProgramAgreementCollection.upsert({
                                    guid: programAgreement.guid
                                }, programAgreement);
                            });

                            future.return({
                                success: true,
                                partnerContacts: programAgreementDownloadResponse.partnerContacts.length,
                                programAgreements: programAgreementDownloadResponse.programAgreements.length
                            });
                        }
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    console.error("GAO API Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            }
        });
    }

    private checkForUser(): Meteor.User {
        let currentUserId = Meteor.userId();
        let user: Meteor.User;
        if (!currentUserId) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.SIGN_IN, "Please sign in.");
        } else {
            user = Meteor.users.findOne(currentUserId);
            if (!user) {
                throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid User ID");
            }
        }
        return user;
    }

    private createMetorUserFromGAOUserProfile(userProfile: IGAOUserProfile): string {
        let meteorProfile: any = {
            name: {
                given: userProfile.firstName,
                family: userProfile.lastName,
                display: userProfile.displayName
            },
            accountNumber: userProfile.accountNumber,
            picture: Constants.IMAGE_URI_PREFIX + userProfile.serializedThumbnailPhoto,
            partnerContactId: userProfile.partnerContactId,
        };
        return Accounts.createUser({
            username: userProfile.logonName,
            email: userProfile.emailAddress,
            profile: meteorProfile
        });
    }

    private getLoginTokenForUser(userId: string): string {
        let stampedLoginToken = Accounts._generateStampedLoginToken();
        Accounts._insertLoginToken(userId, stampedLoginToken);
        return stampedLoginToken.token;
    }

    private prepareHeaders(data: IGaoApiData): any {
        let appName: string = Meteor.settings.public.appName;
        let appVersion: string = Meteor.settings.public.appVersion;
        let userAgent: string = appName + "/" + appVersion + "/" + data.agentInfo.platform + "/" + data.agentInfo.version;
        let baseUrl: string = Meteor.settings.private.gao.baseUrl;
        let host: string = baseUrl.split("://")[1];
        return {
            "Content-Type": "application/json; charset=UTF-8",
            "Cookie": data.authTicket.name + "=" + data.authTicket.value,
            "User-Agent": userAgent,
            "From": data.accountNumber + "@" + host
        };
    }
}

interface IGAOUserProfile {
    accountNumber?: number,
    displayName: string,
    emailAddress: string,
    firstName: string,
    lastName: string,
    logonName: string,
    partnerContactId: string,
    serializedThumbnailPhoto: string
}

interface IAuthTicket {
    name?: string,
    value?: string
}

interface IGaoApiData {
    accountNumber?: string,
    agentInfo?: {
        platform?: string,
        version?: string
    },
    authTicket?: IAuthTicket
}