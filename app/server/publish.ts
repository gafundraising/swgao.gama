import {Meteor} from "meteor/meteor";
import {
    ProgramAgreementCollection,
    TempProgramAgreementCollection
} from "../both/collections/ProgramAgreementCollection";
import {OrganizationCollection} from "../both/collections/OrganizationCollection";
import {GroupCollection} from "../both/collections/GroupCollection";
import {ConfigurationCollection} from "../both/collections/GroundConfigurationCollection";
import {Constants} from "../both/Constants";
import {ProgramAgreementDownloadsCollection} from "../both/collections/ProgramAgreementDownloadResponseCollection";
import {
    PartnerContactCollection,
    TempPartnerContactCollection
} from "../both/collections/GroundPartnerContactCollection";


Meteor.publish(Constants.PUBLICATIONS.PROGRAM_AGREEMENT, function () {
    return ProgramAgreementCollection.find({userId: this.userId});
});

Meteor.publish(Constants.PUBLICATIONS.ORGANIZATION, function () {
    return OrganizationCollection.find();
});

Meteor.publish(Constants.PUBLICATIONS.GROUP, function (orgId: string) {
    return GroupCollection.find({orgId: orgId});
});

Meteor.publish(Constants.PUBLICATIONS.CONFIGURATION, function (accountNumber: string) {
    return ConfigurationCollection.find({salesRepAccountNumber: accountNumber});
});

Meteor.publish(Constants.PUBLICATIONS.PROGRAM_AGREEMENT_DOWNLOAD, function () {
    return ProgramAgreementDownloadsCollection.find({userId: this.userId});
});

Meteor.publish(Constants.PUBLICATIONS.PARTNER_CONTACTS, function () {
    return PartnerContactCollection.find({userId: this.userId});
});

Meteor.publish(Constants.PUBLICATIONS.TEMP_PROGRAM_AGREEMENT, function () {
    return TempProgramAgreementCollection.find({userId: this.userId});
});

Meteor.publish(Constants.PUBLICATIONS.TEMP_PARTNER_CONTACTS, function () {
    return TempPartnerContactCollection.find({userId: this.userId});
});