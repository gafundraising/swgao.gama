import {Mongo} from "meteor/mongo";
import {Meteor} from "meteor/meteor";
import {IProgramAgreementDownloadResponse} from "../models/ProgramAgreementDownloadResponseModel";

declare var Ground;

export var ProgramAgreementDownloadsCollection = new Mongo.Collection<IProgramAgreementDownloadResponse>('program_agreement_downloads');
export var GroundProgramAgreementDownloadsCollection = new Ground.Collection(ProgramAgreementDownloadsCollection,'program_agreement_downloads');

if (Meteor.isServer) {
    ProgramAgreementDownloadsCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}