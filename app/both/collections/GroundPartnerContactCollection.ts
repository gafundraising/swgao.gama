import {Mongo} from "meteor/mongo";
import {Meteor} from "meteor/meteor";
import {IPartnerContact} from "../models/ProgramAgreementDownloadResponseModel";

declare var Ground;

export var PartnerContactCollection = new Mongo.Collection<IPartnerContact>('partner_contacts');
export var GroundPartnerContactCollection = new Ground.Collection(PartnerContactCollection,'partner_contacts');
export var TempPartnerContactCollection = new Mongo.Collection<IPartnerContact>('temp_partner_contacts');
export var GroundTempPartnerContactCollection = new Ground.Collection(TempPartnerContactCollection,'temp_partner_contacts');

if (Meteor.isServer) {
    PartnerContactCollection.rawCollection().createIndex({userId: 1});
    PartnerContactCollection.rawCollection().createIndex({guid: 1});
    PartnerContactCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    TempPartnerContactCollection.rawCollection().createIndex({userId: 1});
    TempPartnerContactCollection.rawCollection().createIndex({guid: 1});
    TempPartnerContactCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}