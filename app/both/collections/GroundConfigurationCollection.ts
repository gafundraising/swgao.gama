import {Mongo} from "meteor/mongo";
import {Meteor} from "meteor/meteor";
import {IConfigurationModel} from "../models/ConfigurationModel";

declare var Ground;

export var ConfigurationCollection = new Mongo.Collection<IConfigurationModel>('configurations');
export var GroundConfigurationCollection = new Ground.Collection(ConfigurationCollection,'configurations');

if (Meteor.isServer) {
    ConfigurationCollection.rawCollection().createIndex({salesRepAccountNumber: 1});
    ConfigurationCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}