/**
 * Created by mjwheatley on 7/26/16.
 */
declare var Ground;
import {OrganizationModel} from "../models/OrganizationModel";
import {Meteor} from "meteor/meteor";
import {Mongo} from "meteor/mongo";

export var OrganizationCollection = new Mongo.Collection<OrganizationModel>('organizations');
export var GroundOrganizationCollection = new Ground.Collection(OrganizationCollection, 'organizations');


if (Meteor.isServer) {
    OrganizationCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    //OrganizationCollection.deny({
    //    insert: function (userId, doc) {
    //        return true;
    //    },
    //
    //    update: function (userId, doc, fieldNames, modifier) {
    //        return true;
    //    },
    //
    //    remove: function (userId, doc) {
    //        return true;
    //    }
    //});
}
