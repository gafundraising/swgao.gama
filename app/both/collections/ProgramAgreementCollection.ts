/**
 * Created by mjwheatley on 7/26/16.
 */
import {IProgramAgreement} from "../models/ProgramAgreementDownloadResponseModel";
import {Meteor} from "meteor/meteor";
import {Mongo} from "meteor/mongo";

declare var Ground;

export var ProgramAgreementCollection = new Mongo.Collection<IProgramAgreement>('program_agreements');
export var GroundProgramAgreementCollection = new Ground.Collection(ProgramAgreementCollection, 'program_agreements');
export var TempProgramAgreementCollection = new Mongo.Collection<IProgramAgreement>('temp_program_agreements');
export var GroundTempProgramAgreementCollection = new Ground.Collection(TempProgramAgreementCollection, 'temp_program_agreements');


if (Meteor.isServer) {
    ProgramAgreementCollection.rawCollection().createIndex({userId: 1});
    ProgramAgreementCollection.rawCollection().createIndex({guid: 1});
    ProgramAgreementCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    TempProgramAgreementCollection.rawCollection().createIndex({userId: 1});
    TempProgramAgreementCollection.rawCollection().createIndex({guid: 1});
    TempProgramAgreementCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}
