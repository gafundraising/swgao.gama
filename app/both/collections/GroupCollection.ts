/**
 * Created by mjwheatley on 7/26/16.
 */
declare var Ground;
import {GroupModel} from "../models/GroupModel";
import {Meteor} from "meteor/meteor";
import {Mongo} from "meteor/mongo";

export var GroupCollection = new Mongo.Collection<GroupModel>('groups');
export var GroundGroupCollection = new Ground.Collection(GroupCollection, 'groups');


if (Meteor.isServer) {
    GroupCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    //GroupCollection.deny({
    //    insert: function (userId, doc) {
    //        return true;
    //    },
    //
    //    update: function (userId, doc, fieldNames, modifier) {
    //        return true;
    //    },
    //
    //    remove: function (userId, doc) {
    //        return true;
    //    }
    //});
}
