/**
 * Created by mjwheatley on 3/22/16.
 */
import {Constants} from "../Constants";
import {IPartnerContactAddress} from "./ProgramAgreementDownloadResponseModel";
import * as moment from "moment";
import * as uuid from "uuid/v4";

export interface IAddress {
    streetAddress:string,
    locality:string,
    region:string,
    postalCode:string
}

export class Address implements IAddress {
    streetAddress:string = Constants.EMPTY_STRING;
    locality:string = Constants.EMPTY_STRING;
    region:string = Constants.EMPTY_STRING;
    postalCode:string = Constants.EMPTY_STRING;

    constructor(addressDetails?:IAddress) {
        if (addressDetails) {
            if (addressDetails.hasOwnProperty("streetAddress")) {
                this.streetAddress = addressDetails.streetAddress;
            }
            if (addressDetails.hasOwnProperty("locality")) {
                this.locality = addressDetails.locality;
            }
            if (addressDetails.hasOwnProperty("region")) {
                this.region = addressDetails.region;
            }
            if (addressDetails.hasOwnProperty("postalCode")) {
                this.postalCode = addressDetails.postalCode;
            }
        }
    }
}

export class PartnerContactAddress implements IPartnerContactAddress {
    id: string;
    guid: string = uuid();
    partnerContactID: string;
    addressTypeKey: string;
    street: string = Constants.EMPTY_STRING;
    city: string = Constants.EMPTY_STRING;
    county: string;
    stateProvinceKey: string = Constants.EMPTY_STRING;
    postalCode: string = Constants.EMPTY_STRING;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    partnerContact: {
        ref?: string
    };

    constructor(address?:IPartnerContactAddress) {
        if (address) {
            if (address.hasOwnProperty("id")) {
                this.id = address.id;
            }
            if (address.hasOwnProperty("guid")) {
                this.guid = address.guid;
            }
            if (address.hasOwnProperty("partnerContactID")) {
                this.partnerContactID = address.partnerContactID;
            }
            if (address.hasOwnProperty("addressTypeKey")) {
                this.addressTypeKey = address.addressTypeKey;
            }
            if (address.hasOwnProperty("street")) {
                this.street = address.street;
            }
            if (address.hasOwnProperty("city")) {
                this.city = address.city;
            }
            if (address.hasOwnProperty("county")) {
                this.county = address.county;
            }
            if (address.hasOwnProperty("stateProvinceKey")) {
                this.stateProvinceKey = address.stateProvinceKey;
            }
            if (address.hasOwnProperty("postalCode")) {
                this.postalCode = address.postalCode;
            }
            if (address.hasOwnProperty("created")) {
                this.created = address.created;
            }
            if (address.hasOwnProperty("createdBy")) {
                this.createdBy = address.createdBy;
            }
            if (address.hasOwnProperty("modified")) {
                this.modified = address.modified;
            }
            if (address.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = address.modifiedBy;
            }
            if (address.hasOwnProperty("version")) {
                this.version = address.version;
            }
            if (address.hasOwnProperty("partnerContact")) {
                this.partnerContact = address.partnerContact;
            }
        }
    }
}