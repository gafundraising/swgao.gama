/**
 * Created by mjwheatley on 7/25/16.
 */
import {Meteor} from "meteor/meteor";
import {OrganizationModel} from "./OrganizationModel";
import {GroupModel} from "./GroupModel";
import {ProgramModel} from "./ProgramModel";
import {ProductServicesModel, ProductServicesSelection} from "./ProductServicesModel";
import {InvoiceOtherModel} from "./InvoiceOtherModel";
import {Constants} from "../Constants";
import * as moment from "moment";
import {
    IProductServicesSelection,
    IProgramAgreement,
    IProgramAgreementNote,
    IProgramAgreementPartner,
    IProgramAgreementProductLine,
    ProgramAgreementPartner
} from "./ProgramAgreementDownloadResponseModel";
import {Enums} from "../Enums";
import {BrochureModel, ProgramAgreementProductLine} from "./BrochureModel";
import * as uuid from "uuid/v4";

export interface IProgramAgreementReviewModel {
    _id?: string,
    userId?: string,
    id?: string,
    guid?: string,
    key?: string,
    divisionTypeKey?: string,
    programAgreementStateKey?: string
    season?: string,
    status?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    defaultStorefrontTypeKey?: string,
    sponsorSignature?: string,
    sponsorApproved?: string,
    salesRepSignature?: string,
    salesRepApproved?: string,
    submissionPending?: boolean,
    hasRuleViolations?: boolean,
    hasSupplyOrder?: boolean,
    organization?: OrganizationModel,
    group?: GroupModel,
    program?: ProgramModel,
    brochures?: Array<BrochureModel>,
    productServices?: ProductServicesModel,
    invoiceOther?: InvoiceOtherModel,
    programAgreementPartners?: Array<IProgramAgreementPartner>,
    searchable?: string
}

export class ProgramAgreementReviewModel implements IProgramAgreementReviewModel {
    public _id: string;
    public userId: string = Meteor.userId();
    public id: string = Constants.EMPTY_STRING;
    public guid: string = uuid();
    public key: string = Constants.EMPTY_STRING;
    public divisionTypeKey: string = "20";
    public programAgreementStateKey: string = Enums.PROGRAM_AGREEMENT_STATE.D0;
    public season: string = Constants.EMPTY_STRING;
    public status: string = Constants.CONTRACT_STATUS.DRAFT;
    public created: string = moment().toISOString();
    public createdBy: number = 0;
    public modified: string = moment().toISOString();
    public modifiedBy: number = 0;
    public version: string = Constants.EMPTY_STRING;
    public defaultStorefrontTypeKey: string = null;
    public sponsorSignature: string = null;
    public sponsorApproved: string = null;
    public salesRepSignature: string = null;
    public salesRepApproved: string = null;
    public submissionPending: boolean = false;
    public hasRuleViolations: boolean = false;
    public hasSupplyOrder: boolean = false;
    public organization: OrganizationModel = new OrganizationModel();
    public group: GroupModel = new GroupModel();
    public program: ProgramModel = new ProgramModel();
    public brochures: Array<BrochureModel> = [];
    public productServices: ProductServicesModel = new ProductServicesModel({
        programAgreementID: this.guid,
        programAgreement: {
            ref: this.id
        }
    });
    public invoiceOther: InvoiceOtherModel = new InvoiceOtherModel({
        programAgreementID: this.guid,
        programAgreementRef: this.id
    });
    public programAgreementPartners: Array<IProgramAgreementPartner> = [];
    public searchable: string;
    public isEditable: boolean = true;

    constructor(program?: IProgramAgreementReviewModel) {
        if (program) {
            if (program.hasOwnProperty("_id")) {
                this._id = program._id;
            }
            if (program.hasOwnProperty("userId")) {
                this.userId = program.userId;
            }
            if (program.hasOwnProperty("id")) {
                this.id = program.id;
            }
            if (program.hasOwnProperty("guid")) {
                this.guid = program.guid;
            }
            if (program.hasOwnProperty("key")) {
                this.key = program.key;
            }
            if (program.hasOwnProperty("divisionTypeKey")) {
                this.divisionTypeKey = program.divisionTypeKey;
            }
            if (program.hasOwnProperty("programAgreementStateKey")) {
                this.programAgreementStateKey = program.programAgreementStateKey;
            }
            if (program.hasOwnProperty("season")) {
                this.season = program["season"];
            }
            if (program.hasOwnProperty("status")) {
                this.status = program["status"];
            }
            if (program.hasOwnProperty("created")) {
                this.created = program.created;
            }
            if (program.hasOwnProperty("createdBy")) {
                this.createdBy = program.createdBy;
            }
            if (program.hasOwnProperty("modified")) {
                this.modified = program.modified;
            }
            if (program.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = program.modifiedBy;
            }
            if (program.hasOwnProperty("version")) {
                this.version = program.version;
            }
            if (program.hasOwnProperty("defaultStorefrontTypeKey")) {
                this.defaultStorefrontTypeKey = program.defaultStorefrontTypeKey;
            }
            if (program.hasOwnProperty("sponsorSignature")) {
                this.sponsorSignature = program.sponsorSignature;
            }
            if (program.hasOwnProperty("sponsorApproved")) {
                this.sponsorApproved = program.sponsorApproved;
            }
            if (program.hasOwnProperty("salesRepSignature")) {
                this.salesRepSignature = program.salesRepSignature;
            }
            if (program.hasOwnProperty("salesRepApproved")) {
                this.salesRepApproved = program.salesRepApproved;
            }
            if (program.hasOwnProperty("submissionPending")) {
                this.submissionPending = program.submissionPending;
            }
            if (program.hasOwnProperty("hasRuleViolations")) {
                this.hasRuleViolations = program.hasRuleViolations;
            }
            if (program.hasOwnProperty("hasSupplyOrder")) {
                this.hasSupplyOrder = program.hasSupplyOrder;
            }
            if (program.hasOwnProperty("organization")) {
                this.organization = program.organization;
            }
            if (program.hasOwnProperty("group")) {
                this.group = program.group;
            }
            if (program.hasOwnProperty("program")) {
                this.program = program.program;
            }
            if (program.hasOwnProperty("brochures")) {
                this.brochures = program.brochures;
            }
            if (program.hasOwnProperty("productServices")) {
                this.productServices = new ProductServicesModel(program.productServices);
            }
            if (program.hasOwnProperty("invoiceOther")) {
                this.invoiceOther = new InvoiceOtherModel(program.invoiceOther);
            }
            if (program.hasOwnProperty("programAgreementPartners")) {
                this.programAgreementPartners = program["programAgreementPartners"];
            }
            this.isEditable = this.getIsEditable();
        }
        this.searchable = this.key + this.group.name + this.group.sponsor.name
            + this.group.address.city + this.group.address.stateProvinceKey + moment(this.program.startDate).format("MM/DD/YYYY");
    }

    public getIsEditable(): boolean {
        let isEditable: boolean = false;
        switch (this.programAgreementStateKey) {
            case Enums.PROGRAM_AGREEMENT_STATE.D0:
            case Enums.PROGRAM_AGREEMENT_STATE.D1:
            case Enums.PROGRAM_AGREEMENT_STATE.D2:
            case Enums.PROGRAM_AGREEMENT_STATE.D3:
            case Enums.PROGRAM_AGREEMENT_STATE.D4:
            case Enums.PROGRAM_AGREEMENT_STATE.D5:
            case Enums.PROGRAM_AGREEMENT_STATE.D6:
            case Enums.PROGRAM_AGREEMENT_STATE.A1:
            case Enums.PROGRAM_AGREEMENT_STATE.A2:
            case Enums.PROGRAM_AGREEMENT_STATE.A3:
            case Enums.PROGRAM_AGREEMENT_STATE.A4:
                isEditable = true;
                break;

        }

        return isEditable;
    }
}

export class ProgramAgreement implements IProgramAgreement {
    _id: string;
    userId: string;
    id: string;
    guid: string;
    key: string;
    divisionTypeKey: string;
    programAgreementStateKey: string;
    startDate: string;
    signDate: string;
    cancellationDate: string;
    estimatedGroupSize: number;
    estimatedWholesale: number;
    estimatedNumberOfClassrooms: number;
    purchaseOrderTypeKey: string;
    purchaseOrderNumber: string;
    accountTaxClassificationKey: string;
    accountTaxClassificationExpirationDate: string;
    defaultStorefrontTypeKey: string = null;
    hasSupplyOrder: boolean;
    collationFeeAmount: number;
    prizeCostAmount: number;
    prizeCostPercentage: number;
    sponsorSignature: any;
    sponsorApproved: any;
    salesRepSignature: any;
    salesRepApproved: any;
    submissionPending: any;
    hasRuleViolations: boolean;
    created: string;
    createdBy: number;
    modified: string;
    modifiedBy: number;
    version: string;
    minimumDonationAmount: number;
    isDonationsEnabled: boolean;
    isFullServiceEnabled: boolean;
    is59MinuteEnabled: boolean;
    isTuitionRewardsEnabled: boolean;
    isPaymentAppEnabled: boolean;
    isShippingToAccountAllowed: boolean;
    programAgreementNotes: Array<IProgramAgreementNote> = [];
    programAgreementPartners: Array<IProgramAgreementPartner> = [];
    programAgreementProductLines: Array<IProgramAgreementProductLine> = [];
    productServicesSelections: Array<IProductServicesSelection> = [];

    constructor(model: IProgramAgreementReviewModel) {
        if (model) {
            // Base properties
            if (model.hasOwnProperty("_id")) {
                this._id = model._id;
            }
            if (model.hasOwnProperty("userId")) {
                this.userId = model.userId;
            }
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("key")) {
                this.key = model.key;
            }
            if (model.hasOwnProperty("divisionTypeKey")) {
                this.divisionTypeKey = model.divisionTypeKey;
            }
            if (model.hasOwnProperty("programAgreementStateKey")) {
                this.programAgreementStateKey = model.programAgreementStateKey;
            }
            if (model.hasOwnProperty("hasSupplyOrder")) {
                this.hasSupplyOrder = model.hasSupplyOrder;
            }
            if (model.hasOwnProperty("sponsorApproved")) {
                this.sponsorApproved = model.sponsorApproved;
            }
            if (model.hasOwnProperty("sponsorSignature")) {
                this.sponsorSignature = model.sponsorSignature;
            }
            if (model.hasOwnProperty("salesRepApproved")) {
                this.salesRepApproved = model.salesRepApproved;
            }
            if (model.hasOwnProperty("salesRepSignature")) {
                this.salesRepSignature = model.salesRepSignature;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("submissionPending")) {
                this.submissionPending = model.submissionPending;
            }
            if (model.hasOwnProperty("hasRuleViolations")) {
                this.hasRuleViolations = model.hasRuleViolations;
            }

            // ProgramModel
            if (model.hasOwnProperty("program")) {
                let program: ProgramModel = new ProgramModel(model.program);
                this.startDate = program.startDate;
                this.signDate = program.signDate;
                this.cancellationDate = program.cancellationDate;
                this.estimatedGroupSize = program.estimatedGroupSize;
                this.estimatedWholesale = program.estimatedWholesale;
                this.estimatedNumberOfClassrooms = program.estimatedNumberOfClassrooms;
                this.purchaseOrderTypeKey = program.purchaseOrderTypeKey;
                this.purchaseOrderNumber = program.purchaseOrderNumber;
                //todo
                // this.minimumDonationAmount = program.minimumDonationAmount;
                this.isDonationsEnabled = program.isDonationsEnabled;
                this.isFullServiceEnabled = program.isFullServiceEnabled;
                this.is59MinuteEnabled = program.is59MinuteEnabled;
                this.isPaymentAppEnabled = program.isPaymentAppEnabled;
                this.isTuitionRewardsEnabled = program.isTuitionRewardsEnabled;
                this.isShippingToAccountAllowed = program.isShippingToAccountAllowed;
            }

            // Brochures
            if (model.brochures) {
                model.brochures.forEach((brochure: BrochureModel) => {
                    this.programAgreementProductLines.push(new ProgramAgreementProductLine(brochure));
                });
            }

            // ProductServicesModel
            if (model.productServices) {
                this.productServicesSelections.push(new ProductServicesSelection(model.productServices));
                if (model.productServices.warehouseInstructions.content) {
                    this.programAgreementNotes.push(model.productServices.warehouseInstructions);
                }
                if (model.productServices.carrierInstructions.content) {
                    this.programAgreementNotes.push(model.productServices.carrierInstructions);
                }
            }

            // InfoOtherModel
            if (model.hasOwnProperty("invoiceOther")) {
                let invoiceOther: InvoiceOtherModel = new InvoiceOtherModel(model.invoiceOther);
                this.accountTaxClassificationKey = invoiceOther.accountTaxClassificationKey;
                this.accountTaxClassificationExpirationDate = invoiceOther.accountTaxClassificationExpirationDate;
                this.collationFeeAmount = invoiceOther.collationFeeAmount;
                this.prizeCostAmount = invoiceOther.prizeCostAmount;
                this.prizeCostPercentage = invoiceOther.prizeCostPercentage;
                if (invoiceOther.noteForOrderProcessing.content) {
                    this.programAgreementNotes.push(invoiceOther.noteForOrderProcessing);
                }
            }

            // Program Agreement Partners
            if (model.programAgreementPartners) {
                model.programAgreementPartners.forEach((partner: IProgramAgreementPartner) => {
                    this.programAgreementPartners.push(new ProgramAgreementPartner(partner));
                });
            }
        }
    }
}