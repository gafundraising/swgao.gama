/**
 * Created by mjwheatley on 7/25/16.
 */
import {PartnerContactAddress} from "./Address";
import {Constants} from "../Constants";
import {IPartnerContact} from "./ProgramAgreementDownloadResponseModel";
import {Enums} from "../Enums";

export interface IOrganizationModel {
    _id?: string,
    name?: string,
    phone?: string,
    fax?: string,
    addresses?: {
        physical?: PartnerContactAddress,
        mailing?: PartnerContactAddress,
        differentMailingAddress?: boolean
    },
    partnerAccountKey?: string,
    partnerContact?: IPartnerContact
}

export class OrganizationModel implements IOrganizationModel {
    public _id: string;
    public name: string = Constants.EMPTY_STRING;
    public phone: string = Constants.EMPTY_STRING;
    public fax: string = Constants.EMPTY_STRING;
    public addresses: {
        physical?: PartnerContactAddress,
        mailing?: PartnerContactAddress,
        differentMailingAddress?: boolean
    } = {
        physical: new PartnerContactAddress({
            addressTypeKey: Enums.ADDRESS_TYPE.P,
        }),
        mailing: new PartnerContactAddress({
            addressTypeKey: Enums.ADDRESS_TYPE.M
        }),
        differentMailingAddress: false
    };
    public partnerAccountKey: string;
    public partnerContact: IPartnerContact;

    constructor(orgInfo?: IOrganizationModel) {
        if (orgInfo) {
            if (orgInfo.hasOwnProperty("_id")) {
                this._id = orgInfo._id;
            }
            if (orgInfo.hasOwnProperty("name")) {
                this.name = orgInfo.name;
            }
            if (orgInfo.hasOwnProperty("phone")) {
                this.phone = orgInfo.phone;
            }
            if (orgInfo.hasOwnProperty("fax")) {
                this.fax = orgInfo.fax;
            }
            if (orgInfo.hasOwnProperty("addresses")) {
                this.addresses = orgInfo.addresses;
            }
            if (orgInfo.hasOwnProperty("partnerAccountKey")) {
                this.partnerAccountKey = orgInfo.partnerAccountKey;
            }
            if (orgInfo.hasOwnProperty("partnerContact")) {
                this.partnerContact = orgInfo.partnerContact;
            }
        }
    }
}