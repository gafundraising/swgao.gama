/**
 * Created by mjwheatley on 7/25/16.
 */
import {Address, PartnerContactAddress} from "./Address";
import {SponsorModel} from "./SponsorModel";
import {Constants} from "../Constants";
import {IPartnerContact} from "./ProgramAgreementDownloadResponseModel";

export interface IGroupModel {
    _id?: string,
    orgId?: string,
    name?: string,
    address?: PartnerContactAddress,
    sponsor?: SponsorModel,
    onlineStoreId?: string,
    partnerAccountKey?: string,
    partnerContact?: IPartnerContact
}

export class GroupModel implements IGroupModel {
    public _id: string;
    public orgId: string;
    public name: string = Constants.EMPTY_STRING;
    public address: PartnerContactAddress = new PartnerContactAddress();
    public sponsor: SponsorModel = new SponsorModel();
    public onlineStoreId: string;
    public partnerAccountKey: string;
    public partnerContact: IPartnerContact;

    constructor(groupInfo?: IGroupModel) {
        if (groupInfo) {
            if (groupInfo.hasOwnProperty("_id")) {
                this._id = groupInfo._id;
            }
            if (groupInfo.hasOwnProperty("orgId")) {
                this.orgId = groupInfo.orgId;
            }
            if (groupInfo.hasOwnProperty("name")) {
                this.name = groupInfo.name;
            }
            if (groupInfo.hasOwnProperty("address")) {
                this.address = groupInfo.address;
            }
            if (groupInfo.hasOwnProperty("sponsor")) {
                this.sponsor = groupInfo.sponsor;
            }
            if (groupInfo.hasOwnProperty("onlineStoreId")) {
                this.onlineStoreId = groupInfo.onlineStoreId;
            }
            if (groupInfo.hasOwnProperty("partnerAccountKey")) {
                this.partnerAccountKey = groupInfo.partnerAccountKey;
            }
            if (groupInfo.hasOwnProperty("partnerContact")) {
                this.partnerContact = groupInfo.partnerContact;
            }
        }
    }
}
