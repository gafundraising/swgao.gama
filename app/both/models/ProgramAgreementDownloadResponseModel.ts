import {Constants} from "../Constants";
import * as moment from "moment";
import * as uuid from "uuid/v4";
import {PartnerContactAddress} from "./Address";

export interface IPartnerContactAddress {
    id?: string,
    guid?: string,
    partnerContactID?: string,
    addressTypeKey?: string,
    street?: string,
    city?: string,
    county?: string,
    stateProvinceKey?: string,
    postalCode?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    partnerContact?: {
        ref?: string
    }
}

export interface IPartnerContactPhone {
    id?: string,
    guid?: string,
    partnerContactID?: string,
    phoneTypeKey?: string,
    number?: string,
    extension?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    partnerContact?: {
        ref?: string
    }
}

export class PartnerContactPhone implements IPartnerContactPhone {
    id: string;
    guid: string = uuid();
    partnerContactID: string;
    phoneTypeKey: string;
    number: string = Constants.EMPTY_STRING;
    extension: string = Constants.EMPTY_STRING;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    partnerContact: {
        ref?: string
    } = {ref: Constants.EMPTY_STRING};

    constructor(model?:IPartnerContactPhone) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("partnerContactID")) {
                this.partnerContactID = model.partnerContactID;
            }
            if (model.hasOwnProperty("phoneTypeKey")) {
                this.phoneTypeKey = model.phoneTypeKey;
            }
            if (model.hasOwnProperty("number")) {
                this.number = model.number;
            }
            if (model.hasOwnProperty("extension")) {
                this.extension = model.extension;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("partnerContact")) {
                this.partnerContact = model.partnerContact;
            }
        }
    }
}

export interface IPartner {
    id?: string,
    guid?: string,
    accountKey?: string,
    partnerTypeCode?: number,
    partnerContactID?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    partnerContact?: {
        ref?: string
    },
    partnerAttributes?: Array<IPartnerAttribute>,
    partnerRelationsAsChild?: Array<any>,
    partnerRelationsAsParent?: Array<IPartnerRelationsAsParent>
}

export class Partner implements IPartner {
    id: string;
    guid: string = uuid();
    accountKey: string;
    partnerTypeCode: number;
    partnerContactID: string;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = null;
    modifiedBy: number = null;
    version: string;
    partnerContact: {
        ref?: string
    } = {ref: Constants.EMPTY_STRING};
    partnerAttributes: Array<IPartnerAttribute> = [];
    partnerRelationsAsChild: Array<any> = [];
    partnerRelationsAsParent: Array<IPartnerRelationsAsParent> = [];

    constructor(model?:IPartner) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("partnerContactID")) {
                this.partnerContactID = model.partnerContactID;
            }
            if (model.hasOwnProperty("accountKey")) {
                this.accountKey = model.accountKey;
            }
            if (model.hasOwnProperty("partnerTypeCode")) {
                this.partnerTypeCode = model.partnerTypeCode;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("partnerContact")) {
                this.partnerContact = model.partnerContact;
            }
            if (model.hasOwnProperty("partnerAttributes")) {
                model.partnerAttributes.forEach((attr:IPartnerAttribute) => {
                    this.partnerAttributes.push(new PartnerAttribute(attr));
                });
            }
            if (model.hasOwnProperty("partnerRelationsAsParent")) {
                model.partnerRelationsAsParent.forEach((relation:IPartnerRelationsAsParent) => {
                    this.partnerRelationsAsParent.push(new PartnerRelationsAsParent(relation));
                });
            }
        }
    }
}

export interface IPartnerAttribute {
    id?: string,
    guid?: string,
    partnerID?: string,
    partnerAttributeTypeCode?: number,
    value?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    partner?: {
        ref?: string
    }
}

export class PartnerAttribute implements IPartnerAttribute {
    id: string;
    guid: string = uuid();
    partnerID: string;
    partnerAttributeTypeCode: number;
    value: string = Constants.EMPTY_STRING;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    partner: {
        ref?: string
    } = {ref: Constants.EMPTY_STRING};

    constructor(model?:IPartnerAttribute) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("partnerID")) {
                this.partnerID = model.partnerID;
            }
            if (model.hasOwnProperty("partnerAttributeTypeCode")) {
                this.partnerAttributeTypeCode = model.partnerAttributeTypeCode;
            }
            if (model.hasOwnProperty("value")) {
                this.value = model.value;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("partner")) {
                this.partner = model.partner;
            }
        }
    }
}

export interface IPartnerRelationsAsParent {
    id?: string,
    guid?: string,
    parentPartnerID?: string,
    childPartnerID?: string,
    created?: string,
    createdBy?: number,
    childPartner?: any,
    parentPartner?: {
        ref?: string
    },
    modified?: string
}

export class PartnerRelationsAsParent implements IPartnerRelationsAsParent {
    id: string;
    guid: string = uuid();
    parentPartnerID: string;
    childPartnerID: string;
    childPartner: {
        ref?: string
    } = null;
    parentPartner: {
        ref?: string
    } = {ref: Constants.EMPTY_STRING};
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = null;

    constructor(model?:IPartnerRelationsAsParent) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("parentPartnerID")) {
                this.parentPartnerID = model.parentPartnerID;
            }
            if (model.hasOwnProperty("childPartnerID")) {
                this.childPartnerID = model.childPartnerID;
            }
            if (model.hasOwnProperty("childPartner")) {
                this.childPartner = model.childPartner;
            }
            if (model.hasOwnProperty("parentPartner")) {
                this.parentPartner = model.parentPartner;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
        }
    }
}

export interface IPartnerContact {
    _id?: string,
    userId?: string,
    id?: string,
    guid?: string,
    addressKey?: string,
    isMasterData?: boolean,
    name?: string,
    emailAddress?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    partnerContactAddresses?: Array<IPartnerContactAddress>,
    partnerContactPhones?: Array<IPartnerContactPhone>,
    programAgreementPartners?: Array<IProgramAgreementPartner>,
    partners?: Array<IPartner>
}

export class PartnerContact implements IPartnerContact {
    _id: string;
    userId: string;
    id: string;
    guid: string = uuid();
    addressKey: string;
    isMasterData: boolean = false;
    name: string;
    emailAddress: string;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    partnerContactAddresses: Array<IPartnerContactAddress> = [];
    partnerContactPhones: Array<IPartnerContactPhone> = [];
    programAgreementPartners: Array<IProgramAgreementPartner> = [];
    partners: Array<IPartner> = [];

    constructor(model?: IPartnerContact) {
        if (model) {
            if (model.hasOwnProperty("_id")) {
                this._id = model._id;
            }
            if (model.hasOwnProperty("userId")) {
                this.userId = model.userId;
            }
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("addressKey")) {
                this.addressKey = model.addressKey;
            }
            if (model.hasOwnProperty("isMasterData")) {
                this.isMasterData = model.isMasterData;
            }
            if (model.hasOwnProperty("name")) {
                this.name = model.name;
            }
            if (model.hasOwnProperty("emailAddress")) {
                this.emailAddress = model.emailAddress;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("partnerContactAddresses")) {
                model.partnerContactAddresses.forEach((address: IPartnerContactAddress) => {
                    this.partnerContactAddresses.push(new PartnerContactAddress(address));
                });
            }
            if (model.hasOwnProperty("partnerContactPhones")) {
                model.partnerContactPhones.forEach((phone: IPartnerContactPhone) => {
                    this.partnerContactPhones.push(new PartnerContactPhone(phone));
                });
            }
            if (model.hasOwnProperty("partners")) {
                model.partners.forEach((partner: IPartner) => {
                    this.partners.push(new Partner(partner));
                });
            }
        }
    }
}

export interface IProgramAgreementPartner {
    id?: string,
    guid?: string,
    programAgreementID?: string,
    partnerContactID?: string,
    partnerFunctionKey?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    partnerContact?: any,
    programAgreement?: {
        ref?: string
    }
}

export class ProgramAgreementPartner implements IProgramAgreementPartner {
    id: string;
    guid: string = uuid();
    programAgreementID: string;
    partnerContactID: string;
    partnerFunctionKey: string;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    partnerContact: any = null;
    programAgreement: {
        ref?: string
    } = {ref: Constants.EMPTY_STRING};

    constructor(model?: IProgramAgreementPartner) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = model.programAgreementID;
            }
            if (model.hasOwnProperty("partnerContactID")) {
                this.partnerContactID = model.partnerContactID;
            }
            if (model.hasOwnProperty("partnerFunctionKey")) {
                this.partnerFunctionKey = model.partnerFunctionKey;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("programAgreement")) {
                if (model.programAgreement && model.programAgreement.ref) {
                    this.programAgreement = {
                        ref: model.programAgreement.ref
                    };
                }
            }
        }
    }
}

export interface IProgramAgreementProductLine {
    id?: string,
    guid?: string,
    programAgreementID?: string,
    brochureTypeKey?: string,
    brochureGroupKey?: string,
    brochureCodeKey?: string,
    brochureMaterialKey?: string,
    productLineMasterMaterialKey?: string,
    productLineMasterMaterialDescription?: string,
    productLineMaterialKey?: string,
    productLineMaterialDescription?: string,
    shippingLevelKey?: string,
    priceCodeKey?: string,
    taxOnPaperworkIndicator?: string,
    profitLevelKey?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    programAgreement?: {
        ref?: string
    }
}

export interface IProductServicesSelection {
    id?: string,
    guid?: string,
    programAgreementID?: string,
    prizeProgramKey?: string,
    goalPrizeLevel?: number,
    goalPrizeChargeTypeKey?: string,
    prizePrePackKey?: string,
    collectionMethodKey?: string,
    aplusServiceTypeKey?: string,
    requestedDeliveryTypeKey?: string,
    requestedDeliveryDate?: string,
    lastFullDayDate?: string,
    donationQty1?: number,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    programAgreement?: {
        ref?: string
    }
}

export interface IProgramAgreementNote {
    id?: string,
    guid?: string,
    programAgreementID?: string,
    programAgreementNoteTypeKey?: string,
    content?: string,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    programAgreement?: {
        ref?: string
    }
}

export class ProgramAgreementNote implements IProgramAgreementNote {
    id: string;
    guid: string = uuid();
    programAgreementID: string;
    programAgreementNoteTypeKey: string;
    content: string = Constants.EMPTY_STRING;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    programAgreement: {
        ref: string
    } = {ref: Constants.EMPTY_STRING};

    constructor(note?: IProgramAgreementNote) {
        if (note) {
            if (note.hasOwnProperty("id")) {
                this.id = note.id;
            }
            if (note.hasOwnProperty("guid")) {
                this.guid = note.guid;
            }
            if (note.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = note.programAgreementID;
            }
            if (note.hasOwnProperty("programAgreementNoteTypeKey")) {
                this.programAgreementNoteTypeKey = note.programAgreementNoteTypeKey;
            }
            if (note.hasOwnProperty("content")) {
                this.content = note.content;
            }
            if (note.hasOwnProperty("created")) {
                this.created = note.created;
            }
            if (note.hasOwnProperty("createdBy")) {
                this.createdBy = note.createdBy;
            }
            if (note.hasOwnProperty("modified")) {
                this.modified = note.modified;
            }
            if (note.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = note.modifiedBy;
            }
            if (note.hasOwnProperty("version")) {
                this.version = note.version;
            }
            if (note.hasOwnProperty("programAgreement")) {
                if (note.programAgreement && note.programAgreement.ref) {
                    this.programAgreement = {
                        ref: note.programAgreement.ref
                    };
                }
            }
        }
    }
}

export interface IProgramAgreement {
    _id?: string,
    userId?: string,
    id?: string,
    guid?: string,
    key?: string,
    divisionTypeKey?: string,
    programAgreementStateKey?: string,
    startDate?: string,
    signDate?: string,
    cancellationDate?: string,
    estimatedGroupSize?: number,
    estimatedWholesale?: number,
    estimatedNumberOfClassrooms?: number,
    purchaseOrderTypeKey?: string,
    purchaseOrderNumber?: string,
    accountTaxClassificationKey?: string,
    accountTaxClassificationExpirationDate?: string,
    defaultStorefrontTypeKey?: string,
    hasSupplyOrder?: boolean,
    collationFeeAmount?: number,
    prizeCostAmount?: number,
    prizeCostPercentage?: number,
    sponsorSignature?: any,
    sponsorApproved?: any,
    salesRepSignature?: any,
    salesRepApproved?: any,
    submissionPending?: any,
    hasRuleViolations?: boolean,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    minimumDonationAmount?: number,
    isDonationsEnabled?: boolean,
    isFullServiceEnabled?: boolean,
    is59MinuteEnabled?: boolean,
    isTuitionRewardsEnabled?: boolean,
    isPaymentAppEnabled?: boolean,
    isShippingToAccountAllowed?: boolean,
    programAgreementNotes?: Array<IProgramAgreementNote>,
    programAgreementPartners?: Array<IProgramAgreementPartner>,
    programAgreementProductLines?: Array<IProgramAgreementProductLine>,
    productServicesSelections?: Array<IProductServicesSelection>
}

export interface IProgramAgreementDownloadResponse {
    _id?: string,
    userId?: string,
    timestamp?: string
    partnerContacts?: Array<IPartnerContact>,
    programAgreements?: Array<IProgramAgreement>
}