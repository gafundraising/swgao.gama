/**
 * Created by mjwheatley on 7/25/16.
 */
import * as moment from 'moment';
import * as uuid from "uuid/v4";
import {Constants} from "../Constants";
import {IProductServicesSelection, ProgramAgreementNote} from "./ProgramAgreementDownloadResponseModel";
import {Enums} from "../Enums";

export interface IProductServicesModel {
    id?: string,
    guid?: string,
    programAgreementID?: string,
    warehouseInstructions?: ProgramAgreementNote,
    carrierInstructions?: ProgramAgreementNote,
    collectionMethodKey?: string,
    aplusServiceTypeKey?: string,
    prizeProgramKey?: string,
    prizePrePackKey?: string,
    goalPrizeLevel?: number,
    goalPrizeChargeTypeKey?: string,
    requestedDeliveryTypeKey?: string,
    requestedDeliveryDate?: string,
    lastFullDayDate?: string,
    donationQty1?: number,
    created?: string,
    createdBy?: number,
    modified?: string,
    modifiedBy?: number,
    version?: string,
    programAgreement?: { ref?: string },
    translations?: IProductServicesModel
}

export class ProductServicesModel implements IProductServicesModel, IProductServicesSelection {
    public id: string;
    public guid: string = uuid();
    public programAgreementID: string;
    public warehouseInstructions: ProgramAgreementNote = new ProgramAgreementNote({
        programAgreementNoteTypeKey: Enums.PROGRAM_AGREEMENT_NOTE_TYPE.W
    });
    public carrierInstructions: ProgramAgreementNote = new ProgramAgreementNote({
        programAgreementNoteTypeKey: Enums.PROGRAM_AGREEMENT_NOTE_TYPE.C
    });
    public collectionMethodKey: string = Constants.EMPTY_STRING;
    public aplusServiceTypeKey: string = Constants.EMPTY_STRING;
    public prizeProgramKey: string = Constants.EMPTY_STRING;
    public prizePrePackKey: string = Constants.EMPTY_STRING;
    public goalPrizeLevel: number = 0;
    public goalPrizeChargeTypeKey: string = Constants.EMPTY_STRING;
    public requestedDeliveryTypeKey: string = Constants.EMPTY_STRING;
    public requestedDeliveryDate: string = moment().toISOString();
    public lastFullDayDate: string = moment().toISOString();
    public donationQty1: number = 0;
    public created: string = moment().toISOString();
    public createdBy: number = 0;
    public modified: string = moment().toISOString();
    public modifiedBy: number = 0;
    public version: string = Constants.EMPTY_STRING;
    public programAgreement: { ref?: string } = {ref: Constants.EMPTY_STRING};

    constructor(productServices?: IProductServicesModel) {
        if (productServices) {
            if (productServices.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = productServices.programAgreementID;
            }
            if (productServices.hasOwnProperty("warehouseInstructions")) {
                this.warehouseInstructions = new ProgramAgreementNote(productServices.warehouseInstructions);
            }
            if (productServices.hasOwnProperty("carrierInstructions")) {
                this.carrierInstructions = new ProgramAgreementNote(productServices.carrierInstructions);
            }
            if (productServices.hasOwnProperty("collectionMethodKey")) {
                this.collectionMethodKey = productServices.collectionMethodKey;
            }
            if (productServices.hasOwnProperty("aplusServiceTypeKey")) {
                this.aplusServiceTypeKey = productServices.aplusServiceTypeKey;
            }
            if (productServices.hasOwnProperty("prizeProgramKey")) {
                this.prizeProgramKey = productServices.prizeProgramKey;
            }
            if (productServices.hasOwnProperty("prizePrePackKey")) {
                this.prizePrePackKey = productServices.prizePrePackKey;
            }
            if (productServices.hasOwnProperty("goalPrizeLevel")) {
                this.goalPrizeLevel = productServices.goalPrizeLevel;
            }
            if (productServices.hasOwnProperty("goalPrizeChargeTypeKey")) {
                this.goalPrizeChargeTypeKey = productServices.goalPrizeChargeTypeKey;
            }
            if (productServices.hasOwnProperty("requestedDeliveryTypeKey")) {
                this.requestedDeliveryTypeKey = productServices.requestedDeliveryTypeKey;
            }
            if (productServices.hasOwnProperty("requestedDeliveryDate")) {
                this.requestedDeliveryDate = productServices.requestedDeliveryDate;
            }
            if (productServices.hasOwnProperty("lastFullDayDate")) {
                this.lastFullDayDate = productServices.lastFullDayDate;
            }
            if (productServices.hasOwnProperty("donationQty1")) {
                this.donationQty1 = productServices.donationQty1;
            }
            if (productServices.hasOwnProperty("created")) {
                this.created = productServices.created;
            }
            if (productServices.hasOwnProperty("createdBy")) {
                this.createdBy = productServices.createdBy;
            }
            if (productServices.hasOwnProperty("modified")) {
                this.modified = productServices.modified;
            }
            if (productServices.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = productServices.modifiedBy;
            }
            if (productServices.hasOwnProperty("version")) {
                this.version = productServices.version;
            }
            if (productServices.hasOwnProperty("programAgreement")) {
                this.programAgreement = productServices.programAgreement;
            }
        }

        if (this.programAgreementID && !this.warehouseInstructions.programAgreementID) {
            this.warehouseInstructions.programAgreementID = this.programAgreementID;
        }
        if (this.programAgreement.ref && !this.warehouseInstructions.programAgreement.ref) {
            this.warehouseInstructions.programAgreement.ref = this.programAgreement.ref;
        }

        if (this.programAgreementID && !this.carrierInstructions.programAgreementID) {
            this.carrierInstructions.programAgreementID = this.programAgreementID;
        }
        if (this.programAgreement.ref && !this.carrierInstructions.programAgreement.ref) {
            this.carrierInstructions.programAgreement.ref = this.programAgreement.ref;
        }
    }
}

export class ProductServicesSelection implements IProductServicesSelection {
    id: string;
    guid: string = uuid();
    programAgreementID: string;
    prizeProgramKey: string;
    goalPrizeLevel: number;
    goalPrizeChargeTypeKey: string;
    prizePrePackKey: string;
    collectionMethodKey: string;
    aplusServiceTypeKey: string;
    requestedDeliveryTypeKey: string;
    requestedDeliveryDate: string;
    lastFullDayDate: string;
    donationQty1: number;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    programAgreement: {
        ref: string
    } = {ref: Constants.EMPTY_STRING};

    constructor(model?: IProductServicesSelection) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = model.programAgreementID;
            }
            if (model.hasOwnProperty("prizeProgramKey")) {
                this.prizeProgramKey = model.prizeProgramKey;
            }
            if (model.hasOwnProperty("goalPrizeLevel")) {
                this.goalPrizeLevel = model.goalPrizeLevel;
            }
            if (model.hasOwnProperty("goalPrizeChargeTypeKey")) {
                this.goalPrizeChargeTypeKey = model.goalPrizeChargeTypeKey;
            }
            if (model.hasOwnProperty("prizePrePackKey")) {
                this.prizePrePackKey = model.prizePrePackKey;
            }
            if (model.hasOwnProperty("collectionMethodKey")) {
                this.collectionMethodKey = model.collectionMethodKey;
            }
            if (model.hasOwnProperty("aplusServiceTypeKey")) {
                this.aplusServiceTypeKey = model.aplusServiceTypeKey;
            }
            if (model.hasOwnProperty("requestedDeliveryTypeKey")) {
                this.requestedDeliveryTypeKey = model.requestedDeliveryTypeKey;
            }
            if (model.hasOwnProperty("requestedDeliveryDate")) {
                this.requestedDeliveryDate = model.requestedDeliveryDate;
            }
            if (model.hasOwnProperty("lastFullDayDate")) {
                this.lastFullDayDate = model.lastFullDayDate;
            }
            if (model.hasOwnProperty("donationQty1")) {
                this.donationQty1 = model.donationQty1;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("programAgreement")) {
                if (model.programAgreement && model.programAgreement.ref) {
                    this.programAgreement = {
                        ref: model.programAgreement.ref
                    };
                }
            }
        }
    }
}