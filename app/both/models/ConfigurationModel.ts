export interface IConfigurationModel {
    _id?: string,
    salesRepAccountNumber?: string,
    salesRepPriceListType?: string,
    hasSubordinates?: boolean,
    portalUri?: string,
    showBonusAndIncentivesChart?: boolean,
    showContractsChart?: boolean,
    showSalesGoalChart?: boolean,
    termsAndConditions?: string,
    signatureVerbiage?: string,
    salesRepActivity?: ISalesRepActivity,
    profitSalesLevels?: Array<IProfitSalesLevel>,
    productLineEntries?: Array<IProductLineEntry>,
    brochureTypes?: Array<IKeyText>,
    brochureGroups?: Array<IKeyText>,
    brochureCodes?: Array<IKeyText>,
    brochureMaterials?: Array<IKeyText>
    productLineMasters?: Array<IKeyText>,
    productLineMaterials?: Array<IKeyText>,
    productLineMaterialGroup4Mappings?: Array<IProductLineMaterialGroup4Mapping>,
    materialGroup4Requirements?: Array<IMaterialGroup4Requirement>,
    serviceGroupShippingLevels?: Array<IServiceGroupShippingLevel>,
    shippingLevels?: Array<IKeyText>,
    productLineShippingLevelProfitLevelMappings?: Array<IProductLineShippingLevelProfitLevelMapping>,
    profitLevels?: Array<IKeyText>,
    priceListTypes?: Array<any>,
    taxOnPaperworkRules?: Array<ITaxOnPaperworkRule>,
    prizeGroupPrizePrograms?: Array<IPrizeGroupPrizeProgram>,
    prizePrograms?: Array<IKeyText>,
    prizePrePackIndicators?: Array<IKeyText>,
    prizeProgramOptions?: Array<IPrizeProgramOption>,
    goalPrizeChargeIndicators?: Array<IKeyText>,
    purchaseOrderRequirements?: Array<IKeyText>,
    collectionMethods?: Array<IKeyText>,
    requestedDeliveryTypes?: Array<IKeyText>,
    serviceLevels?: Array<IKeyText>,
    seasons?: Array<ISeason>,
    schoolLevels?: Array<IKeyText>,
    schoolTypes?: Array<IKeyText>,
    marketSegments?: Array<IKeyText>,
    groupTypes?: Array<IKeyText>,
    taxClassifications?: Array<IKeyText>,
    phoneTypes?: Array<IKeyText>,
    stateProvinces?: Array<IStateProvince>,
    donationDefault?: IDonationDefault
}

export interface ISalesRepActivity {
    period?: number,
    periodStart?: string,
    periodEnd?: string,
    commissionAsOf?: string,
    incentiveTripGoal?: number,
    incentiveTripActual?: number,
    growthBonusGoal?: number,
    growthBonusActual?: number,
    milestoneClubGoal?: number,
    presidentsClubGoal?: number,
    chairmansClubGoal?: number,
    chairmansGoldClubGoal?: number,
    chairmansPlatinumClubGoal?: number,
    chairmansEliteClubGoal?: number,
    clubSalesActual?: number,
    profitabilitySalesActual?: number,
    accountsSignedNowPriorWeek?: number,
    accountsSignedNowCurrentWeek?: number,
    accountsSignedTotalPriorSeasonToDate?: number,
    accountsSignedTotalCurrentSeasonToDate?: number,
    accountsSignedTotalPriorYearToDate?: number,
    accountsSignedTotalCurrentYearToDate?: number,
    grandTotalNetSalesPriorWeek?: number,
    grandTotalNetSalesCurrentWeek?: number,
    grandTotalNetSalesPriorSeasonToDate?: number,
    grandTotalNetSalesCurrentSeasonToDate?: number,
    grandTotalNetSalesPriorYearToDate?: number,
    grandTotalNetSalesCurrentYearToDate?: number,
    fallSalesGoalMagazine?: number,
    fallSalesGoalTotal?: number,
    fallActualSalesMagazine?: number,
    fallActualSalesTotal?: number,
    fallSalesToGoalMagazine?: number,
    fallSalesToGoalTotal?: number,
    springSalesGoalMagazine?: number,
    springSalesGoalTotal?: number
    springActualSalesMagazine?: number,
    springActualSalesTotal?: number,
    springSalesToGoalMagazine?: number,
    springSalesToGoalTotal?: number,
    yearSalesGoalMagazine?: number,
    yearSalesGoalTotal?: number,
    yearActualSalesMagazine?: number,
    yearActualSalesTotal?: number,
    yearSalesToGoalMagazine?: number,
    yearSalesToGoalTotal?: number,
    studentsSignedPriorSeasonToDate?: number,
    studentsSignedCurrentSeasonToDate?: number,
    garpStudentsPriorSeasonToDate?: number,
    garpStudentsCurrentSeasonToDate?: number,
    wholesalePerGroupPriorSeasonToDate?: number,
    wholesalePerGroupCurrentSeasonToDate?: number,
    averageGroupSizePriorSeasonToDate?: number,
    averageGroupSizeCurrentSeasonToDate?: number,
    averageItemsPriorSeasonToDate?: number,
    averageItemsCurrentSeasonToDate?: number,
    averageWholesalePriorSeasonToDate?: number,
    averageWholesaleCurrentSeasonToDate?: number,
    averageParticipationPercentagePriorSeasonToDate?: number,
    averageParticipationPercentageCurrentSeasonToDate?: number
}

export interface IProfitSalesLevel {
    salesLevel?: number,
    salaryEarned?: number
}

export interface IProductLineEntry {
    salesRepPriceListType?: string,
    brochureType?: string,
    brochureGroup?: string,
    brochureCode?: string,
    brochureMaterialNumber?: string,
    masterProductLineMaterialNumber?: string,
    productLineMaterialNumber?: string,
    serviceGroup?: string,
    prizeGroup?: string,
    priceListType?: string,
    validFrom?: string,
    validTo?: string,
    materialGroup?: string
}

export interface IKeyText {
    key?: string,
    text?: string
}

export interface IProductLineMaterialGroup4Mapping {
    productLineMaterial?: number,
    materialGroup4?: string,
    exclusionGroupIndicator?: string
}

export interface IMaterialGroup4Requirement {
    materialGroup4?: string,
    includeMaterialGroup4?: string,
    excludeAllMaterialGroup4?: string,
    includeMaterialGroup4Msg?: string,
    excludeMaterialGroup4Msg?: string
}

export interface IServiceGroupShippingLevel {
    serviceGroup?: string,
    shippingLevel?: string
}

export interface IProductLineShippingLevelProfitLevelMapping {
    masterProductLineMaterialNumber?: string,
    productLineMaterialNumber?: string,
    shippingLevel?: string,
    profitLevel?: string
}

export interface ITaxOnPaperworkRule {
    division?: number,
    priceListType?: string,
    indicator?: string,
    isDefault?: boolean,
    description?: string
}

export interface IPrizeGroupPrizeProgram {
    groupingDesignation?: string,
    prizeProgram?: string
}

export interface IPrizeProgramOption {
    prizeProgram?: string,
    shippingLevel?: number,
    prizePrepackIndicator?: string,
    goalPrizeFlag?: number,
    default?: number
}

export interface ISeason {
    descriptor?: string,
    start?: string,
    end?: string
}

export interface IStateProvince {
    key?: string,
    countryKey?: string,
    text?: string
}

export interface IDonationDefault {
    minimumDonationAmount?: number,
    minimumDonationAmountMin?: number,
    minimumDonationAmountMax?: number,
    donationQty1?: number,
    donationQty1Min?: number,
    donationQty1Max?: number
}
