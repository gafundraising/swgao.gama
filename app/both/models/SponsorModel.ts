/**
 * Created by mjwheatley on 7/25/16.
 */
import {Constants} from "../Constants";
import {IPartnerContact, IPartnerContactPhone} from "./ProgramAgreementDownloadResponseModel";

export interface ISponsorModel {
    name?: string,
    emailAddress?: string,
    phoneNumbers?: {
        mobile?: IPartnerContactPhone,
        home?: IPartnerContactPhone,
        work?: IPartnerContactPhone
    },
    partnerAccountKey?: string,
    partnerContact?: IPartnerContact
}

export class SponsorModel implements ISponsorModel {
    public name: string = Constants.EMPTY_STRING;
    public emailAddress: string = Constants.EMPTY_STRING;
    public phoneNumbers: { mobile?: IPartnerContactPhone, home?: IPartnerContactPhone, work?: IPartnerContactPhone } = {
        mobile: {number: Constants.EMPTY_STRING},
        home: {number: Constants.EMPTY_STRING},
        work: {number: Constants.EMPTY_STRING}
    };
    public partnerAccountKey: string;
    public partnerContact: IPartnerContact;

    constructor(sponsor?: ISponsorModel) {
        if (sponsor) {
            if (sponsor.hasOwnProperty("name")) {
                this.name = sponsor.name;
            }
            if (sponsor.hasOwnProperty("emailAddress")) {
                this.emailAddress = sponsor.emailAddress;
            }
            if (sponsor.hasOwnProperty("phoneNumbers")) {
                this.phoneNumbers = sponsor.phoneNumbers;
            }
            if (sponsor.hasOwnProperty("partnerAccountKey")) {
                this.partnerAccountKey = sponsor.partnerAccountKey;
            }
            if (sponsor.hasOwnProperty("partnerContact")) {
                this.partnerContact = sponsor.partnerContact;
            }
        }
    }
}