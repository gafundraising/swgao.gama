/**
 * Created by mjwheatley on 8/13/18.
 */
import {IProgramAgreementProductLine} from "./ProgramAgreementDownloadResponseModel";
import * as moment from "moment";
import * as uuid from "uuid/v4";
import {Constants} from "../Constants";

export class BrochureModel implements IProgramAgreementProductLine {
    public id: string;
    public guid: string = uuid();
    public programAgreementID: string;
    public brochureTypeKey: string;
    public brochureTypeDescription: string;
    public brochureGroupKey: string;
    public brochureCodeKey: string;
    public brochureMaterialKey: string;
    public productLineMasterMaterialKey: string;
    public productLineMasterMaterialDescription: string;
    public productLineMaterialKey: string;
    public productLineMaterialDescription: string;
    public shippingLevelKey: string;
    public priceCodeKey: string;
    public taxOnPaperworkIndicator: string;
    public taxOnPaperworkDescription: string;
    public profitLevelKey: string;
    public created: string = moment().toISOString();
    public createdBy: number = 0;
    public modified: string = moment().toISOString();
    public modifiedBy: number = 0;
    public version: string;
    public programAgreement: {
        ref?: string
    };

    constructor(model?: IProgramAgreementProductLine) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = model.programAgreementID;
            }
            if (model.hasOwnProperty("brochureTypeKey")) {
                this.brochureTypeKey = model.brochureTypeKey;
            }
            if (model.hasOwnProperty("brochureGroupKey")) {
                this.brochureGroupKey = model.brochureGroupKey;
            }
            if (model.hasOwnProperty("brochureCodeKey")) {
                this.brochureCodeKey = model.brochureCodeKey;
            }
            if (model.hasOwnProperty("brochureMaterialKey")) {
                this.brochureMaterialKey = model.brochureMaterialKey;
            }
            if (model.hasOwnProperty("productLineMasterMaterialKey")) {
                this.productLineMasterMaterialKey = model.productLineMasterMaterialKey;
            }
            if (model.hasOwnProperty("productLineMasterMaterialDescription")) {
                this.productLineMasterMaterialDescription = model.productLineMasterMaterialDescription;
            }
            if (model.hasOwnProperty("productLineMaterialKey")) {
                this.productLineMaterialKey = model.productLineMaterialKey;
            }
            if (model.hasOwnProperty("productLineMaterialDescription")) {
                this.productLineMaterialDescription = model.productLineMaterialDescription;
            }
            if (model.hasOwnProperty("shippingLevelKey")) {
                this.shippingLevelKey = model.shippingLevelKey;
            }
            if (model.hasOwnProperty("priceCodeKey")) {
                this.priceCodeKey = model.priceCodeKey;
            }
            if (model.hasOwnProperty("taxOnPaperworkIndicator")) {
                this.taxOnPaperworkIndicator = model.taxOnPaperworkIndicator;
            }
            if (model.hasOwnProperty("profitLevelKey")) {
                this.profitLevelKey = model.profitLevelKey;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("programAgreement")) {
                if (model.programAgreement && model.programAgreement.ref) {
                    this.programAgreement = {
                        ref: model.programAgreement.ref
                    };
                }
            }
            if (model.hasOwnProperty("brochureTypeDescription")) {
                this.brochureTypeDescription = model["brochureTypeDescription"];
            }
            if (model.hasOwnProperty("taxOnPaperworkDescription")) {
                this.taxOnPaperworkDescription = model["taxOnPaperworkDescription"];
            }
        }
    }
}

export interface IProductLineForProfitPercentageModel {
    plMasterMaterialNumber: string,
    plMaterialNumber: string,
    plMaterialDescription: string,
    profitLevelKey: string,
    plPriceCodeKey: string
}

export class ProductLineForProfitPercentageModel implements IProductLineForProfitPercentageModel {
    public plMasterMaterialNumber: string;
    public plMaterialNumber: string;
    public plMaterialDescription: string;
    public profitLevelKey: string;
    public plPriceCodeKey: string;

    constructor(brochure: IProgramAgreementProductLine) {
        this.plMasterMaterialNumber = brochure.productLineMasterMaterialKey;
        this.plMaterialNumber = brochure.productLineMaterialKey;
        this.plMaterialDescription = brochure.productLineMaterialDescription;
        this.profitLevelKey = brochure.profitLevelKey;
        this.plPriceCodeKey = brochure.priceCodeKey;
    }
}

export class ProgramAgreementProductLine implements IProgramAgreementProductLine {
    id: string;
    guid: string = uuid();
    programAgreementID: string;
    brochureTypeKey: string;
    brochureGroupKey: string;
    brochureCodeKey: string;
    brochureMaterialKey: string;
    productLineMasterMaterialKey: string;
    productLineMasterMaterialDescription: string;
    productLineMaterialKey: string;
    productLineMaterialDescription: string;
    shippingLevelKey: string;
    priceCodeKey: string;
    taxOnPaperworkIndicator: string;
    profitLevelKey: string;
    created: string = moment().toISOString();
    createdBy: number = 0;
    modified: string = moment().toISOString();
    modifiedBy: number = 0;
    version: string;
    programAgreement: {
        ref: string
    } = {ref: Constants.EMPTY_STRING};

    constructor(model?: IProgramAgreementProductLine) {
        if (model) {
            if (model.hasOwnProperty("id")) {
                this.id = model.id;
            }
            if (model.hasOwnProperty("guid")) {
                this.guid = model.guid;
            }
            if (model.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = model.programAgreementID;
            }
            if (model.hasOwnProperty("brochureTypeKey")) {
                this.brochureTypeKey = model.brochureTypeKey;
            }
            if (model.hasOwnProperty("brochureGroupKey")) {
                this.brochureGroupKey = model.brochureGroupKey;
            }
            if (model.hasOwnProperty("brochureCodeKey")) {
                this.brochureCodeKey = model.brochureCodeKey;
            }
            if (model.hasOwnProperty("brochureMaterialKey")) {
                this.brochureMaterialKey = model.brochureMaterialKey;
            }
            if (model.hasOwnProperty("productLineMasterMaterialKey")) {
                this.productLineMasterMaterialKey = model.productLineMasterMaterialKey;
            }
            if (model.hasOwnProperty("productLineMasterMaterialDescription")) {
                this.productLineMasterMaterialDescription = model.productLineMasterMaterialDescription;
            }
            if (model.hasOwnProperty("productLineMaterialKey")) {
                this.productLineMaterialKey = model.productLineMaterialKey;
            }
            if (model.hasOwnProperty("productLineMaterialDescription")) {
                this.productLineMaterialDescription = model.productLineMaterialDescription;
            }
            if (model.hasOwnProperty("shippingLevelKey")) {
                this.shippingLevelKey = model.shippingLevelKey;
            }
            if (model.hasOwnProperty("priceCodeKey")) {
                this.priceCodeKey = model.priceCodeKey;
            }
            if (model.hasOwnProperty("taxOnPaperworkIndicator")) {
                this.taxOnPaperworkIndicator = model.taxOnPaperworkIndicator;
            }
            if (model.hasOwnProperty("profitLevelKey")) {
                this.profitLevelKey = model.profitLevelKey;
            }
            if (model.hasOwnProperty("created")) {
                this.created = model.created;
            }
            if (model.hasOwnProperty("createdBy")) {
                this.createdBy = model.createdBy;
            }
            if (model.hasOwnProperty("modified")) {
                this.modified = model.modified;
            }
            if (model.hasOwnProperty("modifiedBy")) {
                this.modifiedBy = model.modifiedBy;
            }
            if (model.hasOwnProperty("version")) {
                this.version = model.version;
            }
            if (model.hasOwnProperty("programAgreement")) {
                if (model.programAgreement && model.programAgreement.ref) {
                    this.programAgreement = {
                        ref: model.programAgreement.ref
                    };
                }
            }
        }
    }
}
