/**
 * Created by mjwheatley on 7/25/16.
 */
import * as moment from 'moment';
import {Constants} from "../Constants";

export interface IProgramModel {
    startDate?: string,
    signDate?: string,
    cancellationDate?: string,
    estimatedGroupSize?: number,
    estimatedNumberOfClassrooms?: number,
    estimatedWholesale?: number,
    purchaseOrderTypeKey?: string,
    purchaseOrderNumber?: string,
    isDonationsEnabled?: boolean,
    isFullServiceEnabled?: boolean,
    is59MinuteEnabled?: boolean,
    isPaymentAppEnabled?: boolean,
    isTuitionRewardsEnabled?: boolean,
    isShippingToAccountAllowed?: boolean,
    translations?: IProgramModel
}

export class ProgramModel implements IProgramModel {
    public startDate: string = moment().format("YYYY-MM-DD");
    public signDate: string = moment().format("YYYY-MM-DD");
    public cancellationDate: string;
    public estimatedGroupSize: number = 0;
    public estimatedNumberOfClassrooms: number = 0;
    public estimatedWholesale: number = 0;
    public purchaseOrderTypeKey: string = Constants.EMPTY_STRING;
    public purchaseOrderNumber: string = Constants.EMPTY_STRING;
    public isDonationsEnabled: boolean = false;
    public isFullServiceEnabled: boolean = false;
    public is59MinuteEnabled: boolean = false;
    public isPaymentAppEnabled: boolean = false;
    public isTuitionRewardsEnabled: boolean = false;
    public isShippingToAccountAllowed: boolean = false;

    constructor(programInfo?: IProgramModel) {
        if (programInfo) {
            if (programInfo.hasOwnProperty("startDate")) {
                this.startDate = programInfo.startDate;
            }
            if (programInfo.hasOwnProperty("signDate")) {
                this.signDate = programInfo.signDate;
            }
            if (programInfo.hasOwnProperty("cancellationDate")) {
                this.cancellationDate = programInfo.cancellationDate;
            }
            if (programInfo.hasOwnProperty("estimatedGroupSize")) {
                this.estimatedGroupSize = programInfo.estimatedGroupSize;
            }
            if (programInfo.hasOwnProperty("estimatedNumberOfClassrooms")) {
                this.estimatedNumberOfClassrooms = programInfo.estimatedNumberOfClassrooms;
            }
            if (programInfo.hasOwnProperty("estimatedWholesale")) {
                this.estimatedWholesale = programInfo.estimatedWholesale;
            }
            if (programInfo.hasOwnProperty("purchaseOrderTypeKey")) {
                this.purchaseOrderTypeKey = programInfo.purchaseOrderTypeKey;
            }
            if (programInfo.hasOwnProperty("purchaseOrderNumber")) {
                this.purchaseOrderNumber = programInfo.purchaseOrderNumber;
            }
            if (programInfo.hasOwnProperty("isDonationsEnabled")) {
                this.isDonationsEnabled = programInfo.isDonationsEnabled;
            }
            if (programInfo.hasOwnProperty("isFullServiceEnabled")) {
                this.isFullServiceEnabled = programInfo.isFullServiceEnabled;
            }
            if (programInfo.hasOwnProperty("is59MinuteEnabled")) {
                this.is59MinuteEnabled = programInfo.is59MinuteEnabled;
            }
            if (programInfo.hasOwnProperty("isPaymentAppEnabled")) {
                this.isPaymentAppEnabled = programInfo.isPaymentAppEnabled;
            }
            if (programInfo.hasOwnProperty("isTuitionRewardsEnabled")) {
                this.isTuitionRewardsEnabled = programInfo.isTuitionRewardsEnabled;
            }
            if (programInfo.hasOwnProperty("isShippingToAccountAllowed")) {
                this.isShippingToAccountAllowed = programInfo.isShippingToAccountAllowed;
            }
        }
    }
}