/**
 * Created by mjwheatley on 7/25/16.
 */
import * as moment from 'moment';
import {Constants} from "../Constants";
import {IProgramAgreementNote, ProgramAgreementNote} from "./ProgramAgreementDownloadResponseModel";
import {Enums} from "../Enums";

export interface IInvoiceOtherModel {
    programAgreementID?: string,
    programAgreementRef?: string,
    prizeCostAmount?: number,
    prizeCostPercentage?: number,
    collationFeeAmount?: number,
    accountTaxClassificationKey?: string;
    accountTaxClassificationExpirationDate?: string,
    noteForOrderProcessing?: IProgramAgreementNote
}

export class InvoiceOtherModel implements IInvoiceOtherModel {
    public programAgreementID: string;
    public programAgreementRef: string;
    public prizeCostAmount: number = 0;
    public prizeCostPercentage: number = 0;
    public collationFeeAmount: number = 0;
    public accountTaxClassificationKey: string = Constants.EMPTY_STRING;
    public accountTaxClassificationExpirationDate: string = moment().format("YYYY-MM-DD");
    public noteForOrderProcessing:ProgramAgreementNote = new ProgramAgreementNote({
        programAgreementNoteTypeKey: Enums.PROGRAM_AGREEMENT_NOTE_TYPE.O
    });

    constructor(invoiceOther?: IInvoiceOtherModel) {
        if (invoiceOther) {
            if (invoiceOther.hasOwnProperty("programAgreementID")) {
                this.programAgreementID = invoiceOther.programAgreementID;
            }
            if (invoiceOther.hasOwnProperty("programAgreementRef")) {
                this.programAgreementRef = invoiceOther.programAgreementRef;
            }
            if (invoiceOther.hasOwnProperty("prizeCostAmount")) {
                this.prizeCostAmount = invoiceOther.prizeCostAmount;
            }
            if (invoiceOther.hasOwnProperty("prizeCostPercentage")) {
                this.prizeCostPercentage = invoiceOther.prizeCostPercentage;
            }
            if (invoiceOther.hasOwnProperty("collationFeeAmount")) {
                this.collationFeeAmount = invoiceOther.collationFeeAmount;
            }
            if (invoiceOther.hasOwnProperty("accountTaxClassificationKey")) {
                this.accountTaxClassificationKey = invoiceOther.accountTaxClassificationKey;
            }
            if (invoiceOther.hasOwnProperty("accountTaxClassificationExpirationDate")) {
                this.accountTaxClassificationExpirationDate = invoiceOther.accountTaxClassificationExpirationDate;
            }
            if (invoiceOther.hasOwnProperty("noteForOrderProcessing") && invoiceOther.noteForOrderProcessing) {
                this.noteForOrderProcessing = new ProgramAgreementNote(invoiceOther.noteForOrderProcessing);
            }
        }
        if (this.programAgreementID && !this.noteForOrderProcessing.programAgreementID) {
            this.noteForOrderProcessing.programAgreementID = this.programAgreementID;
        }
        if (this.programAgreementRef && !this.noteForOrderProcessing.programAgreement.ref) {
            this.noteForOrderProcessing.programAgreement.ref = this.programAgreementRef;
        }
    }
}