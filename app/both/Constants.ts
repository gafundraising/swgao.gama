/**
 * Created by mjwheatley on 5/2/16.
 */
export class Constants {
    public static EMPTY_STRING = "";

    public static DEVICE: any = {
        IOS: "iOS",
        ANDROID: "Android"
    };

    public static STYLE: any = {
        IOS: "ios",
        MD: "md"
    };

    public static SESSION: any = {
        LANGUAGE: "language",
        DEVICE_READY: "deviceready",
        PLATFORM_READY: "platformReady",
        THREE_DEE_TOUCH_PAYLOAD: "threeDeeTouchPayload",
        USER_NAME: "username",
        USE_FINGERPRINT: "useFingerprint",
        REMEMBER_ME: "rememberMe",
        PROGRAM_AGREEMENT_ID: "programAgreementId",
        OFFLINE_USER: "offlineUser",
        TRANSLATIONS_READY: "translationsReady",
        LOADING: "isLoading",
        PATH: "path",
        URL_PARAMS: "urlParams",
        INCORRECT_PASSWORD: "incorrectPassword",
        FORGOT_PASSWORD: "forgotPassword",
        CREATE_ACCOUNT: "createAccount",
        RESET_PASSWORD: "resetPassword",
        REGISTERED_ERROR: "registeredError",
        NOT_REGISTERED_ERROR: "notRegisteredError",
        RESET_PASSWORD_ERROR: "resetPasswordError",
        RESET_PASSWORD_ERROR_MESSAGE: "resetPasswordErrorMessage",
        RESET_PASSWORD_TOKEN: "resetPasswordToken",
        WAS_PASSWORD_RESET: "wasPasswordReset",
        EMAIL: "email",
        IS_IPHONE_X_LAYOUT: "isIPhoneXLayout",
        IS_ADMIN: "isAdmin",
        SELECTED_SEASON: "selectedSeason",
        HIDE_IMPORTANT_MESSAGE: "hideImportantMessage",
        SORT_CONTRACTS_DESCENDING: "sortContractsDescending",
        SORT_CATEGORY: "sortCategory",
    };

    public static ROLES = {
        ADMIN: "admin"
    };

    public static CONTRACT_STATUS = {
        ALL: "All",
        DRAFT: "Draft",
        SUBMITTED: "Submitted"
    };

    public static METEOR_ERRORS:any = {
        TIMEDOUT: "ETIMEDOUT",
        SIGN_IN: "sign-in",
        ACCOUNT_NOT_FOUND: "account-not-found",
        NO_PASSWORD: "User has no password set",
        USER_NOT_FOUND: "User not found",
        INCORRECT_PASSWORD: "Incorrect password",
        EMAIL_EXISTS: "Email already exists.",
        ALREADY_EXISTS: 'already-exists',
        TOKEN_EXPIRED: "Token expired"
    };

    public static GAO_ENDPOINTS:any = {
        AUTHENTICATE: "/api/user/authenticate",
        HIERARCHY: "/api/hierarchy",
        IMPERSONATE: "/api/user/impersonate",
        CONFIGURE: "/api/configuration/configure",
        PROGRAM_AGREEMENTS: "/api/programagreement/download?effective=0001-01-01T00%3A00%3A00.0000000",
        SUBMIT: "/api/programagreement/submit"
    };

    public static IMAGE_URI_PREFIX: string = "data:image/jpeg;base64,";

    public static PUBLICATIONS: any = {
        PROGRAM_AGREEMENT: "ProgramAgreementCollection",
        ORGANIZATION: "OrganizationCollection",
        GROUP: "GroupCollection",
        CONFIGURATION: "ConfigurationCollection",
        PROGRAM_AGREEMENT_DOWNLOAD: "ProgramAgreementDownloadsCollection",
        PARTNER_CONTACTS: "PartnerContactCollection",
        TEMP_PROGRAM_AGREEMENT: "TempProgramAgreementCollection",
        TEMP_PARTNER_CONTACTS: "TempPartnerContactCollection"
    };

    public static SIGNATURE_TYPES: any = {
        SALES_REP: "salesRep",
        SPONSOR: "sponsor"
    };
}