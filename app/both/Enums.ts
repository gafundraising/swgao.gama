export class Enums {
    public static PARTNER_TYPE: any = {
        /// <summary>
        ///   School (Organization) (School)
        /// </summary>
        ZSCH: 1,
        /// <summary>
        ///   Sponsor (Payer) (Sponsor)
        /// </summary>
        ZSPR: 2,
        /// <summary>
        ///   Group (Sold To) (Group)
        /// </summary>
        ZGRP: 3,
        /// <summary>
        ///   Sales Representative (SalesRep)
        /// </summary>
        ZSLS: 4
    };

    public static PARTNER_FUNCTION: any = {
        /// <summary>
        ///   Sold To (Group) (SoldTo)
        /// </summary>
        SOLD_TO: "AG",
        /// <summary>
        ///   Organization (School) (Org)
        /// </summary>
        ORGANIZATION: "OG",
        /// <summary>
        ///   Bill To (BillTo)
        /// </summary>
        BILL_TO: "RE",
        /// <summary>
        ///   Payer (Sponsor) (Payer)
        /// </summary>
        PAYER: "RG",
        /// <summary>
        ///   Ship To (ShipTo)
        /// </summary>
        SHIP_TO: "WE",
        /// <summary>
        ///   Original Sales Rep (OrigRep)
        /// </summary>
        ORIGINAL_SALES_REP: "ZO",
        /// <summary>
        ///   Sales Rep (SalesRep)
        /// </summary>
        SALES_REP: "ZS",
        /// <summary>
        ///   Sold To (Temporary) (TempSold)
        /// </summary>
        TEMP_SOLD_TO: "Z7",
        /// <summary>
        ///   Payer (Temporary) (TempPay)
        /// </summary>
        TEMP_PAYER: "Z8"
    };

    public static PROGRAM_AGREEMENT_STATE: any = {
        /// <summary>
        ///   Draft - no steps completed (Draft 0)
        /// </summary>
        D0: "D0", // 0
        /// <summary>
        ///   Draft - step 1 completed (Draft 1)
        /// </summary>
        D1: "D1", // 1
        /// <summary>
        ///   Draft - step 2 completed (Draft 2)
        /// </summary>
        D2: "D2", // 2
        /// <summary>
        ///   Draft - step 3 completed (Draft 3)
        /// </summary>
        D3: "D3", // 3
        /// <summary>
        ///   Draft - step 4 completed (Draft 4)
        /// </summary>
        D4: "D4", // 4
        /// <summary>
        ///   Draft - step 5 completed (Draft 5)
        /// </summary>
        D5: "D5", // 5
        /// <summary>
        ///   Draft - step 6 completed (Draft 6)
        /// </summary>
        D6: "D6", // 6
        /// <summary>
        ///   Submitted to SAP (Submit)
        /// </summary>
        S1: "S1", // 10,
        /// <summary>
        ///   Confirmed Received by SAP (Received)
        /// </summary>
        S2: "S2", // 20,
        /// <summary>
        ///   Queued for Review (Queued)
        /// </summary>
        S3: "S3", // 30,
        /// <summary>
        ///   In Review (Review)
        /// </summary>
        S4: "S4", // 31,
        /// <summary>
        ///   Released for Create (Creating)
        /// </summary>
        S5: "S5", // 32,
        /// <summary>
        ///   Error (Error)
        /// </summary>
        S6: "S6", // 33,
        /// <summary>
        ///   Future (Future)
        /// </summary>
        A1: "A1", // 40,
        /// <summary>
        ///   Yet to Run (YetToRun)
        /// </summary>
        A2: "A2", // 50,
        /// <summary>
        ///   Delinquent (Delinq)
        /// </summary>
        A3: "A3", // 60,
        /// <summary>
        ///   Active (Active)
        /// </summary>
        A4: "A4", // 70,
        /// <summary>
        ///   Order Received (HasOrder)
        /// </summary>
        A5: "A5", // 80,
        /// <summary>
        ///   Canceled (Canceled)
        /// </summary>
        A6: "A6", // 90
    };

    public static PROGRAM_AGREEMENT_STATE_DESCRIPTION: any = {
        DO: "Draft",
        D1: "Draft",
        D2: "Draft",
        D3: "Draft",
        D4: "Draft",
        D5: "Draft",
        D6: "Draft",
        S1: "Submitted to SAP",
        S2: "Received by SAP",
        S3: "Queued",
        S4: "In Review",
        S5: "Released",
        S6: "Error",
        A1: "Future",
        A2: "Yet to Run",
        A3: "Delinquent",
        A4: "Active",
        A5: "Order Received",
        A6: "Canceled"
    };

    public static PROGRAM_AGREEMENT_NOTE_TYPE: any = {
        /// <summary>
        ///   Warehouse Notes (Whse)
        /// </summary>
        W: "W", // 1,
        /// <summary>
        ///   Carrier Notes (Carrier)
        /// </summary>
        C: "C", //2,
        /// <summary>
        ///   Order Processing Notes (Order)
        /// </summary>
        O: "O" // 3
    };

    public static ADDRESS_TYPE: any = {
        /// <summary>
        ///   Physical Address (Physical)
        /// </summary>
        P: "P", // 1
        /// <summary>
        ///   Mailing Address (Mailing)
        /// </summary>
        M: "M" // 2
    };

    public static PHONE_TYPE: any = {
        /// <summary>
        ///   Home (Home)
        /// </summary>
        H: "H", // 1,
        /// <summary>
        ///   Work (Work)
        /// </summary>
        W: "W", // 2,
        /// <summary>
        ///   Mobile (Mobile)
        /// </summary>
        M: "M", // 3,
        /// <summary>
        ///   Fax (Fax)
        /// </summary>
        F: "F" // 4
    }
}